import React from 'react';
import Overview from './Overview';
import 'spiketip-tooltip/spiketip.min.css';
import 'highlight.js/styles/github.css';
import Intro from './Overview/Contents/Intro';

class App extends React.Component {
  
  constructor(){
    super();
    this.state = {
      isMobileDevice: false,
    }
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions);
  }

  updateDimensions = () => {
    this.setState({
      isMobileDevice: this._isMobileDevice()
    });
  };

  _isMobileDevice = () => {
    return this.getWidth() < 768;
  };

  getWidth() {
    return Math.max(
      document.documentElement.clientWidth,
      window.innerWidth || 0
    );
  }


  render() {
    const { isMobileDevice } = this.state;
    return (
      <>
        {
          isMobileDevice ? 
            <Intro isMobileDevice={isMobileDevice}/> : <Overview/>
        }
      </>
    );
  }
}

export default App;