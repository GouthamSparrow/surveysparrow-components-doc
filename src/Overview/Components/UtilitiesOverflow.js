import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import PreviewBlock from './Helpers/UtilityPreview';
import Text from '../../Components/SSComponents/Text';

const exampleObjectOverflow = {
  1: {
    id: 1,
    title: 'Overflow Blocks',
    content: <div class="utility-preview--container fx-row fx-jc--around">
        <PreviewBlock extraClass="border utility-preview--item--large ss-overflow--hidden"><Text size="h4" weight="normal" color="black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cursus in hac habitasse platea. Tincidunt dui ut ornare lectus sit amet est. Ut porttitor leo a diam sollicitudin tempor id. Nisl condimentum id venenatis a condimentum vitae sapien pellentesque habitant. Posuere morbi leo urna molestie. Blandit volutpat maecenas volutpat blandit aliquam etiam. Adipiscing elit ut aliquam purus sit amet. Vitae sapien pellentesque habitant morbi tristique senectus et netus et. Aenean euismod elementum nisi quis eleifend quam. Aenean sed adipiscing diam donec adipiscing tristique risus. Sed viverra tellus in hac habitasse </Text></PreviewBlock>
        <PreviewBlock extraClass="border utility-preview--item--large ss-overflow--auto">< Text size="h4" weight="normal" color="black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cursus in hac habitasse platea. Tincidunt dui ut ornare lectus sit amet est. Ut porttitor leo a diam sollicitudin tempor id. Nisl condimentum id venenatis a condimentum vitae sapien pellentesque habitant. Posuere morbi leo urna molestie. Blandit volutpat maecenas volutpat blandit aliquam etiam. Adipiscing elit ut aliquam purus sit amet. Vitae sapien pellentesque habitant morbi tristique senectus et netus et. Aenean euismod elementum nisi quis eleifend quam. Aenean sed adipiscing diam donec adipiscing tristique risus. Sed viverra tellus in hac habitasse </Text></PreviewBlock>
        <PreviewBlock extraClass="border utility-preview--item--large ss-overflow--scroll">< Text size="h4" weight="normal" color="black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cursus in hac habitasse platea. Tincidunt dui ut ornare lectus sit amet est. Ut porttitor leo a diam sollicitudin tempor id. Nisl condimentum id venenatis a condimentum vitae sapien pellentesque habitant. Posuere morbi leo urna molestie. Blandit volutpat maecenas volutpat blandit aliquam etiam. Adipiscing elit ut aliquam purus sit amet. Vitae sapien pellentesque habitant morbi tristique senectus et netus et. Aenean euismod elementum nisi quis eleifend quam. Aenean sed adipiscing diam donec adipiscing tristique risus. Sed viverra tellus in hac habitasse </Text></PreviewBlock>
        <PreviewBlock extraClass="border utility-preview--item--large ss-overflow--visible">< Text size="h4" weight="normal" color="black">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Cursus in hac habitasse platea. Tincidunt dui ut ornare lectus sit amet est. Ut porttitor leo a diam sollicitudin tempor id. Nisl condimentum id venenatis a condimentum vitae sapien pellentesque habitant. </Text></PreviewBlock>
        </div>,
    code: 
`<div class="ss-overflow--hidden"></div>
<div class="ss-overflow--auto"></div>
<div class="ss-overflow--scroll"></div>
<div class="ss-overflow--visible"></div>`
  },
}
class Overflow extends React.Component {
    constructor(){
      super();
      this.state = {
        copiedID: 0,
      }
    }
  
    componentDidMount() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
    componentDidUpdate() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
  
    handleCopy = (id, value) => {
      copy(value);
      this.setState({
        copiedID: id
      })
      setTimeout(() => {
        this.setState({
          copiedID: 0
        })
      }, 2000);
    }
    render() {
      const { copiedID } = this.state;
      return (
        <div className="ss-clib-main-content-container">
          <div className="code-content__header">
            <h1 className="ss-heading-text ss-text__family--serif">Overflow</h1>
          </div>
          <div className="code-content__wrapper">
            <CodeReferenceBlock
              title={"Overflow"}
              info={"Use these shorthand utilities for quickly configuring how content overflows an element."}
              exampleObject={exampleObjectOverflow}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          </div>
        </div>
      
      );
    }
  }
  
  export default Overflow;