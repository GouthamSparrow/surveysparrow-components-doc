import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import PreviewBlock from './Helpers/UtilityPreview';

const exampleObjectBackground = {
  2: {
    id: 2,
    title: 'Grid - Row',
    content: <div class="utility-preview--container">
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-md-12"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-md-12"></PreviewBlock></div>
    </div>,
    code: 
`<div class="grid">...</div>
<div class="grid">...</div>`
  },
  3: {
    id: 3,
    title:'Column - MD (768px and Above)',
    content: <div class="utility-preview--container container">
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-md-1"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-md-2"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-md-4"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-md-6"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-md-10"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-md-12"></PreviewBlock></div>
    </div>,
    code: 
`<div class="grid">
  <div class="col-md-1"></div>
  <div class="col-md-2"></div>
  <div class="col-md-4"></div>
  <div class="col-md-6"></div>
  <div class="col-md-10"></div>
  <div class="col-md-12"></div>
</div>`
  },
  4: {
    id: 4,
    title:'Column - SM (576px and Above)',
    content: <div class="utility-preview--container container">
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-sm-1"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-sm-2"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-sm-4"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-sm-6"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-sm-10"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-sm-12"></PreviewBlock></div>
    </div>,
    code: 
`<div class="grid">
  <div class="col-sm-1"></div>
  <div class="col-sm-2"></div>
  <div class="col-sm-4"></div>
  <div class="col-sm-6"></div>
  <div class="col-sm-10"></div>
  <div class="col-sm-12"></div>
</div>`
  },
  5: {
    id: 5,
    title:'Column - All',
    content: <div class="utility-preview--container container">
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-1"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-2"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-4"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-6"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-10"></PreviewBlock></div>
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-12"></PreviewBlock></div>
    </div>,
    code: 
`<div class="grid">
  <div class="col-1"></div>
  <div class="col-2"></div>
  <div class="col-4"></div>
  <div class="col-6"></div>
  <div class="col-10"></div>
  <div class="col-12"></div>
</div>`
  },
  6: {
    id: 6,
    title:'Combination',
    content: <div class="utility-preview--container container">
      <div class="grid"><PreviewBlock extraClass="background-color--yellow mtb--lg col-12 col-sm-8 col-md-6"></PreviewBlock></div>      </div>,
    code: 
`<div class="grid">
  <div class="col-12 col-sm-8 col-md-6"></div>
</div>`
  },
}
class Grid extends React.Component {
    constructor(){
      super();
      this.state = {
        copiedID: 0,
      }
    }
  
    componentDidMount() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
    componentDidUpdate() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
  
    handleCopy = (id, value) => {
      copy(value);
      this.setState({
        copiedID: id
      })
      setTimeout(() => {
        this.setState({
          copiedID: 0
        })
      }, 2000);
    }
    render() {
      const { copiedID } = this.state;
      return (
        <div className="ss-clib-main-content-container">
          <div className="code-content__header">
            <h1 className="ss-heading-text ss-text__family--serif">Grid</h1>
          </div>
          <div className="code-content__wrapper">
            <CodeReferenceBlock
              title={"Grid"}
              info={"Grid system uses a series of grids and columns to layout and align content. default: 0, sm: 576px, md: 768px"}
              exampleObject={exampleObjectBackground}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          </div>
        </div>
      
      );
    }
  }
  
  export default Grid;