import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import ExampleBlock from './Helpers/ExampleBlock';
import { Text } from '../../Components/SSComponents';
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { Text } from 'SSComponents';`;

const exampleObject = {
  1: {
    id: 1,
    content: <Text size="jumbo" weight="bold" color="grey" family="serif">Jumbo - 28px - Lorem ipsum dolor sit amet, consectetur elit.</Text>,
    code: `<Text size="jumbo" weight="bold" color="grey" family="serif">Jumbo - 28px - Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>`,
  },
  2: {
    id: 2,
    content: <Text size="h1" weight="semibold" color="black">H1 - 24px - Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>,
    code: `<Text size="h1" weight="semibold" color="black">H1 - 24px - Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>`,
  },
  3: {
    id: 3,
    content: <Text size="h2" weight="semibold" color="black">H2 - 20px - Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>,
    code: `<Text size="h2" weight="semibold" color="black">H2 - 20px - Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>`,
  },
  4: {
    id: 4,
    content: <Text size="h3" weight="semibold" color="black">H3 - 16px - Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>,
    code: `<Text size="h3" weight="semibold" color="black">H3 - 16px - Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>`,
  },
  5: {
    id: 5,
    content: <Text size="h4" weight="normal" color="black">H4 - 14px - Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>,
    code: `<Text size="h4" weight="normal" color="black">H4 - 14px - Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>`,
  },
  6: {
    id: 6,
    content: <Text size="micro" weight="normal" color="black">Micro - 12px - Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>,
    code: `<Text size="micro" weight="normal" color="black">Micro - 12px - Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>`,
  },
}

const propObject = {
  1: {
    id : 1,
    name: 'size',
    info: <>Specify the Size of the Text. Default Size is <span>16px</span></>,
    type: `enum('jumbo', 'h1', 'h2', 'h3', 'h4', 'micro')`,
    required: 'Yes'
  },
  2: {
    id : 2,
    name: 'children',
    info: 'Specify the Content the Text should display.',
    type: 'string',
    required: 'Yes'
  },
  3: {
    id : 3,
    name: 'weight',
    info: <>Specify the Weight of the Text. Default Weight is <span>regular</span></>,
    type: `enum('bold', 'semibold', 'regular', 'normal', 'thin')`,
    required: 'No'
  },
  4: {
    id : 4,
    name: 'color',
    info: <>Specify the Color of the Text. Default Color is <span>grey</span></>,
    type: `enum('black', 'grey', 'white', 'purple')`,
    required: 'No'
  },
  5: {
    id : 5,
    name: 'family',
    info: <>Specify the Font Family of the Text. Default Font Family is <span>sans</span></>,
    type: `enum('sans', 'serif')`,
    required: 'No'
  },
  6: {
    id : 6,
    name: 'lineHeight',
    info: <>Specify the Line Height of the Text. Default Line Height is <span>normal</span></>,
    type: `enum('normal', 'small', 'medium', 'large')`,
    required: 'No'
  },
  7: {
    id : 7,
    name: 'opacity',
    info: <>Specify the Opacity of the Text. Default Opacity is <span>1</span></>,
    type: `Value between 0 and 1`,
    required: 'No'
  },
  8: {
    id : 8,
    name: 'textCenter',
    info: 'Determines whether the Text should be center aligned',
    type: `boolean`,
    required: 'No'
  },
  9: {
    id : 9,
    name: 'isUppercase',
    info: 'Determines whether the Text should be converted to Uppercase',
    type: `boolean`,
    required: 'No'
  },
  10: {
    id : 10,
    name: 'extraClass',
    info: 'Specify any other class that should be applied to the Text',
    type: 'string',
    required: 'No'
  }
}

class Typograhy extends React.Component {

  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }

  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Text</h1>
        </div>
        <div className="code-content__wrapper">

          {/* Import */}

          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />

          <ExampleBlock exampleObject={exampleObject} extraSpacing={true} fullWidth={true}/>
          <CodeReferenceBlock
            exampleObject={exampleObject}
            language="html"
            copiedID={copiedID}
            handleCopy={this.handleCopy}
            fullWidth={true}
          />

          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>
        </div>
      </div>
    );
  }
}

export default Typograhy;