import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import Checkbox from '../../Components/SSComponents/Checkbox';
import Text from '../../Components/SSComponents/Text';
import ExampleBlock from './Helpers/ExampleBlock';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { Checkbox } from 'SSComponents';`;

const exampleObject = {
  1: {
    id: 1,
    content: <div className="flex_row"><Text size="h4" color="grey" weight="normal" extraClass="mr--sm">Small</Text><Checkbox name="small" size="small"/></div>,
    code: `<Checkbox name="small" size="small"/>`
  },
  2: {
    id: 2,
    content: <div className="flex_row"><Text size="h4" color="grey" weight="normal" extraClass="mr--sm">Large</Text><Checkbox name="large" size="large"/></div>,
    code: `<Checkbox name="large" size="large"/>`
  },
 
  3: {
    id: 3,
    content: <div className="flex_row"><Text size="h4" color="grey" weight="normal" extraClass="mr--sm">Disabled</Text><Checkbox name="disabled" isDisabled /></div>,
    code: `<Checkbox name="disabled" isDisabled />`
  },
  4: {
    id: 4,
    content: <div className="flex_row"><Text size="h4" color="grey" weight="normal" extraClass="mr--sm">Checked</Text><Checkbox name="checked" size="large" isChecked/></div>,
    code: `<Checkbox name="checked" isChecked />`
  },
}
const propObject = {
  1: {
    id : 1,
    name: 'name',
    info: 'Specify the name, id of checkbox',
    type: 'string',
    required: 'Yes'
  },
  2: {
    id : 2,
    name: 'size',
    info: <>Specify the size of the Checkbox. Default value is <span>medium</span></>,
    type: 'string',
    required: 'No'
  },
  3: {
    id : 3,
    name: 'isDisabled',
    info: 'Specify whether the checkbox is disabled or not',
    type: 'boolean',
    required: 'No'
  },
  4: {
    id : 4,
    name: 'isChecked',
    info: 'Specify whether the checkbox is checked or not',
    type: 'boolean',
    required: 'No'
  },
  5: {
    id : 5,
    name: 'extraClass',
    info: 'Specify extra classes',
    type: 'boolean',
    required: 'No'
  },
}

class FormCheckbox extends React.Component {
  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }
  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Checkbox</h1>
        </div>
        <div className="code-content__wrapper">
          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />
          <ExampleBlock exampleObject={exampleObject}/>
          <CodeReferenceBlock
              exampleObject={exampleObject}
              language="jsx"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
          />
          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>
        </div>
      </div>
    
    );
  }
}

export default FormCheckbox;