import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import Select from '../../Components/SSComponents/Select';
import ExampleBlock from './Helpers/ExampleBlock';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { Select } from 'SSComponents';`;

const selectOptionCode = `const options = [
  { label: "Iron Man", value: "value_1" },
  { label: "Captain America", value: "value_2" },
  { label: "Thor", value: "value_3" }
];`;

const options = [
  { label: "Iron Man", value: "value_1" },
  { label: "Captain America", value: "value_2" },
  { label: "Thor", value: "value_3" }
];

const exampleObject = {
  1: {
    id: 1,
    content: <Select options={options} placeholder={'Select without Label'}/>,
    code: `<Select options={options} placeholder={'Select without Label'}/>`
  },
  2: {
    id: 2,
    content: <Select options={options} label={'Select with Label'}/>,
    code: `<Select options={options} label={'Select with Label'}/>`
  },
  4: {
    id: 4,
    content: <Select options={options} label={'Select with Label and Info'} inputInfo={'This is an Select Info'}/>,
    code: `<Select options={options} label={'Select with Label and Info'} inputInfo={'This is an Select Info'}/>`
  },
  5: {
    id: 5,
    content: <Select options={options} label={'Select with Value'} value={'dev@surveysparrow.com'}/>,
    code: `<Select options={options} label={'Select with Value'} value={'dev@surveysparrow.com'}/>`
  },
  6: {
    id: 6,
    content: <Select options={options} label={'Disabled Select'} value={'You Cannot Edit Me!'} isDisabled/>,
    code: `<Select options={options} label={'Disabled Select'} value={'You Cannot Edit Me!'} isDisabled/>`
  },
  7: {
    id: 7,
    content: <Select size="small" options={options} label={'Small Select with Label'}/>,
    code: `<Select size="small" options={options} label={'Small Select with Label'}/>`
  },
  8: {
    id: 8,
    content: <Select options={options} label={'Select with Error'} error={'Please Enter a Value'}/>,
    code: `<Select options={options} label={'Select with Error'} error={'Please Enter a Value'}/>`
  },
}

const propObject = {
  1: {
    id : 1,
    name: 'label',
    info: 'Specify the text to be displayed above the <select> input.',
    type: 'string',
    required: 'No'
  },
  2: {
    id : 2,
    name: 'inputInfo',
    info: 'Specify the helper text for the <select> if needed.',
    type: 'string',
    required: 'No'
  },
  3: {
    id : 3,
    name: 'isDisabled',
    info: <>Determines whether the &lt;select&gt; is disabled. Default value is <span>false</span></>,
    type: 'boolean',
    required: 'No'
  },
  4: {
    id : 4,
    name: 'error',
    info: 'Determines whether there is any error in the <select> and shows the error message.',
    type: 'string',
    required: 'No'
  },
  5: {
    id : 5,
    name: 'placeholder',
    info: 'Specify the Placeholder Text the <select> should display.',
    type: 'string',
    required: 'No'
  },
  6: {
    id : 6,
    name: 'noPadding',
    info: 'Determines whether the Padding of the <select> should be removed',
    type: 'boolean',
    required: 'No'
  },
  7: {
    id : 7,
    name: 'noMargin',
    info: 'Determines whether the Margin of the <select> should be removed',
    type: 'boolean',
    required: 'No'
  },
  8: {
    id : 8,
    name: 'size',
    info: 'Determines the size of <select>. Default value is Normal',
    type: `enum('small', 'normal')`,
    required: 'No'
  },
}

class FormSelect extends React.Component {

  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }

  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Select</h1>
        </div>
        <div className="code-content__wrapper">

          {/* Import */}

          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />


          <ExampleBlock exampleObject={exampleObject}/>
          <CodeReferenceBlock
            exampleObject={exampleObject}
            language="jsx"
            copiedID={copiedID}
            handleCopy={this.handleCopy}
          />
          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>

          <CodeBlock
            title="Options Reference"
            item={-2}
            copiedID={copiedID}
            importCode={selectOptionCode}
            handleCopy={this.handleCopy}
          />
        </div>
      </div>
    );
  }
}

export default FormSelect;