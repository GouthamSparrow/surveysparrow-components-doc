
import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import PreviewBlock from './Helpers/UtilityPreview';

const exampleObjectBackground = {
  1: {
    id: 1,
    title:'Background Colors',
    content: <div class="utility-preview--container fx-row">
    <PreviewBlock extraClass="utility-preview--item--dimensions background-color--grey mlr--sm"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--dimensions background-color--swing mlr--sm"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--dimensions background-color--orange mlr--sm"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--dimensions background-color--yellow ss-float--right mlr--sm"></PreviewBlock>
    </div>,
    code: 
`<div></div>
<div></div>
<div></div>
<div class="ss-float--right"></div>`
  },
}
class Float extends React.Component {
    constructor(){
      super();
      this.state = {
        copiedID: 0,
      }
    }
  
    componentDidMount() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
    componentDidUpdate() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
  
    handleCopy = (id, value) => {
      copy(value);
      this.setState({
        copiedID: id
      })
      setTimeout(() => {
        this.setState({
          copiedID: 0
        })
      }, 2000);
    }
    render() {
      const { copiedID } = this.state;
      return (
        <div className="ss-clib-main-content-container">
          <div className="code-content__header">
            <h1 className="ss-heading-text ss-text__family--serif">Float</h1>
          </div>
          <div className="code-content__wrapper">
            <CodeReferenceBlock
              title={"Float Right"}
              info={"Toggle floats on any element using the float utility."}
              exampleObject={exampleObjectBackground}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          </div>
        </div>
      
      );
    }
  }
  
  export default Float;