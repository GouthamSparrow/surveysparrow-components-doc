import React from 'react';
import hljs from 'highlight.js';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import SectionTitle from './Helpers/SectionTitle';
import { Text, SVG } from '../../Components/SSComponents';
import * as icons from '../../Components/SVGIcons'
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { SVG } from 'SSComponents';`;

const propObject = {
  1: {
    id : 1,
    name: 'name',
    info: 'Specify the name of the Icon to be displayed.',
    type: 'string',
    required: 'Yes'
  },
  2: {
    id : 2,
    name: 'size',
    info: <>Specify the Size of the Icon. Default Size is <span>16px</span></>,
    type: 'number',
    required: 'No'
  },
  3: {
    id : 3,
    name: 'color',
    info: <>Specify the Color of the Icon. Default Color is <span>grey</span></>,
    type: `enum('white', 'black', 'grey', 'orange', 'purple') or #hex`,
    required: 'Yes'
  },
  4: {
    id : 4,
    name: 'extraClass',
    info: 'Specify any other class that should be applied to the Icon',
    type: 'string',
    required: 'No'
  }
}

class Icon extends React.Component {

  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Icon</h1>
        </div>
        <div className="code-content__wrapper">

          {/* Import */}

          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />

          <div className="code-content__section">
            <SectionTitle
              title="Icon List"
              info="Just click to copy the code and use it. It's as simple as that."
            />
            <div className="flex_row flex_row--row-mob flex_row--wrap code-content__example">
              {
                Object.keys(icons).map((item, index) => {
                  const name = item.toLowerCase().replace('svg', '');
                  if (name === 'surveysparrow')
                   return null;
                  else
                  return (
                    <div key={index} className="code-content__icon-wrapper">
                      <SVG size={32} name={name} color="grey" extraClass="mb--md"/>
                      <Text size="micro" color="grey" weight="normal">{name}</Text>
                    </div>
                  );
                })
              }
            </div>
          </div>

          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>
        </div>
      </div>
    );
  }
}

export default Icon;