import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import FlexboxPreview from './Helpers/FlexPreview';

const exampleObjectRow = {
    1: {
      id: 1,
      title:'Flex No Wrap',
      content: <><FlexboxPreview type="large" blocks="21" extraClass="fx-row fx--nowrap"></FlexboxPreview></>,
      code: `<div class="fx-row fx--nowrap">{children}</div>`
    },
    2: {
      id: 2,
      title:'Flex Wrap',
      content: <><FlexboxPreview type="large" blocks="21" extraClass="fx-row fx--wrap"></FlexboxPreview></>,
      code: `<div class="fx-row fx--wrap">{children}</div>`
    },
    3: {
      id: 3,
      title:'Flex Wrap Reverse',
      content: <><FlexboxPreview type="large" blocks="12" extraClass="fx-row fx--wrapreverse fx-ac--end" childClass="color"></FlexboxPreview></>,
      code: `<div class="fx-row">{children}</div>`
    },
}
const exampleObjectColumn = {
    1: {
      id: 1,
      title:'Flex No Wrap',
      content: <><FlexboxPreview type="large" blocks="21" extraClass="fx-column fx-jc--start fx--nowrap"></FlexboxPreview></>,
      code: `<div class="fx-column fx--nowrap">{children}</div>`
    },
    2: {
      id: 2,
      title:'Flex Wrap',
      content: <><FlexboxPreview type="large" blocks="10" extraClass="fx-column fx--wrap"></FlexboxPreview></>,
      code: `<div class="fx-column fx--wrap">{children}</div>`
    },
    3: {
      id: 3,
      title:'Flex Wrap Reverse',
      content: <><FlexboxPreview type="large" blocks="10
      " extraClass="fx-column fx-ac--end fx--wrapreverse" childClass="color"></FlexboxPreview></>,
      code: `<div class="fx-column fx--wrapreverse">{children}</div>`
    },
}
class Wrap extends React.Component {
    constructor(){
      super();
      this.state = {
        copiedID: 0,
      }
    }
  
    componentDidMount() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
    componentDidUpdate() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
  
    handleCopy = (id, value) => {
      copy(value);
      this.setState({
        copiedID: id
      })
      setTimeout(() => {
        this.setState({
          copiedID: 0
        })
      }, 2000);
    }
    render() {
      const { copiedID } = this.state;
      return (
        <div className="ss-clib-main-content-container">
          <div className="code-content__header">
            <h1 className="ss-heading-text ss-text__family--serif">Flex Wrap</h1>
          </div>
          <div className="code-content__wrapper">
            <CodeReferenceBlock
              title={"Flex Row - Wrap"}
              info={"Change how flex items wrap in a flex container. Choose from no wrapping at all (the browser default) with .fx--nowrap, wrapping with .fx--wrap"}
              exampleObject={exampleObjectRow}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          <CodeReferenceBlock
              title={"Flex Column - Wrap"}
              info={"Change how flex items wrap in a flex container. Choose from no wrapping at all (the browser default) with .fx--nowrap, wrapping with .fx--wrap"}
              exampleObject={exampleObjectColumn}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          </div>
        </div>
      
      );
    }
  }
  
  export default Wrap;