import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import Input from '../../Components/SSComponents/Input';
import ExampleBlock from './Helpers/ExampleBlock';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { Input } from 'SSComponents';`;

const exampleObject = {
  1: {
    id: 1,
    content: <Input placeholder={'Input without Label'}/>,
    code: `<Input placeholder={'Input without Label'}/>`
  },
  2: {
    id: 2,
    content: <Input label={'Input with Label'}/>,
    code: `<Input label={'Input with Label'}/>`
  },
  4: {
    id: 4,
    content: <Input label={'Input with Label and Info'} inputInfo={'This is an Input Info'}/>,
    code: `<Input label={'Input with Label and Info'} inputInfo={'This is an Input Info'}/>`
  },
  5: {
    id: 5,
    content: <Input label={'Input with Value'} value={'dev@surveysparrow.com'}/>,
    code: `<Input label={'Input with Value'} value={'dev@surveysparrow.com'}/>`
  },
  6: {
    id: 6,
    content: <Input label={'Input with Character Limit'} maxLength={20} value={'Lorem Ipsum'}/>,
    code: `<Input label={'Input with Character Limit'} maxLength={20} value={'Lorem Ipsum'}/>`
  },
  7: {
    id: 7,
    content: <Input label={'Disabled Input'} value={'You Cannot Edit Me!'} isDisabled/>,
    code: `<Input label={'Disabled Input'} value={'You Cannot Edit Me!'} isDisabled/>`
  },
  8: {
    id: 8,
    content: <Input size="small" label={'Small Input'}/>,
    code: `<Input size="small" label={'Small Input'}/>`
  },
  9: {
    id: 9,
    content: <Input label={'Input with Error'} error={'Please Enter a Value'}/>,
    code: `<Input label={'Input with Error'} error={'Please Enter a Value'}/>`
  },
}

const propObject = {
  1: {
    id : 1,
    name: 'label',
    info: 'Specify the text to be displayed above the <input>',
    type: 'string',
    required: 'No'
  },
  2: {
    id : 2,
    name: 'inputInfo',
    info: 'Specify the helper text for the <input> if needed.',
    type: 'string',
    required: 'No'
  },
  3: {
    id : 3,
    name: 'isDisabled',
    info: <>Determines whether the &lt;input&gt; is disabled. Default value is <span>false</span></>,
    type: 'boolean',
    required: 'No'
  },
  4: {
    id : 4,
    name: 'size',
    info: 'Specify the Size of the <input>. Default value is Normal',
    type: `enum('small', 'normal')`,
    required: 'No'
  },
  5: {
    id : 5,
    name: 'error',
    info: 'Determines whether there is any error in the <input> and shows the error message.',
    type: 'string',
    required: 'No'
  },
  6: {
    id : 6,
    name: 'maxLength',
    info: 'Specifies the maximum number of characters allowed in the <input> element.',
    type: 'number',
    required: 'No'
  },
  7: {
    id : 7,
    name: 'noPadding',
    info: 'Determines whether the Padding of the <input> should be removed',
    type: 'boolean',
    required: 'No'
  },
  8: {
    id : 8,
    name: 'noMargin',
    info: 'Determines whether the Margin of the <input> should be removed',
    type: 'boolean',
    required: 'No'
  },
}

class FormInput extends React.Component {

  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }

  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Input</h1>
        </div>
        <div className="code-content__wrapper">

          {/* Import */}

          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />

          <ExampleBlock exampleObject={exampleObject}/>
          <CodeReferenceBlock
            exampleObject={exampleObject}
            language="jsx"
            copiedID={copiedID}
            handleCopy={this.handleCopy}
          />
          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>
        </div>
      </div>
    );
  }
}

export default FormInput;