import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import ExampleBlock from './Helpers/ExampleBlock';
import { Button } from '../../Components/SSComponents';
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { Button } from 'SSComponents';`;

const loaderExampleObject = {
  1: {
    id: 1,
    content: <Button type="primary" text="Lorem ipsum dolor sit amet" isLoading loaderType="bar" loadingText="Working"/>,
    code: `<Button type="primary" text="Lorem ipsum dolor sit amet" isLoading loaderType="bar" loadingText="Working"/>`,
  },
  2: {
    id: 2,
    content: <Button type="primary" text="Lorem ipsum dolor sit amet" isLoading loaderType="dots"/>,
    code: `<Button type="primary" text="Lorem ipsum dolor sit amet" isLoading loaderType="dots"/>`,
  },
  3: {            
    id: 3,
    content: <Button type="primary" text="Lorem ipsum dolor sit amet" isLoading loaderType="spinner"/>,
    code: `<Button type="primary" text="Lorem ipsum dolor sit amet" isLoading loaderType="spinner"/>`,
  },
  4: {
    id: 4,
    content: <Button type="secondary" text="Lorem ipsum dolor sit amet" isLoading loaderType="bar" loadingText="Working"/>,
    code: `<Button type="secondary" text="Lorem ipsum dolor sit amet" isLoading loaderType="bar" loadingText="Working"/>`,
  },
  5: {
    id: 5,
    content: <Button type="secondary" text="Lorem ipsum dolor sit amet" isLoading loaderType="dots"/>,
    code: `<Button type="secondary" text="Lorem ipsum dolor sit amet" isLoading loaderType="dots"/>`,
  },
  6: {            
    id: 6,
    content: <Button type="secondary" text="Lorem ipsum dolor sit amet" isLoading loaderType="spinner"/>,
    code: `<Button type="secondary" text="Lorem ipsum dolor sit amet" isLoading loaderType="spinner"/>`,
  },
  7: {
    id: 7,
    content: <Button type="text" text="Lorem ipsum dolor sit amet" isLoading loaderType="dots" extraClass="mtb--sm"/>,
    code: `<Button type="text" text="Lorem ipsum dolor sit amet" isLoading loaderType="dots"/>`,
  },
  8: {            
    id: 8,
    content: <Button type="text" text="Lorem ipsum dolor sit amet" isLoading loaderType="spinner" extraClass="mtb--sm"/>,
    code: `<Button type="text" text="Lorem ipsum dolor sit amet" isLoading loaderType="spinner"/>`,
  },
  9: {
    id: 9,
    content: <Button type="link" text="Lorem ipsum dolor sit amet" isLoading loaderType="dots" extraClass="mtb--sm"/>,
    code: `<Button type="link" text="Lorem ipsum dolor sit amet" isLoading loaderType="dots"/>`,
  },
  10: {            
    id: 10,
    content: <Button type="link" text="Lorem ipsum dolor sit amet" isLoading loaderType="spinner" extraClass="mtb--sm"/>,
    code: `<Button type="link" text="Lorem ipsum dolor sit amet" isLoading loaderType="spinner"/>`,
  },
}
const exampleObject = {
  1: {
    id: 1,
    content: <Button type="primary" text="Primary Button"/>,
    code: `<Button type="primary" text="Primary Button"/>`,
  },
  2: {
    id: 2,
    content: <Button type="primary" svgName="right" svgPos="right" text="Primary Button with Icon"/>,
    code: `<Button type="primary" svgName="right" svgPos="right" text="Primary Button with Icon"/>`,
  },
  3: {
    id: 3,
    content: <Button type="secondary" text="Primary Button"/>,
    code: `<Button type="secondary" text="Primary Button"/>`,
  },
  4: {
    id: 4,
    content: <Button type="secondary" svgName="right" svgPos="right" text="Primary Button with Icon"/>,
    code: `<Button type="secondary" svgName="right" svgPos="right" text="Primary Button with Icon"/>`,
  },
  5: {
    id: 5,
    content: <Button type="text" text="Text Button"/>,
    code: `<Button type="text" text="Text Button"/>`,
  },
  6: {
    id: 6,
    content: <Button type="text" svgName="delete" text="Text Button with Icon"/>,
    code: `<Button type="text" svgName="delete" text="Text Button with Icon"/>`,
  },
  7: {
    id: 7,
    content: <Button type="link" text="Plain Link"/>,
    code: `<Button type="link" text="Plain Link"/>`,
  },
  8: {
    id: 8,
    content: <Button type="link" hasUnderline text="Plain Link with Underline"/>,
    code: `<Button type="link" hasUnderline text="Plain Link with Underline"/>`,
  },
  9: {
    id: 9,
    content: <Button type="link" svgName="delete" svgPos="right" text="Plain Link with Icon"/>,
    code: `<Button type="link" svgName="delete" svgPos="right" text="Plain Link with Icon"/>`,
  },
  10: {
    id: 10,
    content: <Button type="icon" svgName="delete"/>,
    code: `<Button type="icon" svgName="delete"/>`,
  },
}

const propObject = {
  1: {
    id : 1,
    name: 'type',
    info: 'Select the type of Button needed.',
    type: `enum('primary', 'secondary', 'text', 'link', 'icon')`,
    required: 'Yes'
  },
  2: {
    id : 2,
    name: 'text',
    info: 'Specify the Content the Button should display.',
    type: 'string',
    required: 'Yes'
  },
  3: {
    id : 3,
    name: 'size',
    info: <>Specify the size of the Button. Default value is <span>normal</span></>,
    type:  `enum('small', 'normal')`,
    required: 'No'
  },
  4: {
    id : 4,
    name: 'isDisabled',
    info: <>Determines whether the Button is disabled. Default value is <span>false</span></>,
    type: 'boolen',
    required: 'No'
  },
  5: {
    id : 5,
    name: 'isLink',
    info: <>Determines whether the Button is a Link Component. Default value is <span>false</span></>,
    type: 'boolen',
    required: 'No'
  },
  6: {
    id : 6,
    name: 'hasUnderline',
    info: <>Determines whether the Button has underline. Applicable only when <span>type</span> is <span>Link</span></>,
    type: 'boolen',
    required: 'No'
  },
  7: {
    id : 7,
    name: 'svgName',
    info: 'Specify the SVG that should be displayed in the Button',
    type: 'string',
    required: 'No'
  },
  8: {
    id : 8,
    name: 'svgPos',
    info: <>Specify the Position the SVG should be placed. Default value is <span>left</span> to the content</>,
    type: `enum('left', 'right')`,
    required: 'No'
  },
  9: {
    id : 9,
    name: 'svgSize',
    info: <>Specify the size of the SVG. Default value is <span>16</span></>,
    type: 'interger',
    required: 'No'
  },
  10: {
    id : 10,
    name: 'svgColor',
    info: <>Specify the color of the SVG. Default value is <span>black</span></>,
    type: `enum('white', 'black', 'grey')`,
    required: 'No'
  },
  11: {
    id : 11,
    name: 'isLoading',
    info: <>Determines whether the Button should display loaders. Default value is <span>false</span></>,
    type: 'boolean',
    required: 'No'
  },
  12: {
    id : 12,
    name: 'loaderType',
    info: <>Specify the type of loader that should be displayed inside the Button. Applicable only when <span>isLoading</span> is <span>true</span>. Default value is <span>dots</span></>,
    type: `enum('bar', 'dots', 'spinner')`,
    required: 'No'
  },
  13: {
    id : 13,
    name: 'loadingText',
    info: <>Specify the text to be displayed during Saving State. Applicable only when <span>loaderType</span> is <span>bar</span></>,
    type: 'string',
    required: 'No'
  },
  14: {
    id : 14,
    name: 'inheritFontSize',
    info: 'Determines whether the Button should inherit the Font Size from its Parent',
    type: 'boolean',
    required: 'No'
  },
  15: {
    id : 15,
    name: 'extraClass',
    info: 'Specify any other class that should be applied to the button',
    type: 'string',
    required: 'No'
  }
}

class Buttons extends React.Component {

  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }

  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Buttons</h1>
        </div>
        <div className="code-content__wrapper">

          {/* Import */}

          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />


          {/* Normal Buttons */}

          <ExampleBlock exampleObject={exampleObject} extraSpacing={true}/>
          <CodeReferenceBlock
            exampleObject={exampleObject}
            language="jsx"
            copiedID={copiedID}
            handleCopy={this.handleCopy}
          />


          {/* Loader Buttons */}

          <ExampleBlock title="Loader Examples" exampleObject={loaderExampleObject} extraSpacing={true} isLoaderExample/>
          <CodeReferenceBlock
            title="Loader Code Reference"
            exampleObject={loaderExampleObject}
            language="jsx"
            copiedID={copiedID}
            handleCopy={this.handleCopy}
          />

          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>
        </div>
      </div>
    );
  }
}

export default Buttons;