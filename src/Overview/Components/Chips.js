import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import Chip from '../../Components/SSComponents/Chip';
import ExampleBlock from './Helpers/ExampleBlock';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { Chip } from 'SSComponents';`;

const exampleObject = {
  1: {
    id: 1,
    content: <Chip text="Chip"/>,
    code: `<Chip text="Chip"/>`
  },
  2: {
    id: 2,
    content: <Chip text="Disabled Chip" isDisabled/>,
    code: `<Chip text="Disabled Chip" isDisabled/>`
  },
}

const propObject = {
  1: {
    id : 1,
    name: 'text',
    info: 'Specify the text to be displayed inside the Chip',
    type: 'string',
    required: 'Yes'
  },
  2: {
    id : 2,
    name: 'onDelete',
    info: 'Specify what should happen when the Chip is removed',
    type: 'function',
    required: 'Yes'
  },
  3: {
    id : 3,
    name: 'isDisabled',
    info: <>Determines whether the Chip is disabled. Default value is <span>false</span></>,
    type: 'boolean',
    required: 'No'
  }
}

class Chips extends React.Component {

  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }

  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Chip</h1>
        </div>
        <div className="code-content__wrapper">

          {/* Import */}

          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />

          <ExampleBlock exampleObject={exampleObject}/>
          <CodeReferenceBlock
            exampleObject={exampleObject}
            language="jsx"
            copiedID={copiedID}
            handleCopy={this.handleCopy}
          />
          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>
        </div>
      </div>
    );
  }
}

export default Chips;