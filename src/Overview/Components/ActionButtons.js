import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import ExampleBlock from './Helpers/ExampleBlock';
import { ActionButton } from '../../Components/SSComponents';
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { ActionButton } from 'SSComponents';`;

const exampleObject = {
  1: {
    id: 1,
    content: <ActionButton text="ActionButton"/>,
    code: `<ActionButton text="ActionButton"/>`,
  },
  2: {
    id: 2,
    content: <ActionButton svgName="arrowright" text="ActionButton with Other Icon"/>,
    code: `<ActionButton svgName="arrowright" text="ActionButton with Other Icon"/>`,
  },
}

const propObject = {
  2: {
    id : 2,
    name: 'text',
    info: 'Specify the Content the ActionButton should display.',
    type: 'string',
    required: 'Yes'
  },
  4: {
    id : 4,
    name: 'isDisabled',
    info: <>Determines whether the ActionButton is disabled. Default value is <span>false</span></>,
    type: 'boolen',
    required: 'No'
  },
  5: {
    id : 5,
    name: 'isLink',
    info: <>Determines whether the ActionButton is a Link Component. Default value is <span>false</span></>,
    type: 'boolen',
    required: 'No'
  },
  7: {
    id : 7,
    name: 'svgName',
    info: <>Specify the SVG that should be displayed in the ActionButton. Default SVG is <span>tick</span></>,
    type: 'string',
    required: 'No'
  },
  9: {
    id : 9,
    name: 'svgSize',
    info: <>Specify the size of the SVG. Default value is <span>16</span></>,
    type: 'interger',
    required: 'No'
  },
  10: {
    id : 10,
    name: 'svgColor',
    info: <>Specify the color of the SVG. Default value is <span>black</span></>,
    type: `enum('white', 'black', 'grey')`,
    required: 'No'
  },
  11: {
    id : 11,
    name: 'isLoading',
    info: <>Determines whether the ActionButton should display loaders. Default value is <span>false</span></>,
    type: 'boolean',
    required: 'No'
  },
  12: {
    id : 12,
    name: 'loaderType',
    info: <>Specify the type of loader that should be displayed inside the ActionButton. Applicable only when <span>isLoading</span> is <span>true</span>. Default value is <span>dots</span></>,
    type: `enum('bar', 'dots', 'spinner')`,
    required: 'No'
  },
  13: {
    id : 13,
    name: 'loadingText',
    info: <>Specify the text to be displayed during Saving State. Applicable only when <span>loaderType</span> is <span>bar</span></>,
    type: 'string',
    required: 'No'
  },
  15: {
    id : 15,
    name: 'extraClass',
    info: 'Specify any other class that should be applied to the ActionButton',
    type: 'string',
    required: 'No'
  }
}

class ActionActionButtons extends React.Component {

  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }

  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Action Button</h1>
        </div>
        <div className="code-content__wrapper">

          {/* Import */}

          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />


          {/* Normal ActionActionButtons */}

          <ExampleBlock exampleObject={exampleObject} extraSpacing={true}/>
          <CodeReferenceBlock
            exampleObject={exampleObject}
            language="jsx"
            copiedID={copiedID}
            handleCopy={this.handleCopy}
          />

          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>
        </div>
      </div>
    );
  }
}

export default ActionActionButtons;