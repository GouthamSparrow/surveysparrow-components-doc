import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import PageLoader from '../../Components/SSComponents/PageLoader';
import Button from '../../Components/SSComponents/Button';
import SectionTitle from './Helpers/SectionTitle';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { PageLoader } from 'SSComponents';`;

const exampleObject = {
  1: {
    id: 1,
    // content: <PageLoader text="Component Library is Loading"/>,
    code: `<PageLoader text="Component Library is Loading"/>`
  },
}

const propObject = {
  1: {
    id : 1,
    name: 'text',
    info: 'Specify the text to be displayed inside the PageLoader',
    type: 'string',
    required: 'Yes'
  },
}

class PageLoaders extends React.Component {

  constructor(){
    super();
    this.state = {
      copiedID: 0,
      showDemo: false
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();

    if(this.state.showDemo) {
      setTimeout(() => {
        this.setDemoStatus(false);
      }, 2000);
    }
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }

  setDemoStatus = (status) => {
    this.setState({
      showDemo: status
    })
  }

  render() {
    const { copiedID, showDemo } = this.state;
    if(showDemo) {
      return (
        <div className="ss-clib-main-content-container--demo-wrapper">
          <PageLoader text="Component Library is Loading"/>
        </div>
      )
    }
    else {
      return (
        <div className="ss-clib-main-content-container">
          <div className="code-content__header">
            <h1 className="ss-heading-text ss-text__family--serif">Page Loader</h1>
          </div>
          <div className="code-content__wrapper">
  
            {/* Import */}
  
            <CodeBlock
              title="Import"
              item={-1}
              copiedID={copiedID}
              importCode={importCode}
              handleCopy={this.handleCopy}
            />
  
            <div className="code-content__section">
              <SectionTitle
                title="Demo"
              />
              <div className="mt--xl">
                <Button type="primary" text="Trigger Loader" onClick={() => this.setDemoStatus(true)}/>
              </div>
            </div>
  
            <CodeReferenceBlock
              exampleObject={exampleObject}
              language="jsx"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
            />
            <PropsList propObject={propObject}/>
            <PropsReference propObject={propObject}/>
          </div>
        </div>
      );
    }
  }
}

export default PageLoaders;