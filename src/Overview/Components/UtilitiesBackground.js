import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import PreviewBlock from './Helpers/UtilityPreview';

const exampleObjectBackground = {
  1: {
    id: 1,
    content: <div class="utility-preview--container fx-row fx-jc--around">
    <PreviewBlock extraClass="utility-preview--item--dimensions background-color--grey"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--dimensions background-color--swing"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--dimensions background-color--orange"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--dimensions background-color--yellow"></PreviewBlock>
    </div>,
    code: 
`<div class="background-color--grey"></div>
<div class="background-color--swing"></div>
<div class="background-color--orange"></div>
<div class="background-color--yellow"></div>`
  },
}
class Background extends React.Component {
    constructor(){
      super();
      this.state = {
        copiedID: 0,
      }
    }
  
    componentDidMount() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
    componentDidUpdate() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
  
    handleCopy = (id, value) => {
      copy(value);
      this.setState({
        copiedID: id
      })
      setTimeout(() => {
        this.setState({
          copiedID: 0
        })
      }, 2000);
    }
    render() {
      const { copiedID } = this.state;
      return (
        <div className="ss-clib-main-content-container">
          <div className="code-content__header">
            <h1 className="ss-heading-text ss-text__family--serif">Background</h1>
          </div>
          <div className="code-content__wrapper">
            <CodeReferenceBlock
              title={"Background"}
              info={"Use the Background class to add a Background to an item."}
              exampleObject={exampleObjectBackground}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          </div>
        </div>
      );
    }
  }
  
  export default Background;