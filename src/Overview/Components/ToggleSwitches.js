import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import ToggleSwitch from '../../Components/SSComponents/ToggleSwitch';
import ExampleBlock from './Helpers/ExampleBlock';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { ToggleSwitch } from 'SSComponents';`;


const exampleObject = {
    1: {
      id: 1,
      content: <ToggleSwitch name="switchlabel" label="Toggle Switch with label"/>,
      code: `<ToggleSwitch name="switchlabel" label="Toggle Switch with label"/>`
    },
    2: {
      id: 2,
      content: <ToggleSwitch label="Disabled" name="switchdisabled" disabled/>,
      code: `<ToggleSwitch name="switchdisabled" disabled/>`
    },
    3: {
      id: 3,
      content: <ToggleSwitch name="info" label="Toggle Switch with label and info" inputInfo="Info here" />,
      code: `<ToggleSwitch name="info" disabled/>`
    },
}
const propObject = {
    1: {
      id : 1,
      name: 'name',
      info: 'Specify the name of toggle switch.',
      type: 'string',
      required: 'Yes'
    },
    2: {
      id : 2,
      name: 'label',
      info: 'Specify the label of toggle switch.',
      type: 'string',
      required: 'No'
    },
    3: {
      id : 3,
      name: 'disabled',
      info: 'Specify whether the switch is disabled or not.',
      type: 'string',
      required: 'No'
    },
    4: {
        id : 4,
        name: 'inputInfo',
        info: 'Specify the helper text for the switch if needed..',
        type: 'string',
        required: 'No'
      },
    5: {
        id : 5,
        name: 'extraClass',
        info: 'Specify extra classes for switch.',
        type: 'string',
        required: 'No'
      },
}
class ToggleSwitches extends React.Component {
  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }
  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">ToggleSwitch</h1>
        </div>
        <div className="code-content__wrapper">
          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />
          <ExampleBlock exampleObject={exampleObject} extraSpacing/>
          <CodeReferenceBlock
              exampleObject={exampleObject}
              language="jsx"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
          />
          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>
        </div>
      </div>
    
    );
  }
}

export default ToggleSwitches;