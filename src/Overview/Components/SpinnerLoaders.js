import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import SpinnerLoader from '../../Components/SSComponents/SpinnerLoader';
import ExampleBlock from './Helpers/ExampleBlock';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { SpinnerLoader } from 'SSComponents';`;

const exampleObject = {
  1: {
    id: 1,
    content: <SpinnerLoader size={24}/>,
    code: `<SpinnerLoader size={24}/>`
  },
  2: {
    id: 2,
    content: <SpinnerLoader size={40}/>,
    code: `<SpinnerLoader size={40}/>`
  },
  3: {
    id: 3,
    content: <SpinnerLoader size={72}/>,
    code: `<SpinnerLoader size={72}/>`
  },
  4: {
    id: 4,
    content: <SpinnerLoader size={104}/>,
    code: `<SpinnerLoader size={104}/>`
  },
}

const propObject = {
  1: {
    id : 1,
    name: 'size',
    info: 'Specify the size of the SpinnerLoader Container',
    type: 'number',
    required: 'Yes'
  },
}

class SpinnerLoaders extends React.Component {

  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }

  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Spinner</h1>
        </div>
        <div className="code-content__wrapper">

          {/* Import */}

          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />

          <ExampleBlock exampleObject={exampleObject}/>
          <CodeReferenceBlock
            exampleObject={exampleObject}
            language="jsx"
            copiedID={copiedID}
            handleCopy={this.handleCopy}
          />
          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>
        </div>
      </div>
    );
  }
}

export default SpinnerLoaders;