import React from 'react';
import hljs from 'highlight.js';
import 'highlight.js/styles/github.css';
import ExampleBlock from './Helpers/ExampleBlock';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';

import copy from 'copy-to-clipboard';
const exampleObject = {
  1: {
    id: 1,
    title: 'Normal Table',
    content: 
<table className="ss-table">
  <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Designation</th>
      <th>Phone</th>
      <th>Mobile</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Bruce Wayne</td>
      <td>bruce@wayne.com</td>
      <td>Private Detective</td>
      <td>+91 9876543210</td>
      <td>+91 9876543210</td>
      </tr>
    <tr>
      <td>Barry Allern</td>
      <td>barry@allen.com</td>
      <td>CSI Agent</td>
      <td>+91 9182736450</td>
      <td>+91 9182736450</td>
    </tr>
    <tr>
      <td>Tony Stark</td>
      <td>tony@stark.com</td>
      <td>Philanthropist</td>
      <td>+91 9988776655</td>
      <td>+91 9988776655</td>
    </tr>
    <tr>
      <td>Steve Rogers</td>
      <td>steve@rogers.com</td>
      <td>Captain</td>
      <td>+91 9753124680</td>
      <td>+91 9753124680</td>
    </tr>
  </tbody>
</table>,
    code: `
<table className="ss-table">
  <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Designation</th>
      <th>Phone</th>
      <th>Mobile</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Bruce Wayne</td>
      <td>bruce@wayne.com</td>
      <td>Private Detective</td>
      <td>+91 9876543210</td>
      <td>+91 9876543210</td>
      </tr>
    <tr>
      <td>Barry Allern</td>
      <td>barry@allen.com</td>
      <td>CSI Agent</td>
      <td>+91 9182736450</td>
      <td>+91 9182736450</td>
    </tr>
    <tr>
      <td>Tony Stark</td>
      <td>tony@stark.com</td>
      <td>Philanthropist</td>
      <td>+91 9988776655</td>
      <td>+91 9988776655</td>
    </tr>
    <tr>
      <td>Steve Rogers</td>
      <td>steve@rogers.com</td>
      <td>Captain</td>
      <td>+91 9753124680</td>
      <td>+91 9753124680</td>
    </tr>
  </tbody>
</table>`
  },
  2: {
    id: 2,
    title: 'Table With Image',
    content: 
<table className="ss-table ss-table--image ss-table--fixed-layout">
  <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Designation</th>
      <th>Phone</th>
      <th>Mobile</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td>
            <div className="img-container"><span>BW</span></div>Bruce Wayne
        </td>
        <td>bruce@wayne.com</td>
        <td>Private Detective</td>
        <td>+91 9876543210</td>
        <td>+91 9876543210</td>
    </tr>
    <tr>
        <td>
            <div className="img-container"><span>BW</span></div>Bruce Wayne
        </td>
        <td>bruce@wayne.com</td>
        <td>Private Detective</td>
        <td>+91 9876543210</td>
        <td>+91 9876543210</td>
    </tr>
    <tr>
        <td>
            <div className="img-container"><span>BW</span></div>Bruce Wayne
        </td>
        <td>bruce@wayne.com</td>
        <td>Private Detective</td>
        <td>+91 9876543210</td>
        <td>+91 9876543210</td>
    </tr>
  </tbody>
</table>,
    code: `
<table className="ss-table ss-table--image ss-table--fixed-layout">
  <thead>
    <tr>
      <th>Name</th>
      <th>Email</th>
      <th>Designation</th>
      <th>Phone</th>
      <th>Mobile</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td>
            <div className="img-container"><span>BW</span></div>Bruce Wayne
        </td>
        <td>bruce@wayne.com</td>
        <td>Private Detective</td>
        <td>+91 9876543210</td>
        <td>+91 9876543210</td>
    </tr>
    <tr>
        <td>
            <div className="img-container"><span>BW</span></div>Bruce Wayne
        </td>
        <td>bruce@wayne.com</td>
        <td>Private Detective</td>
        <td>+91 9876543210</td>
        <td>+91 9876543210</td>
    </tr>
    <tr>
        <td>
            <div className="img-container"><span>BW</span></div>Bruce Wayne
        </td>
        <td>bruce@wayne.com</td>
        <td>Private Detective</td>
        <td>+91 9876543210</td>
        <td>+91 9876543210</td>
    </tr>
  </tbody>
</table>`
  },
  3: {
    id: 3,
    title: 'Table with Loader',
    content: 
<table className="ss-table ss-table--fixed-layout ss-table--checkbox ss-table--image">
  <thead>
    <tr>
      <th></th>
      <th>Name</th>
      <th>Email</th>
      <th>Designation</th>
      <th>Phone</th>
      <th>Mobile</th>
    </tr>
  </thead>
  <tbody>
    <tr className="ss-table--row__loading">
      <td></td>
      <td>
          <div className="img-container"><span></span></div>
      </td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr className="ss-table--row__loading">
      <td></td>
      <td>
          <div className="img-container"><span></span></div>
      </td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr className="ss-table--row__loading">
      <td></td>
      <td>
          <div className="img-container"><span></span></div>
      </td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr className="ss-table--row__loading">
      <td></td>
      <td>
          <div className="img-container"><span></span></div>
      </td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>,
    code: `
<table className="ss-table ss-table--fixed-layout ss-table--checkbox ss-table--image">
  <thead>
    <tr>
      <th></th>
      <th>Name</th>
      <th>Email</th>
      <th>Designation</th>
      <th>Phone</th>
      <th>Mobile</th>
    </tr>
  </thead>
  <tbody>
    <tr className="ss-table--row__loading">
      <td></td>
      <td>
          <div className="img-container"><span></span></div>
      </td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr className="ss-table--row__loading">
      <td></td>
      <td>
          <div className="img-container"><span></span></div>
      </td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr className="ss-table--row__loading">
      <td></td>
      <td>
          <div className="img-container"><span></span></div>
      </td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr className="ss-table--row__loading">
      <td></td>
      <td>
          <div className="img-container"><span></span></div>
      </td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>`
  },
}
class Tables extends React.Component {
  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }

  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Tables</h1>
        </div>
        <div className="code-content__wrapper">
          <ExampleBlock
            exampleObject={exampleObject}
            fullWidth
            isTableExample
          />
          <CodeReferenceBlock
            exampleObject={exampleObject}
            language="html"
            copiedID={copiedID}
            handleCopy={this.handleCopy}
            fullWidth
            extraSpacing
          />
        </div>
      </div>
    );
  }
}

export default Tables;