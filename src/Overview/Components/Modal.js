import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import Modal from '../../Components/SSComponents/Modal';
import Input from '../../Components/SSComponents/Input';
import Text from '../../Components/SSComponents/Text';
import Button from '../../Components/SSComponents/Button';
import SectionTitle from './Helpers/SectionTitle';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import CodeBlock from './Helpers/CodeBlock';
import classnames from 'classnames';

const importCode = `import { Modal } from 'SSComponents';`;

const ModalContent = ({size, title, content, close}) => {

  const isSmall = size === 'small';
  const isMedium = size === 'medium';
  const isLarge = size === 'large';
  const isFullPage = size === 'full-page';

  return (
    <div className="ss-clib-modal-content">
      
      <Text size="jumbo" color="black" extraClass={classnames({"mb--sm": true, "mb--lg": !isSmall })}>{title}</Text>
      <Text size="h3" color="grey" extraClass="mb--xl">{content}</Text>

      {/* To double the content */}
      {isFullPage && <Text size="h3" color="grey" extraClass="mb--xl">{content}</Text>} 
      
      <div className="grid">
        <div className={classnames({
          'col modal-form-group': true,
          'col-12': isSmall || isMedium,
          'col-8': isLarge,
          'col-6': isFullPage,
        })}>
          <div className="grid grid-nogutter">
            <div className="col col-6 modal-form-item">
              <Input label={'Input with Label and Info'} inputInfo={'This is an Input Info'}/>
            </div>
            <div className="col col-6 modal-form-item">
              <Input label={'Input with Label and Info'} inputInfo={'This is an Input Info'}/>
            </div>
            {!isSmall &&
            <>
              <div className="col col-6 modal-form-item">
                <Input label={'Input with Label and Info'} inputInfo={'This is an Input Info'}/>
              </div>
              <div className="col col-6 modal-form-item">
                <Input label={'Input with Label and Info'} inputInfo={'This is an Input Info'}/>
              </div>
              </>
            }
          </div>
        </div>
      </div>
    </div>
  )
}

const exampleObject = {
  1: {
    id: 1,
    size: 'small',
    title: 'Small Modal',
    content: `Being evil has a price. I hear a lot of little secrets. Tell me yours, and I'll keep it. Once in every lifetime, comes a love like this.`,
  },
  2: {
    id: 2,
    size: 'medium',
    title: 'Medium Modal',
    content: `Once in every lifetime, comes a love like this. Oh I need you, you need me, oh my darling can't you see. Young Ones. Darling we're the Young Ones. Young Ones. Shouldn't be afraid. To live, love, there's a song to be sung. Cause we may not be the Young Ones very long. Your tread must be light and sure, as though your path were upon rice paper. It is said, a Shaolin priest can walk through walls. Looked for, he can not be seen. Listened for, he can not be heard. Touched, can not be felt. This rice paper is the test. Fragile as the wings of the dragonfly, clinging as the cocoon of the silk worm. When you can walk its length and leave no trace. You will have learned.`,
  },
  3: {
    id: 3,
    size: 'large',
    title: 'Large Modal',
    content: `My kinda people, my kinda place. There's something special about this place. Got no reason to stray too far, 'cause it's all right here in my own backyard! This is a Burger King town, it's made just for me! This is a Burger King town, we know how burgers should be! Right up the road, left at the sign. My way, your way, one at a time, hot off the fire with anything on it! And don't it feel good when it's just how you want it? This is a Burger King town, it's made just for me! This is a Burger King town, we know how burgers should be! I am Duncan Macleod, born 400 years ago in the Highlands of Scotland. I am Immortal, and I am not alone. For centuries, we have waited for the time of the Gathering when the stroke of a sword and the fall of a head will release the power of the Quickening. In the end, there can be only one. Come and knock on our door. We've been waiting for you. Where the kisses are hers and hers and his, three's company, too! Come and dance on our floor. Take a step that is new. We've a lovable space that needs your face, three's company, too! You'll see that life is a ball again and laughter is callin' for you. Down at our rendezvous, three's company, too!`,
  },
  4: {
    id: 4,
    size: 'full-page',
    title: 'Full Page Modal',
    content: `Green Acres is the place to be. Farm livin' is the life for me. Land spreadin' out so far and wide. Keep Manhattan, just give me that countryside. New York is where I'd rather stay. I get allergic smelling hay. I just adore a penthouse view. Darling I love you but give me Park Avenue. The chores! The stores! Fresh air! Times Square! You are my wife. Good bye, city life. Green Acres we are there! Sunny day, sweepin' the clouds away. On my way to where the air is sweet. Can you tell me how to get, how to get to Sesame Street? Come and play, everything's A-OK. Friendly neighbors there, that's where we meet. Can you tell me how to get, how to get to Sesame Street? Well we're movin' on up, to the east side. To a deluxe apartment in the sky. Movin' on up, To the east side. We finally got a piece of the pie. Fish don't fry in the kitchen, beans don't burn on the grill. Took a whole lotta tryin' just to get up that hill. Now we're up in the big leagues, gettin' our turn at bat. As long as we live, it's you and me baby, There ain't nothin' wrong with that. Well we're movin' on up, to the east side. To a deluxe apartment in the sky. Movin' on up, to the east side. We finally got a piece of the pie.`,
  },
  5: {
    id: 5,
    size: 'small',
    title: 'Modal with Loader',
    isLoading: true,
    content: `Being evil has a price. I hear a lot of little secrets. Tell me yours, and I'll keep it. Once in every lifetime, comes a love like this.`,
  },
  6: {
    id: 6,
    size: 'small',
    title: 'Modal without Close Button',
    hideCloseButton: true,
    content: `Being evil has a price. I hear a lot of little secrets. Tell me yours, and I'll keep it. Once in every lifetime, comes a love like this.`,
  },
  7: {
    id: 7,
    size: 'small',
    title: 'Modal with Submit Loader',
    submitIsLoading: true,
    content: `Being evil has a price. I hear a lot of little secrets. Tell me yours, and I'll keep it. Once in every lifetime, comes a love like this.`,
  },
}

const codeObject = {
  1: {
    id: 1,
    content: 'Small Modal',
    code: `<Modal size="small"   show={this.state.showModal}   close={() => this.handleModalClose}/>`
  },
  2: {
    id: 2,
    content: 'Medium Modal',
    code: `<Modal size="medium" show={this.state.showModal}   close={() => this.handleModalClose}/>`
  },
  3: {
    id: 3,
    content: 'Large Modal',
    code: `<Modal size="large"   show={this.state.showModal}   close={() => this.handleModalClose}/>`
  },
  4: {
    id: 4,
    content: 'Full Page Modal',
    code: `<Modal size="full-page" show={this.state.showModal}   close={() => this.handleModalClose}/>`
  },
  5: {
    id: 5,
    content: 'Modal with Loader',
    code: `<Modal size="small" show={this.state.showModal} isLoading close={() => this.handleModalClose}/>`
  },
  6: {
    id: 6,
    content: 'Modal with Close Button Hidden',
    code: `<Modal size="small" show={this.state.showModal} hideCloseButton close={() => this.handleModalClose}/>`
  },
  7: {
    id: 7,
    content: 'Modal with Submit Loader',
    code: `<Modal size="small" show={this.state.showModal} submitIsLoading close={() => this.handleModalClose}/>`
  },
}

const propObject = {
  1: {
    id : 1,
    name: 'size',
    info: <>Specify the size of the Modal. Default size is <span>small</span></>,
    type: `enum('small', 'medium', 'large', 'full-page')`,
    required: 'No'
  },
  2: {
    id : 2,
    name: 'show',
    info: 'Determines whether the Modal is to be shown or not.',
    type: 'boolean',
    required: 'Yes'
  },
  3: {
    id : 3,
    name: 'close',
    info: <>Specify the action to be given to the <span>close</span> button of the Modal</>,
    type: 'function',
    required: 'Yes'
  },
  4: {
    id:4,
    name:'header',
    info: <>Specify the header to the Modal</>,
    type: 'function',
    required: 'Yes'
  },
  5: {
    id:5,
    name:'hideCloseButton',
    info: <>Hide close button in modal.</>,
    type: 'boolean',
    required: 'No',
  },
  6: {
    id:6,
    name:'onCancel',
    info: <>Specify action on cancel.</>,
    type: 'function',
    required: 'No'
  },
  7: {
    id:7,
    name:'autoHeight',
    info: <>Determines whether modal will have auto height.</>,
    required: 'No'
  },
  8: {
    id:8,
    name:'autoWidth',
    info: <>Determines whether modal will have auto width.</>,
    required: 'No'
  },
  9: {
    id:9,
    name:'hasFooter',
    info: <>Determines whether footer is to be displayed</>,
    type: 'boolean',
    required: 'No'
  },
  10: {
    id:10,
    name:'submitIsLoading',
    info: <>Determines whether submit button will have loader</>,
    type:'boolean',
    required: 'No'
  },
  11: {
    id:11,
    name:'isLoading',
    info: <>Determines whether the modal loader should be shown.</>,
    type: 'boolean',
    required: 'No'
  },
  12: {
    id:12,
    name:'submitText',
    info: <>Determines text to be displayed on submit button </>,
    type: 'string',
    required: 'No'
  },
  13: {
    id:13,
    name:'cancelText',
    info: <>Determines text to be displayed on cancel button </>,
    type: 'string',
    required: 'No'
  },
}

class CustomModal extends React.Component {

  constructor(){
    super();
    this.state = {
      copiedID: 0,
      showModal: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }

  displayModal = (id) => {
    this.setState({
      showModal: id
    })
  }

  handleClose = () => {
    this.setState({
      showModal: 0
    })
  }

  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Modal</h1>
        </div>
        <div className="code-content__wrapper">

          {/* Import */}

          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />
          <div className="code-content__section">
            <SectionTitle
              title="Examples"
            />
            <div className="flex_column code-content__example">
              <div className="grid code-content__example-item mb--xl">
                {
                  Object.keys(exampleObject).map((item, index) => {
                    return (
                      <div key={index}>
                        <Modal
                          size={exampleObject[item].size}
                          show={this.state.showModal === exampleObject[item].id}
                          header={exampleObject[item].title}
                          isLoading={exampleObject[item].isLoading}
                          hideCloseButton={exampleObject[item].hideCloseButton}
                          hasFooter={true}
                          submitText={"Submit"}
                          submitIsLoading={exampleObject[item].submitIsLoading}
                          close={() => this.handleClose(exampleObject[item].id)}
                        >
                          <ModalContent
                            size={exampleObject[item].size}
                            content={exampleObject[item].content}
                            close={() => this.handleClose(exampleObject[item].id)}/>
                        </Modal>
                        <Button onClick={() => this.displayModal(exampleObject[item].id)} extraClass="mb--sm" type="primary" text={`Show ${exampleObject[item].title}`}/>
                      </div>
                    );
                  })
                }
                
              </div>
            </div>
          </div>

          

          <CodeReferenceBlock
            exampleObject={codeObject}
            language="jsx"
            copiedID={copiedID}
            handleCopy={this.handleCopy}
          />
          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>
        </div>
      </div>
    );
  }
}

export default CustomModal;