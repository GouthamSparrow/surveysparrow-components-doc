import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import PreviewBlock from './Helpers/UtilityPreview';

const exampleObjectBorder = {
  1: {
    id: 1,
    title:'Normal Border',
    content: <div class="utility-preview--container fx-row fx-jc--around">
    <PreviewBlock extraClass="utility-preview--item--filled border"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-top"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-right"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-bottom"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-left"></PreviewBlock>
    </div>,
    code: 
`<div class="border"></div>
<div class="border-top"></div>
<div class="border-right"></div>
<div class="border-bottom"></div>
<div class="border-left"></div>`
  },
  2: {
    id: 2,
    title:'Purple Border',
    content: <div class="utility-preview--container fx-row fx-jc--around">
    <PreviewBlock extraClass="utility-preview--item--filled border--purple"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-top--purple"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-right--purple"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-bottom--purple"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-left--purple"></PreviewBlock>
    </div>,
    code: 
`<div class="border--purple"></div>
<div class="border-top--purple"></div>
<div class="border-right--purple"></div>
<div class="border-bottom--purple"></div>
<div class="border-left--purple"></div>`
  },
  3: {
    id: 3,
    title:'Orange Border',
    content: <div class="utility-preview--container fx-row fx-jc--around">
    <PreviewBlock extraClass="utility-preview--item--filled border--orange"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-top--orange"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-right--orange"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-bottom--orange"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-left--orange"></PreviewBlock>
    </div>,
    code: 
`<div class="border--orange"></div>
<div class="border-top--orange"></div>
<div class="border-right--orange"></div>
<div class="border-bottom--orange"></div>
<div class="border-left--orange"></div>`
  },
  4: {
    id: 4,
    title:'Border Zero',
    content: <div class="utility-preview--container fx-row fx-jc--around">
    <PreviewBlock extraClass="utility-preview--item--filled border--zero border"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border border-top--zero"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border border-right--zero"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border border-bottom--zero"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border border-left--zero"></PreviewBlock>
    </div>,
    code: 
`<div class="border--zero"></div>
<div class="border-top--zero"></div>
<div class="border-right--zero"></div>
<div class="border-bottom--zero"></div>
<div class="border-left--zero"></div>`
  },
  5: {
    id: 5,
    title:'Border Transparent',
    content: <div class="utility-preview--container fx-row fx-jc--around">
    <PreviewBlock extraClass="utility-preview--item--filled border--transparent"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-top--transparent"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-right--transparent"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-bottom--transparent"></PreviewBlock>
    <PreviewBlock extraClass="utility-preview--item--filled border-left--transparent"></PreviewBlock>
    </div>,
    code:
`<div class="border--transparent"></div>
<div class="border-top--transparent"></div>
<div class="border-right--transparent"></div>
<div class="border-bottom--transparent"></div>
<div class="border-left--transparent"></div>`
  },
}
class Border extends React.Component {
    constructor(){
      super();
      this.state = {
        copiedID: 0,
      }
    }
  
    componentDidMount() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
    componentDidUpdate() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
  
    handleCopy = (id, value) => {
      copy(value);
      this.setState({
        copiedID: id
      })
      setTimeout(() => {
        this.setState({
          copiedID: 0
        })
      }, 2000);
    }
    render() {
      const { copiedID } = this.state;
      return (
        <div className="ss-clib-main-content-container">
          <div className="code-content__header">
            <h1 className="ss-heading-text ss-text__family--serif">Border</h1>
          </div>
          <div className="code-content__wrapper">
            <CodeReferenceBlock
              title={"Border"}
              info={"Use the border class to add a border to an item."}
              exampleObject={exampleObjectBorder}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          </div>
        </div>
      
      );
    }
  }
  
  export default Border;