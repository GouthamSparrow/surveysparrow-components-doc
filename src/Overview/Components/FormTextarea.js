import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import Textarea from '../../Components/SSComponents/Textarea';
import ExampleBlock from './Helpers/ExampleBlock';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { Textarea } from 'SSComponents';`;

const exampleObject = {
  1: {
    id: 1,
    content: <Textarea placeholder={'Textarea without Label'}/>,
    code: `<Textarea placeholder={'Textarea without Label'}/>`
  },
  2: {
    id: 2,
    content: <Textarea label={'Textarea with Label'}/>,
    code: `<Textarea label={'Textarea with Label'}/>`
  },
  3: {
    id: 3,
    content: <Textarea label={'Textarea with Label and Info'} inputInfo={'This is an Textarea Info'}/>,
    code: `<Textarea label={'Textarea with Label and Info'} inputInfo={'This is an Textarea Info'}/>`
  },
  4: {
    id: 4,
    content: <Textarea label={'Textarea with Value'} value={'dev@surveysparrow.com'}/>,
    code: `<Textarea label={'Textarea with Value'} value={'dev@surveysparrow.com'}/>`
  },
  5: {
    id: 5,
    content: <Textarea label={'Textarea with Character Limit'} maxLength={20} value={'Lorem Ipsum'}/>,
    code: `<Textarea label={'Textarea with Character Limit'} maxLength={20} value={'Lorem Ipsum'}/>`
  },
  6: {
    id: 6,
    content: <Textarea label={'Disabled Textarea'} value={'You Cannot Edit Me!'} isDisabled/>,
    code: `<Textarea label={'Disabled Textarea'} value={'You Cannot Edit Me!'} isDisabled/>`
  },
  7: {
    id: 7,
    content: <Textarea label={'Textarea with Error'} error={'Please Enter a Value'}/>,
    code: `<Textarea label={'Textarea with Error'} error={'Please Enter a Value'}/>`
  },
}

const propObject = {
  1: {
    id : 1,
    name: 'label',
    info: 'Specify the text to be displayed above the <textarea>',
    type: 'string',
    required: 'No'
  },
  2: {
    id : 2,
    name: 'inputInfo',
    info: 'Specify the helper text for the <textarea> if needed.',
    type: 'string',
    required: 'No'
  },
  3: {
    id : 3,
    name: 'isDisabled',
    info: <>Determines whether the &lt;textarea&gt; is disabled. Default value is <span>false</span></>,
    type: 'boolean',
    required: 'No'
  },
  4: {
    id : 4,
    name: 'error',
    info: 'Determines whether there is any error in the <textarea> and shows the error message.',
    type: 'string',
    required: 'No'
  },
  5: {
    id : 5,
    name: 'maxLength',
    info: 'Specifies the maximum number of characters allowed in the <textarea> element.',
    type: 'number',
    required: 'No'
  },
  6: {
    id : 6,
    name: 'noPadding',
    info: 'Determines whether the Padding of the <textarea> should be removed',
    type: 'boolean',
    required: 'No'
  },
  7: {
    id : 7,
    name: 'noMargin',
    info: 'Determines whether the Margin of the <textarea> should be removed',
    type: 'boolean',
    required: 'No'
  },
  8: {
    id : 8,
    name: 'extraClass',
    info: 'Specify any other class that should be applied to the <textarea>',
    type: 'string',
    required: 'No'
  }
}

class FormTextarea extends React.Component {

  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }

  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Textarea</h1>
        </div>
        <div className="code-content__wrapper">

          {/* Import */}

          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />


          <ExampleBlock exampleObject={exampleObject}/>
          <CodeReferenceBlock
            exampleObject={exampleObject}
            language="jsx"
            copiedID={copiedID}
            handleCopy={this.handleCopy}
          />
          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>
        </div>
      </div>
    );
  }
}

export default FormTextarea;