import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import FlexboxPreview from './Helpers/FlexPreview';

const exampleObject = {
    1: {
      id: 1,
      title:'Flex Row',
      content: <><FlexboxPreview extraClass="fx-row fx-jc--start"></FlexboxPreview></>,
      code: `<div class="fx-row">{children}</div>`
    },
    2: {
      id: 2,
      title:'Flex Column',
      content: <><FlexboxPreview type="large" extraClass="fx-column fx-jc--start"></FlexboxPreview></>,
      code: `<div class="fx-column">{children}</div>`
    },
}
class direction extends React.Component {
    constructor(){
      super();
      this.state = {
        copiedID: 0,
      }
    }
  
    componentDidMount() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
    componentDidUpdate() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
  
    handleCopy = (id, value) => {
      copy(value);
      this.setState({
        copiedID: id
      })
      setTimeout(() => {
        this.setState({
          copiedID: 0
        })
      }, 2000);
    }
    render() {
      const { copiedID } = this.state;
      return (
        <div className="ss-clib-main-content-container">
          <div className="code-content__header">
            <h1 className="ss-heading-text ss-text__family--serif">Flex Direction</h1>
          </div>
          <div className="code-content__wrapper">
            <CodeReferenceBlock
              title={"Direction"}
              info={"Set the direction of flex items in a flex container with direction utilities. Use .fx-row to set a horizontal direction, .fx-column to set a vertical direction. "}
              exampleObject={exampleObject}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          </div>
        </div>
      
      );
    }
  }
  
  export default direction;