import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import PropsReference from './Helpers/PropsReference';
import PropsList from './Helpers/PropsList';
import Radio from '../../Components/SSComponents/Radio';
import ExampleBlock from './Helpers/ExampleBlock';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import CodeBlock from './Helpers/CodeBlock';

const importCode = `import { Radio } from 'SSComponents';`;

const exampleObject = {
  1: {
    id: 1,
    title: 'Radio Button',
    content: 
    <>
    <Radio extraClass="mb--lg">
      <Radio.Item name="test" label="Item 1"/>
      <Radio.Item name="test" label="Item 2"/>
      <Radio.Item name="test" label="Item 3"/>
      <Radio.Item name="test" label="Item 4"/>
  </Radio>
  </>,
    code: 
    `
  <Radio>
    <Radio.Item name="name" label="Item 1"/>
    <Radio.Item name="name" label="Item 2"/>
    <Radio.Item name="name" label="Item 3"/>
    <Radio.Item name="name" label="Item 4"/>
  </Radio>`
  },
  2: {
    id: 2,
    title: 'Radio Button Stacked', 
    content: 
    <>
    <Radio stacked>
      <Radio.Item name="test" label="Item 1"/>
      <Radio.Item name="test" label="Item 2"/>
      <Radio.Item name="test" label="Item 3"/>
      <Radio.Item name="test" label="Item 4"/>
  </Radio>
  </>,
    code: 
    `
    <Radio stacked>
      <Radio.Item name="test" label="Item 1"/>
      <Radio.Item name="test" label="Item 2"/>
      <Radio.Item name="test" label="Item 3"/>
      <Radio.Item name="test" label="Item 4"/>
    </Radio>`
  },
  3: {
    id: 3,
    title: 'Radio Button Default Checked', 
    content: 
    <>
    <Radio stacked>
      <Radio.Item name="test1" label="Item 1" isChecked/>
      <Radio.Item name="test1" label="Item 2"/>
      <Radio.Item name="test1" label="Item 3"/>
      <Radio.Item name="test1" label="Item 4"/>
    </Radio>
  </>,
    code: 
    `
    <Radio stacked>
      <Radio.Item name="test1" label="Item 1" isChecked/>
      <Radio.Item name="test1" label="Item 2"/>
      <Radio.Item name="test1" label="Item 3"/>
      <Radio.Item name="test1" label="Item 4"/>
    </Radio>`
  },
  4: {
    id: 4,
    title: 'Small Radio Button', 
    content: 
    <>
    <Radio>
      <Radio.Item size="small" name="test" label="Item 1"/>
      <Radio.Item size="small" name="test" label="Item 2"/>
    </Radio>
  </>,
    code: 
    `
    <Radio>
      <Radio.Item size="small" name="test" label="Item 1"/>
      <Radio.Item size="small" name="test" label="Item 2"/>
    </Radio>`
  },
  5: {
    id: 5,
    title: 'Large Radio Button', 
    content: 
    <>
    <Radio>
      <Radio.Item size="large" name="test" label="Item 1"/>
      <Radio.Item size="large" name="test" label="Item 2"/>
    </Radio>
  </>,
    code: 
    `
    <Radio>
      <Radio.Item size="large" name="test" label="Item 1"/>
      <Radio.Item size="large" name="test" label="Item 2"/>
    </Radio>`
  },
}

const codeRefObject = {
  1: {
    id: 1,
    title: 'Radio Button',
    content: 
    <>
    <Radio extraClass="mb--lg">
      <Radio.Item name="test1" label="Item 1"/>
      <Radio.Item name="test1" label="Item 2"/>
      <Radio.Item name="test1" label="Item 3"/>
      <Radio.Item name="test1" label="Item 4"/>
  </Radio>
  </>,
    code: 
    `
  <Radio>
    <Radio.Item name="name" label="Item 1"/>
    <Radio.Item name="name" label="Item 2"/>
    <Radio.Item name="name" label="Item 3"/>
    <Radio.Item name="name" label="Item 4"/>
  </Radio>`
  },
  2: {
    id: 2,
    title: 'Radio Button Stacked', 
    content: 
    <>
    <Radio stacked>
      <Radio.Item name="test2" label="Item 1"/>
      <Radio.Item name="test2" label="Item 2"/>
      <Radio.Item name="test2" label="Item 3"/>
      <Radio.Item name="test2" label="Item 4"/>
  </Radio>
  </>,
    code: 
    `
    <Radio stacked>
      <Radio.Item name="test" label="Item 1"/>
      <Radio.Item name="test" label="Item 2"/>
      <Radio.Item name="test" label="Item 3"/>
      <Radio.Item name="test" label="Item 4"/>
    </Radio>`
  },
  3: {
    id: 3,
    title: 'Radio Button Default Checked', 
    content: 
    <>
    <Radio stacked>
      <Radio.Item name="test3" label="Item 1" isChecked/>
      <Radio.Item name="test3" label="Item 2"/>
      <Radio.Item name="test3" label="Item 3"/>
      <Radio.Item name="test3" label="Item 4"/>
    </Radio>
  </>,
    code: 
    `
    <Radio stacked>
      <Radio.Item name="test1" label="Item 1" isChecked/>
      <Radio.Item name="test1" label="Item 2"/>
      <Radio.Item name="test1" label="Item 3"/>
      <Radio.Item name="test1" label="Item 4"/>
    </Radio>`
  },
  4: {
    id: 4,
    title: 'Small Radio Button', 
    content: 
    <>
    <Radio>
      <Radio.Item size="small" name="test4" label="Item 1"/>
      <Radio.Item size="small" name="test4" label="Item 2"/>
    </Radio>
  </>,
    code: 
    `
    <Radio>
      <Radio.Item size="small" name="test" label="Item 1"/>
      <Radio.Item size="small" name="test" label="Item 2"/>
    </Radio>`
  },
  5: {
    id: 5,
    title: 'Large Radio Button', 
    content: 
    <>
    <Radio>
      <Radio.Item size="large" name="test5" label="Item 1"/>
      <Radio.Item size="large" name="test5" label="Item 2"/>
    </Radio>
  </>,
    code: 
    `
    <Radio>
      <Radio.Item size="large" name="test" label="Item 1"/>
      <Radio.Item size="large" name="test" label="Item 2"/>
    </Radio>`
  },
}

const propObject = {
  1: {
    id : 1,
    name: 'name',
    info: 'Specify the name of the Radio Button',
    type: 'string',
    required: 'Yes'
  },
  2: {
    id : 2,
    name: 'label',
    info: 'Specify the Label for the Radio Button',
    type: 'string',
    required: 'No'
  },
  3: {
    id : 3,
    name: 'size',
    info: 'Specify the Size of Radio Button',
    type: 'string',
    required: 'No'
  },
  4: {
    id : 4,
    name: 'isChecked',
    info: 'Determines whether the Radio Button is Checked by default.',
    type: 'boolean',
    required: 'No'
  },
  5: {
    id : 5,
    name: 'extraClass',
    info: 'Specify any extra classes for the Radio Button.',
    type: 'string',
    required: 'No'
  },

}

class FormRadio extends React.Component {
  constructor(){
    super();
    this.state = {
      copiedID: 0,
    }
  }

  componentDidMount() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }
  componentDidUpdate() {
    hljs.initHighlighting.called = false;
    hljs.initHighlighting();
  }

  handleCopy = (id, value) => {
    copy(value);
    this.setState({
      copiedID: id
    })
    setTimeout(() => {
      this.setState({
        copiedID: 0
      })
    }, 2000);
  }
  render() {
    const { copiedID } = this.state;
    return (
      <div className="ss-clib-main-content-container">
        <div className="code-content__header">
          <h1 className="ss-heading-text ss-text__family--serif">Radio</h1>
        </div>
        <div className="code-content__wrapper">
          <CodeBlock
            title="Import"
            item={-1}
            copiedID={copiedID}
            importCode={importCode}
            handleCopy={this.handleCopy}
          />
          <ExampleBlock exampleObject={exampleObject} isTableExample/>
          <CodeReferenceBlock
              exampleObject={codeRefObject}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
          />
          <PropsList propObject={propObject}/>
          <PropsReference propObject={propObject}/>
        </div>
      </div>
    
    );
  }
}

export default FormRadio;