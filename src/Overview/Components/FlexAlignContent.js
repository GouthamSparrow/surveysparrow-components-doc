import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import FlexboxPreview from './Helpers/FlexPreview';

const exampleObjectRow = {
    1: {
      id: 1,
      title:'Flex Start',
      content: <><FlexboxPreview type="large" blocks="21" extraClass="fx-row fx--wrap fx-ac--start"></FlexboxPreview></>,
      code: `<div class="fx-row fx-ac--start">{children}</div>`
    },
    2: {
      id: 2,
      title:'Flex End',
      content: <><FlexboxPreview type="large" blocks="21" extraClass="fx-row fx--wrap fx-ac--end"></FlexboxPreview></>,
      code: `<div class="fx-row fx-ac--end">{children}</div>`
    },
    3: {
      id: 3,
      title:'Center',
      content: <><FlexboxPreview type="large" blocks="21" extraClass="fx-row fx--wrap fx-ac--center"></FlexboxPreview></>,
      code: `<div class="fx-row fx-ac--center">{children}</div>`
    },
    4: {
      id: 4,
      title:'Space Between',
      content: <><FlexboxPreview type="large" blocks="21" extraClass="fx-row fx--wrap fx-ac--between"></FlexboxPreview></>,
      code: `<div class="fx-row fx-ac--between">{children}</div>`
    },
    5: {
      id: 5,
      title:'Space Around',
      content: <><FlexboxPreview type="large" blocks="21" extraClass="fx-row fx--wrap fx-ac--around"></FlexboxPreview></>,
      code: `<div class="fx-row fx-ac--around">{children}</div>`
    },
}
const exampleObjectColumn = {
    1: {
      id: 1,
      title:'Flex Start',
      content: <><FlexboxPreview type="large" blocks="10" extraClass="fx-column fx--wrap fx-ac--start"></FlexboxPreview></>,
      code: `<div class="fx-column fx-ac--start">{children}</div>`
    },
    2: {
      id: 2,
      title:'Flex End',
      content: <><FlexboxPreview type="large" blocks="10" extraClass="fx-column fx--wrap fx-ac--end"></FlexboxPreview></>,
      code: `<div class="fx-column fx-ac--end">{children}</div>`
    },
    3: {
      id: 3,
      title:'Center',
      content: <><FlexboxPreview type="large" blocks="10" extraClass="fx-column fx--wrap fx-ac--center"></FlexboxPreview></>,
      code: `<div class="fx-column fx-ac--center">{children}</div>`
    },
    4: {
      id: 4,
      title:'Space Between',
      content: <><FlexboxPreview type="large" blocks="10" extraClass="fx-column fx--wrap fx-ac--between"></FlexboxPreview></>,
      code: `<div class="fx-column fx-ac--between">{children}</div>`
    },
    5: {
      id: 5,
      title:'Space Around',
      content: <><FlexboxPreview type="large" blocks="10" extraClass="fx-column fx--wrap fx-ac--around"></FlexboxPreview></>,
      code: `<div class="fx-column fx-ac--around">{children}</div>`
    },
}
class AlignContent extends React.Component {
    constructor(){
      super();
      this.state = {
        copiedID: 0,
      }
    }
  
    componentDidMount() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
    componentDidUpdate() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
  
    handleCopy = (id, value) => {
      copy(value);
      this.setState({
        copiedID: id
      })
      setTimeout(() => {
        this.setState({
          copiedID: 0
        })
      }, 2000);
    }
    render() {
      const { copiedID } = this.state;
      return (
        <div className="ss-clib-main-content-container">
          <div className="code-content__header">
            <h1 className="ss-heading-text ss-text__family--serif">Flex Align Content</h1>
          </div>
          <div className="code-content__wrapper">
            <CodeReferenceBlock
              title={"Flex Row - Align Content"}
              info={"Use align-content utilities on flexbox containers to align flex items together on the cross axis. To demonstrate these utilities, we’ve enforced flex-wrap: wrap and increased the number of flex items. Heads up! This property has no effect on single rows of flex items."}
              exampleObject={exampleObjectRow}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          <CodeReferenceBlock
              title={"Flex Column - Align Content"}
              info={"Use align-content utilities on flexbox containers to align flex items together on the cross axis. To demonstrate these utilities, we’ve enforced flex-wrap: wrap and increased the number of flex items. Heads up! This property has no effect on single rows of flex items."}
              exampleObject={exampleObjectColumn}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          </div>
        </div>
      
      );
    }
  }
  
  export default AlignContent;