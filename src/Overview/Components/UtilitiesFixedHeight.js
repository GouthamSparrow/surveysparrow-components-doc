import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import PreviewBlock from './Helpers/UtilityPreview';

const exampleObjectBackground = {
  1: {
    id: 1,
    title:'Full Width',
    content: 
    <div class="utility-preview--container fx-row fx-jc--around">
      <PreviewBlock extraClass="fx--fw utility-preview--item--fixed_width background-color--swing"></PreviewBlock>
    </div>,
    code: 
`<div class="fx--fw"></div>`
  },
  2: {
    id: 2,
    title:'Full Height',
    content: 
    <div class="utility-preview--container fx-row fx-jc--around">
      <PreviewBlock extraClass="fx--fh utility-preview--item--fixed_height background-color--swing"></PreviewBlock>
    </div>,
    code: 
`<div class="fx--fh"></div>`
  },
}
class HeightWidth extends React.Component {
    constructor(){
      super();
      this.state = {
        copiedID: 0,
      }
    }
  
    componentDidMount() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
    componentDidUpdate() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
  
    handleCopy = (id, value) => {
      copy(value);
      this.setState({
        copiedID: id
      })
      setTimeout(() => {
        this.setState({
          copiedID: 0
        })
      }, 2000);
    }
    render() {
      const { copiedID } = this.state;
      return (
        <div className="ss-clib-main-content-container">
          <div className="code-content__header">
            <h1 className="ss-heading-text ss-text__family--serif">Full Height and Width</h1>
          </div>
          <div className="code-content__wrapper">
            <CodeReferenceBlock
              title={"Background"}
              info={"Use the fx--fh and fx--fw classes to fill the entire heigth and width of container element."}
              exampleObject={exampleObjectBackground}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          </div>
        </div>
      
      );
    }
  }
  
  export default HeightWidth;