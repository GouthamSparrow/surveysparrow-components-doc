import React from 'react';
import classnames from 'classnames';

class PreviewBlock extends React.Component{
    render(){
        const { extraClass, children } = this.props;
        return (
            <div className={classnames({ 
                "utility-preview--item": true, 
                [extraClass]: extraClass
            })}>
                {children}
            </div>
        );
    }
}

export default PreviewBlock;