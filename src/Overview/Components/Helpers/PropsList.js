import React from 'react';
import SectionTitle from './SectionTitle';

class PropsList extends React.Component {
  render() {
    const { propObject } = this.props;
    return (
      <div className="code-content__section">
        <SectionTitle
          title="Props"
        />
        <ul>
          {
            Object.keys(propObject).map((item) => {
              return (
                <li key={propObject[item].id}><a href={`#${propObject[item].id}`}>{propObject[item].name}</a></li>
              );
            })
          }
        </ul>
      </div>
    );
  }
}

export default PropsList;