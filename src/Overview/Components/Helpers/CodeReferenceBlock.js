import React from 'react';
import SectionTitle from './SectionTitle';
import classnames from 'classnames';
import { Button } from '../../../Components/SSComponents';
import { Text } from '../../../Components/SSComponents';

class ExampleBlock extends React.Component {
  render() {
    const { exampleObject, copiedID, language, handleCopy, title, info, fullWidth } = this.props;
    return (
      <div className="code-content__section">
        <SectionTitle
          title={title ? title : "Code Reference"}
          info={info ? info : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eleifend porta nisl ut egestas. Nunc massa tellus, aliquet sit amet nulla in, feugiat malesuada nulla. Ut viverra quam risus, in dapibus nisl finibus eu. Suspendisse nec pharetra enim, id finibus erat. Donec facilisis lorem augue, id blandit velit volutpat vel."}
        />
        <div className="code-content__reference-block">
          {
            Object.keys(exampleObject).map((item) => {
              return (
                <div className="code-content__reference-item" key={exampleObject[item].id}>
                  {exampleObject[item].content && 
                    <div className="grid grid-nogutter">
                      <div className={classnames({"col col-4": !fullWidth, 'col col-10': fullWidth })}>
                        {exampleObject[item].title && <Text size="h2" weight="normal" color="black" extraClass="mb--lg">{exampleObject[item].title}</Text>}
                        {exampleObject[item].content }
                      </div>
                    </div>
                  }
                  <div className="code-content__code-block">
                    <pre>
                      <Button
                        type="text"
                        text={copiedID === item ? 'Copied!' : ''}
                        svgName={copiedID !== item ? 'copy' : ''}
                        svgPos="right"
                        onClick={(e) => handleCopy(item, exampleObject[item].code)}/>
                        <code className={language}>
                          {exampleObject[item].code}
                        </code>
                    </pre>
                  </div>
                </div>
              );
            })
          }
        </div>
      </div>
    );
  }
}

export default ExampleBlock;