import React from 'react';
import classnames from 'classnames';

class FlexboxPreview extends React.Component{
    render(){
        const { type, extraClass, blocks, childClass } = this.props;
        const isLarge = type === "large";


        // Render Child
        
        let content = [];
        let children = (i) => (
                <div key={i} className="flex-preview--item"></div>
        );
        let customChild = (i) => (
            <div key={i} className={classnames({"flex-preview--item": true, [childClass]: childClass})}></div>
        );
        let childShrink = (i) => (
            <div key={i} className="flex-preview--item flex-preview--item__shrink"></div>
        );
        let childColor = (i) => (
            <div key={i} className="flex-preview--item flex-preview--item__color"></div>
        );
        for(let i = 0 ; i < ( blocks || 3 ) ; i++){
            if(childClass && childClass.match('color')){
                content.push(childColor(i));
            }
            else if(childClass) {
                if(i % 2 !== 0) {
                    if(childClass.match('shrink')){
                        content.push(childShrink(i));
                    }
                    else {
                        content.push(customChild(i));   
                    }
                }
                else {
                    content.push(children(i));
                }
            }
            else {
                content.push(children(i));
            }
        };


        return (
            <div className={classnames({ 
                "flex-preview--container": true, 
                "flex-preview--container_lg": isLarge, 
                [extraClass]: extraClass
            })}>
                {content}
            </div>
        );
    }
}

export default FlexboxPreview;