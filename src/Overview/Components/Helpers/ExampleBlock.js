import React from 'react';
import SectionTitle from './SectionTitle';
import classnames from 'classnames';
import { Text } from '../../../Components/SSComponents';

class ExampleBlock extends React.Component {
  render() {
    const { exampleObject, extraSpacing, fullWidth, title, isLoaderExample, isTableExample } = this.props;
    return (
      <div className="code-content__section">
        <SectionTitle
          title={title ? title : "Examples"}
          info="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eleifend porta nisl ut egestas. Nunc massa tellus, aliquet sit amet nulla in, feugiat malesuada nulla. Ut viverra quam risus, in dapibus nisl finibus eu. Suspendisse nec pharetra enim, id finibus erat. Donec facilisis lorem augue, id blandit velit volutpat vel."
        />
        
        
        {isLoaderExample ? 
          <>
            <div className="grid grid-nogutter">
            {
            [{ s: 0, e: 3}, { s: 3, e: 6 }, { s: 6, e: 8}, { s: 8, }].map(({ s: start, e: end }) => {
              return (
                <div className="col col-3" key={start}>
                <div className="flex_column code-content__example">
                  {
                    Object.keys(exampleObject).slice(start, end).map((item) => {
                      return (
                        <div className={classnames({
                          "grid code-content__example-item": true,
                          'mb--xl': extraSpacing
                          })} key={exampleObject[item].id}>
                            {exampleObject[item].content}
                        </div>
                      );
                    })
                  }
                </div>
                </div>
              );
            })
          }
          </div>
        </>
        :
          <div className="flex_column code-content__example">
            {
              Object.keys(exampleObject).map((item) => {
                return (
                  <div className={classnames({
                    "grid code-content__example-item": true,
                    'mb--xl': extraSpacing && !isTableExample,
                    'wpb--xl': isTableExample
                    })} key={exampleObject[item].id}>
                    <div className={classnames({"col col-4": !fullWidth, 'col col-10': fullWidth})}>
                      {exampleObject[item].title && <Text size="h2" weight="normal" color="black" extraClass="mb--lg">{exampleObject[item].title}</Text>}
                      {exampleObject[item].content}
                    </div>
                  </div>
                );
              })
            }
          </div>
        }
      </div>
    );
  }
}

export default ExampleBlock;