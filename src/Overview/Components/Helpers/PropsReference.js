import React from 'react';
import SectionTitle from './SectionTitle';

class PropsReference extends React.Component {
  render() {
    const { propObject } = this.props;
    return (
      <div className="code-content__section">
        <SectionTitle
          title="Props Reference"
        />
        <div className="code-content__reference-block">
          {
            Object.keys(propObject).map((item) => {
              return (
                <div className="code-content__reference-item" id={propObject[item].id} key={propObject[item].id}>
                  <h2 className="ss-text ss-prop-item ss-text__size--h2 ss-text__weight--semibold  mb--xl">{propObject[item].name}</h2>
                  <p className="ss-content-text ss-text__color--chrome mb--xl">{propObject[item].info}</p>
                  <div className="grid grid-nogutter">
                    <div className="col col-6">
                      <table className="ss-table ss-table--fixed-layout ss-table--centered">
                        <thead>
                          <tr>
                            <th>Type</th>
                            <th>Required</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{propObject[item].type}</td>
                            <td>{propObject[item].required}</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              );
            })
          }
        </div>
      </div>
    );
  }
}

export default PropsReference;