import React from 'react';
import { Button } from '../../../Components/SSComponents';
import SectionTitle from './SectionTitle';

class CodeBlock extends React.Component {
  render() {
    const { copiedID, importCode, title, item } = this.props;
    return (
      <div className="code-content__section">
        <SectionTitle
          title={title}
        />
        <div className="code-content__reference-block">
          <div className="code-content__reference-item">
            <div className="code-content__code-block">
              <pre>
                <Button
                  type="text"
                  text={copiedID === item ? 'Copied!' : ''}
                  svgName={copiedID !== item ? 'copy' : ''}
                  svgPos="right"
                  onClick={(e) => this.props.handleCopy(item, importCode)}/>
                  <code className="jsx">
                    {importCode}
                  </code>
              </pre>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CodeBlock;