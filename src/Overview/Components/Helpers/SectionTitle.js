import React from 'react';

class SectionTitle extends React.Component {
  render() {
    const { title, info } = this.props;
    return (
    <div className="code-content__section-title">
      <h2 className="ss-text ss-text__size--jumbo mb--sm">{title}</h2>
      {info && <p className="ss-content-text ss-text__color--chrome">{info}</p>}
    </div>
    );
  }
}

export default SectionTitle;