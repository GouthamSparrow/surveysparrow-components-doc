import React from 'react';
import copy from 'copy-to-clipboard';
import hljs from 'highlight.js';
import CodeReferenceBlock from './Helpers/CodeReferenceBlock';
import FlexboxPreview from './Helpers/FlexPreview';

const exampleObjectRow = {
    1: {
      id: 1,
      title:'Flex Grow',
      content: <><FlexboxPreview type="large" extraClass="fx-row" childClass="fx-grow-1" ></FlexboxPreview></>,
      code:  
      `
      <div class="fx-row">
        <div></div>
        <div class="fx-grow-1"></div>
        <div></div>
      </div>`
    },
    2: {
      id: 2,
      title:'Flex Shrink -1',
      content: <><FlexboxPreview type="large" extraClass="fx-column" childClass="fx-grow-1"></FlexboxPreview></>,
      code:   
      `
      <div class="fx-row">
        <div></div>
        <div class="fx-grow-1"></div>
        <div></div>
      </div>`
    },
}
const exampleObjectColumn = {
    1: {
      id: 1,
      title:'Flex Grow',
      content: <><FlexboxPreview type="large" extraClass="fx-row" childClass="fx-shrink-1"></FlexboxPreview></>,
      code: 
       `
    <div class="fx-row">
      <div></div>
      <div class="fx-shrink"></div>
      <div></div>
    </div>`
    },
    2: {
      id: 2,
      title:'Flex Shrink -1',
      content: <><FlexboxPreview type="large" extraClass="fx-column" childClass="fx-shrink-1"></FlexboxPreview></>,
      code: 
      `
    <div class="fx-column">
      <div></div>
      <div class="fx-shrink" ></div>
      <div></div>
    </div>`
    },
}
class Grow extends React.Component {
    constructor(){
      super();
      this.state = {
        copiedID: 0,
      }
    }
  
    componentDidMount() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
    componentDidUpdate() {
      hljs.initHighlighting.called = false;
      hljs.initHighlighting();
    }
  
    handleCopy = (id, value) => {
      copy(value);
      this.setState({
        copiedID: id
      })
      setTimeout(() => {
        this.setState({
          copiedID: 0
        })
      }, 2000);
    }
    render() {
      const { copiedID } = this.state;
      return (
        <div className="ss-clib-main-content-container">
          <div className="code-content__header">
            <h1 className="ss-heading-text ss-text__family--serif">Flex Grow/Shrink</h1>
          </div>
          <div className="code-content__wrapper">
            <CodeReferenceBlock
              title={"Flex Grow"}
              info={"Use .fx-grow-* utilities to toggle a flex item’s ability to grow to fill available space. In the example below, the .fx-grow-1 elements uses all available space it can, while allowing the remaining two flex items their necessary space."}
              exampleObject={exampleObjectRow}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          <CodeReferenceBlock
              title={"Flex Shrink"}
              info={"Use .fx-shrink-* utilities to toggle a flex item’s ability to shrink if necessary. In the example below, the second flex item with .fx-shrink-1 is forced to wrap it’s contents to a new line"}
              exampleObject={exampleObjectColumn}
              language="html"
              copiedID={copiedID}
              handleCopy={this.handleCopy}
              fullWidth
          />
          </div>
        </div>
      
      );
    }
  }
  
  export default Grow;