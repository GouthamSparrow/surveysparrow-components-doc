import Text from './Components/Typography';
import Tables from './Components/Tables';
import Buttons from './Components/Buttons';
import ActionButtons from './Components/ActionButtons';
import Input from './Components/FormInput';
import Select from './Components/FormSelect';
import Textarea from './Components/FormTextarea';
import Checkbox from './Components/FormCheckbox';
import FormRadio from './Components/FormRadio';
import Tab from './Components/Tab';
import Pane from './Components/Pane';
import Modal from './Components/Modal';
import Loader from './Components/Loader';
import Icon from './Components/Icon';
import ToggleSwitches from './Components/ToggleSwitches';
import Chips from './Components/Chips';
import SpinnerLoaders from './Components/SpinnerLoaders';
import PageLoaders from './Components/PageLoaders';
import Direction from './Components/FlexDirection';
import JustifyContent from './Components/FlexJustifyContent';
import AlignItems from './Components/FlexAlignItems';
import AlignContent from './Components/FlexAlignContent';
import AlignSelf from './Components/FlexAlignSelf';
import Wrap from './Components/FlexWrap';
import Grow from './Components/FlexGrow';
import Border from './Components/UtilitesBorder';
import Background from './Components/UtilitiesBackground';
import Overflow from './Components/UtilitiesOverflow';
import Float from './Components/UtilitiesFloat';
import HeightWidth from './Components/UtilitiesFixedHeight';
import Grid from './Components/UtilitiesGrid';

const componentList = [
{
    label: "Components",
    active: 'true',
    options: [
    { label: "Text", value: Text, link: '/content/text', active: 'true' },
    { label: "Buttons", value: Buttons, link: '/components/buttons', active: 'true' },
    { label: "Action Button", value: ActionButtons, link: '/components/actionbutton', active: 'true' },
    { label: "Chip", value: Chips, link: '/components/chip', active: 'true' },
    { label: "Input", value: Input, link: '/components/input', active: 'true' },
    { label: "Textarea", value: Textarea, link: '/components/textarea', active: 'true' },
    { label: "Select", value: Select, link: '/components/select', active: 'true' },
    { label: "Tables", value: Tables, link: '/content/tables', active: 'true' },
    { label: "Modal", value: Modal, link: '/components/modal', active: 'true' },
    { label: "Checkbox", value: Checkbox, link: '/components/checkbox', active: 'true' },
    { label: "Radio", value: FormRadio, link: '/components/radio', active: 'true' },
    { label: "Tab", value: Tab, link: '/components/tab', active: 'false' },
    { label: "Pane", value: Pane, link: '/components/pane', active: 'false' },
    { label: "Loader", value: Loader, link: '/components/loader', active: 'false' },
    { label: "Icon", value: Icon, link: '/components/icon', active: 'true' },
    { label: "Toggle Switch", value: ToggleSwitches, link: '/components/toggle', active: 'true' },
    { label: "Grid", value: Grid, link: '/components/grid', active: 'true' },
    ]
},
{
    label: "Flex",
    active: 'true',
    options: [
        { label: "Direction", value: Direction, link: '/flex/direction', active: 'true' },
        { label: "Justify Content", value: JustifyContent, link: '/flex/justifycontent', active: 'true' },
        { label: "Align Items", value: AlignItems, link: '/flex/alignitems', active: 'true' },
        { label: "Wrap", value: Wrap, link: '/flex/wrap', active: 'true' },
        { label: "Align Self", value: AlignSelf, link: '/flex/alignself', active: 'true' },
        { label: "Grow & Shrink", value: Grow, link: '/flex/grow', active: 'true' },
        { label: "Align Content", value: AlignContent, link: '/flex/aligncontent', active: 'true' },
    ]
},
{
    label: "Utilities",
    active: 'true',
    options: [
    { label: "Border", value: Border, link: '/utilities/border', active: 'true' },
    { label: "Background", value: Background, link: '/utilities/background', active: 'true' },
    { label: "Overflow", value: Overflow, link: '/utilities/overflow', active: 'true' },
    { label: "Float", value: Float, link: '/utilities/float', active: 'true' },
    { label: "Height & Width", value: HeightWidth, link: '/utilities/height-width', active: 'true' },
    ]
},
{
    label: "Loaders",
    active: 'true',
    options: [
    { label: "Spinner", value: SpinnerLoaders, link: '/loaders/spinner', active: 'true' },
    { label: "Page Loader", value: PageLoaders, link: '/loaders/page-loader', active: 'true' },
    ]
},
];

export default componentList;