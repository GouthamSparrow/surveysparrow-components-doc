import React from 'react';
import Select from '../Components/SSComponents/Select';
import { NavLink } from 'react-router-dom';
import history from './history';
import componentList from './componentList';

class SideBar extends React.Component {

  handleSearch = (value) => {
    let url = value.value.link;
    history.push(url);
  }

  render() {
    return (
      <div className="ss-clib-left-sidebar">
        <div className="ss-clib-sidebar-header">
            <NavLink to="/">
                <svg width="26" height="32" viewBox="0 0 26 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" clipRule="evenodd" d="M12.8786 0C11.643 0.14667 9.71836 0.685173 8.06652 2.51908L5.67679 5.20517L11.7125 7.16327L4.54984 22.4812L6.356 22.3045C6.71244 22.2703 15.1194 21.3624 17.3396 14.0032L15.1379 14.0011C14.2957 16.2183 12.6265 17.9323 10.1465 19.0917C9.40103 19.4396 8.67835 19.6891 8.06 19.8657L14.5739 5.93531L9.30757 4.2288L9.62381 3.87337C11.4984 1.79216 13.8088 1.99235 13.899 1.99985C19.8685 2.36813 22.4549 6.81747 22.393 11.0666C22.2984 17.5875 16.4735 24.7936 3.90867 25.2079L3.26967 25.2294L0.310486 31.5693H2.5948L4.62265 27.2271C18.1014 26.5365 24.3632 18.4397 24.4697 11.0955C24.5588 5.04459 20.451 0.536362 14.4424 0H12.8786Z" fill="#fff"/>
                </svg>
            </NavLink>
        </div>
        <div className="ss-clib-sidebar-search-wrapper">
            <Select
              options={componentList}
              placeholder="Search"
              onChange={(value) =>
                this.handleSearch({ value })
              }
            />
        </div>
        <div className="ss-clib-sidebar-contents">
            {
              componentList.map((item, index) => {
                if(item.active === 'true') {
                  return (
                    <div className="ss-clib-sidebar-contents-block" key={index}>
                      <h4>{item.label}</h4>
                      <div className="ss-clib-sidebar-contents-links">
                        {
                          item.options.map((component, id)=> {
                            if(component.active === 'true') {
                              return <NavLink key={id} activeClassName={'active'} to={component.link}>{component.label}</NavLink>;
                            }
                            return null;
                          })
                        }
                      </div>
                    </div>
                  )
                }
                else return null;
              })
            }
        </div>
      </div>
    );
  }
}

export default SideBar;