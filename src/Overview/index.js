import React from 'react';
import SideBar from './SideBar';
import Intro from './Contents/Intro';
import { Router, Switch, Route } from 'react-router-dom';
import history from './history';
import componentList from './componentList';

class Overview extends React.Component {
  render() {
    return (
    <Router history={history}>
      <div className="ss-clib-main-wrapper">
        <SideBar/>
        <div className="ss-clib-main-content-wrapper">
            <Switch>
                {
                  componentList.map((item, index) => {
                    return item.options.map((route, id) => {
                      if(route.active === 'true') {
                        return (
                          <Route key={id} path={route.link} component={route.value}/>
                        )
                      }
                      else return null;
                    }); 
                  })
                }
                <Route path="/" component={Intro}/>
            </Switch>
        </div>
      </div>
    </Router>
    );
  }
}

export default Overview;