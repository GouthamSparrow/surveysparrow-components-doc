/* eslint-disable */
import React from 'react';

const BrandSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 15 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path fillRule='evenodd' clipRule='evenodd' d='M7.51133 0C6.77286 0.0929192 5.62261 0.434075 4.63539 1.5959L3.20717 3.29761L6.81443 4.53812L2.53365 14.2424L3.6131 14.1305C3.82613 14.1088 8.85057 13.5336 10.1775 8.87141L8.86161 8.87005C8.35826 10.2747 7.36065 11.3606 5.87851 12.0951C5.43297 12.3155 5.00106 12.4735 4.6315 12.5855L8.52453 3.76017L5.37711 2.67906L5.56611 2.45388C6.68648 1.13538 8.06729 1.26221 8.1212 1.26696C11.6888 1.50027 13.2346 4.31905 13.1976 7.01099C13.1411 11.1422 9.65983 15.7074 2.15045 15.9699L1.76855 15.9835L0 20H1.36522L2.57717 17.2491C10.6328 16.8116 14.3751 11.682 14.4388 7.0293C14.492 3.19588 12.037 0.339799 8.44594 0H7.51133Z' fill={color}/>
    </svg>
  );
};

const DeleteSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M1.90912 4H3.18185H13.3637' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M5.09093 4.00004V2.66671C5.09093 2.31309 5.22502 1.97395 5.4637 1.7239C5.70239 1.47385 6.02611 1.33337 6.36366 1.33337H8.90913C9.24669 1.33337 9.57041 1.47385 9.80909 1.7239C10.0478 1.97395 10.1819 2.31309 10.1819 2.66671V4.00004M12.091 4.00004V13.3334C12.091 13.687 11.9569 14.0261 11.7182 14.2762C11.4795 14.5262 11.1558 14.6667 10.8182 14.6667H4.45456C4.11701 14.6667 3.79328 14.5262 3.5546 14.2762C3.31592 14.0261 3.18182 13.687 3.18182 13.3334V4.00004H12.091Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M6.36368 7.33337V11.3334' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M8.90915 7.33337V11.3334' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const EditSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M11 4H4C3.46957 4 2.96086 4.21071 2.58579 4.58579C2.21071 4.96086 2 5.46957 2 6V20C2 20.5304 2.21071 21.0391 2.58579 21.4142C2.96086 21.7893 3.46957 22 4 22H18C18.5304 22 19.0391 21.7893 19.4142 21.4142C19.7893 21.0391 20 20.5304 20 20V13' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M18.5 2.49998C18.8978 2.10216 19.4374 1.87866 20 1.87866C20.5626 1.87866 21.1022 2.10216 21.5 2.49998C21.8978 2.89781 22.1213 3.43737 22.1213 3.99998C22.1213 4.56259 21.8978 5.10216 21.5 5.49998L12 15L8 16L9 12L18.5 2.49998Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>

  );
};

const SearchSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g>
        <path d='M11 19C15.4183 19 19 15.4183 19 11C19 6.58172 15.4183 3 11 3C6.58172 3 3 6.58172 3 11C3 15.4183 6.58172 19 11 19Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round' fill='transparent'/>
        <path d='M21 21L16.65 16.65' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      </g>
    </svg>
  );
};

const CloseSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g>
      <path d='M5 5L19 19' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M19 5L5 19' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      </g>
    </svg>
  );
};

const RightSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path fillRule='evenodd' clipRule='evenodd' d='M5.66552 13.3716C5.46027 13.1869 5.44363 12.8708 5.62836 12.6655L9.82732 8L5.62836 3.33448C5.44363 3.12922 5.46027 2.81308 5.66552 2.62835C5.87078 2.44362 6.18692 2.46026 6.37165 2.66551L10.8717 7.66551C11.0428 7.85567 11.0428 8.14433 10.8717 8.33448L6.37165 13.3345C6.18692 13.5397 5.87078 13.5564 5.66552 13.3716Z' stroke={color} strokeWidth='1'/>
    </svg>
  );
};

const LeftSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path fillRule='evenodd' clipRule='evenodd' d='M10.3345 13.3716C10.5397 13.1869 10.5564 12.8708 10.3716 12.6655L6.17268 8L10.3716 3.33448C10.5564 3.12922 10.5397 2.81308 10.3345 2.62835C10.1292 2.44362 9.81308 2.46026 9.62835 2.66551L5.12835 7.66551C4.95721 7.85567 4.95721 8.14433 5.12835 8.33448L9.62835 13.3345C9.81308 13.5397 10.1292 13.5564 10.3345 13.3716Z' fill={color}/>
    </svg>
  );
};

const NextSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 14 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M1 1L12 12L1 23' stroke={color} strokeWidth='2'/>
    </svg>
  );
};

const PrevSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 14 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M13 1L2 12L13 23' stroke={color} strokeWidth='2'/>
    </svg>
  );
};

const CheckSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path fillRule='evenodd' clipRule='evenodd' d='M4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12ZM12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2ZM16.7863 9.61782C17.1275 9.18355 17.0521 8.5549 16.6178 8.21368C16.1835 7.87247 15.5549 7.94791 15.2137 8.38218L10.3369 14.5889L8.62469 13.2191C8.19343 12.8741 7.56414 12.944 7.21913 13.3753C6.87412 13.8066 6.94404 14.4359 7.3753 14.7809L9.8753 16.7809C10.0836 16.9475 10.3498 17.024 10.6148 16.9934C10.8798 16.9628 11.1215 16.8276 11.2863 16.6178L16.7863 9.61782Z' fill='#7ABF5C'/>
    </svg>
  );
};

const ErrorSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path fillRule='evenodd' clipRule='evenodd' d='M10.2666 4.50622C11.0359 3.17013 12.9639 3.17012 13.7331 4.50622L21.5035 18.0021C22.2711 19.3354 21.3088 21 19.7702 21H4.22954C2.691 21 1.72862 19.3354 2.49629 18.0021L10.2666 4.50622ZM19.7702 19L11.9999 5.50415L4.22954 19L19.7702 19ZM11.9999 9C12.5522 9 12.9999 9.44772 12.9999 10V13C12.9999 13.5523 12.5522 14 11.9999 14C11.4476 14 10.9999 13.5523 10.9999 13V10C10.9999 9.44772 11.4476 9 11.9999 9ZM11.9999 17C12.5522 17 12.9999 16.5523 12.9999 16C12.9999 15.4477 12.5522 15 11.9999 15C11.4476 15 10.9999 15.4477 10.9999 16C10.9999 16.5523 11.4476 17 11.9999 17Z' fill='#FF3945'/>
    </svg>
  );
};

const InfoSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path fillRule='evenodd' clipRule='evenodd' d='M12 16C12.5523 16 13 15.5523 13 15V12C13 11.4477 12.5523 11 12 11C11.4477 11 11 11.4477 11 12V15C11 15.5523 11.4477 16 12 16Z' fill={color}/>
      <path d='M13 9C13 8.44772 12.5523 8 12 8C11.4477 8 11 8.44772 11 9C11 9.55228 11.4477 10 12 10C12.5523 10 13 9.55228 13 9Z' fill={color}/>
      <path fillRule='evenodd' clipRule='evenodd' d='M12 4C16.4183 4 20 7.58172 20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12C4 7.58172 7.58172 4 12 4ZM12 2C17.5228 2 22 6.47715 22 12C22 17.5228 17.5228 22 12 22C6.47715 22 2 17.5228 2 12C2 6.47715 6.47715 2 12 2Z' fill={color}/>
    </svg>
  );
};

const DownloadSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g>
        <path fillRule='evenodd' clipRule='evenodd' d='M12 2C12.5523 2 13 2.44772 13 3V12.5858L15.2929 10.2929C15.6834 9.90237 16.3166 9.90237 16.7071 10.2929C17.0976 10.6834 17.0976 11.3166 16.7071 11.7071L12.7071 15.7071C12.3166 16.0976 11.6834 16.0976 11.2929 15.7071L7.29289 11.7071C6.90237 11.3166 6.90237 10.6834 7.29289 10.2929C7.68342 9.90237 8.31658 9.90237 8.70711 10.2929L11 12.5858V3C11 2.44772 11.4477 2 12 2ZM3 13.0769C3.55228 13.0769 4 13.5246 4 14.0769V18C4 19.1046 4.89543 20 6 20H18C19.1046 20 20 19.1046 20 18V14.0769C20 13.5246 20.4477 13.0769 21 13.0769C21.5523 13.0769 22 13.5246 22 14.0769V18C22 20.2091 20.2092 22 18 22H6C3.79086 22 2 20.2091 2 18V14.0769C2 13.5246 2.44772 13.0769 3 13.0769Z' fill={color}/>
      </g>
    </svg>
  );
};

const MoreSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g opacity='0.5'>
      <path d='M8.00002 9.82218C9.0064 9.82218 9.82224 9.00634 9.82224 7.99996C9.82224 6.99357 9.0064 6.17773 8.00002 6.17773C6.99363 6.17773 6.1778 6.99357 6.1778 7.99996C6.1778 9.00634 6.99363 9.82218 8.00002 9.82218Z' fill={color}/>
      <path d='M8.00002 4.48893C9.0064 4.48893 9.82224 3.67309 9.82224 2.6667C9.82224 1.66032 9.0064 0.844482 8.00002 0.844482C6.99363 0.844482 6.1778 1.66032 6.1778 2.6667C6.1778 3.67309 6.99363 4.48893 8.00002 4.48893Z' fill={color}/>
      <path d='M8.00002 15.1557C9.0064 15.1557 9.82224 14.3398 9.82224 13.3335C9.82224 12.3271 9.0064 11.5112 8.00002 11.5112C6.99363 11.5112 6.1778 12.3271 6.1778 13.3335C6.1778 14.3398 6.99363 15.1557 8.00002 15.1557Z' fill={color}/>
      </g>
    </svg>
  );
};

const MoreoutlineSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill='none' viewBox='0 0 6 22' xmlns='http://www.w3.org/2000/svg'>
      <path d='M3 6.5C1.34315 6.5 0 5.15685 0 3.5C8.9407e-08 1.84315 1.34315 0.5 3 0.5C4.65685 0.5 6 1.84315 6 3.5C6 5.15685 4.65685 6.5 3 6.5ZM3 14C1.34315 14 0 12.6569 0 11C8.9407e-08 9.34315 1.34315 8 3 8C4.65685 8 6 9.34315 6 11C6 12.6569 4.65685 14 3 14ZM3 21.5C1.34315 21.5 0 20.1569 0 18.5C8.9407e-08 16.8431 1.34315 15.5 3 15.5C4.65685 15.5 6 16.8431 6 18.5C6 20.1569 4.65685 21.5 3 21.5ZM3 5C2.17157 5 1.5 4.32843 1.5 3.5C1.5 2.67157 2.17157 2 3 2C3.82843 2 4.5 2.67157 4.5 3.5C4.5 4.32843 3.82843 5 3 5ZM3 12.5C2.17157 12.5 1.5 11.8284 1.5 11C1.5 10.1716 2.17157 9.5 3 9.5C3.82843 9.5 4.5 10.1716 4.5 11C4.5 11.8284 3.82843 12.5 3 12.5ZM1.5 18.5C1.5 19.3284 2.17157 20 3 20C3.82843 20 4.5 19.3284 4.5 18.5C4.5 17.6716 3.82843 17 3 17C2.17157 17 1.5 17.6716 1.5 18.5Z' fill={color} fillRule='evenodd'/>
    </svg>
  );
};

const DoneSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path fillRule='evenodd' clipRule='evenodd' d='M4 12C4 7.58172 7.58172 4 12 4C16.4183 4 20 7.58172 20 12C20 16.4183 16.4183 20 12 20C7.58172 20 4 16.4183 4 12ZM12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2ZM16.7863 9.61782C17.1275 9.18355 17.0521 8.5549 16.6178 8.21368C16.1835 7.87247 15.5549 7.94791 15.2137 8.38218L10.3369 14.5889L8.62469 13.2191C8.19343 12.8741 7.56414 12.944 7.21913 13.3753C6.87412 13.8066 6.94404 14.4359 7.3753 14.7809L9.8753 16.7809C10.0836 16.9475 10.3498 17.024 10.6148 16.9934C10.8798 16.9628 11.1215 16.8276 11.2863 16.6178L16.7863 9.61782Z' fill='#8DCD71'/>
    </svg>
  );
};

const PartialdoneSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} version='1.1' viewBox='0 0 26 26' xmlns='http://www.w3.org/2000/svg'>
      <g fill='none' fillRule='evenodd' stroke='none' strokeLinecap='round' strokeWidth='1'>
        <g stroke='#E59356' strokeWidth='1.5' transform='translate(-970.000000, -145.000000)'>
          <g id='Group-3' transform='translate(971.000000, 146.000000)'>
            <path id='Path' d='M24,12 C24,5.372583 18.627417,0 12,0 C5.372583,0 0,5.372583 0,12 C0,18.627417 5.372583,24 12,24' opacity='0.702706473'/>
            <g transform='translate(8.000000, 8.000000)'>
              <polyline id='Path' points='8.72727273 0.726545455 3.63636364 7.63563636 0 4.72654545'/>
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

const PauseSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 12 12' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M4.5 1.25H2.5C2.22386 1.25 2 1.47386 2 1.75V10.25C2 10.5261 2.22386 10.75 2.5 10.75H4.5C4.77614 10.75 5 10.5261 5 10.25V1.75C5 1.47386 4.77614 1.25 4.5 1.25Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M9.5 1.25H7.5C7.22386 1.25 7 1.47386 7 1.75V10.25C7 10.5261 7.22386 10.75 7.5 10.75H9.5C9.77614 10.75 10 10.5261 10 10.25V1.75C10 1.47386 9.77614 1.25 9.5 1.25Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const LoadingSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g clipPath='url(#clip0)'>
      <path d='M15.3335 2.66666V6.66666H11.3335' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M0.666504 13.3333V9.33334H4.6665' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M2.33984 6.00001C2.67795 5.04453 3.25259 4.19028 4.01015 3.51696C4.7677 2.84363 5.68348 2.37319 6.67203 2.14952C7.66058 1.92584 8.68967 1.95624 9.6633 2.23786C10.6369 2.51948 11.5233 3.04315 12.2398 3.76001L15.3332 6.66668M0.666504 9.33334L3.75984 12.24C4.47634 12.9569 5.36275 13.4805 6.33638 13.7622C7.31 14.0438 8.3391 14.0742 9.32765 13.8505C10.3162 13.6268 11.232 13.1564 11.9895 12.4831C12.7471 11.8097 13.3217 10.9555 13.6598 10' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      </g>
      <defs>
      <clipPath id='clip0'>
      <rect width={size} height={size} fill='white'/>
      </clipPath>
      </defs>
    </svg>
  );
};

const CheckboxSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 17 17' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <rect x='1.13635' y='1.41379' width='15' height='15' rx='2.5' fill='#F2F2F2' stroke={color}/>
    </svg>
  );
};

const BackSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 15 15' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M1.1112 7.00004C1.1112 6.63185 1.40973 6.33337 1.77786 6.33337H14.2224C14.5906 6.33337 14.889 6.63185 14.889 7.00004C14.889 7.36823 14.5906 7.66671 14.2224 7.66671H1.77786C1.40973 7.66671 1.1112 7.36823 1.1112 7.00004Z' fill={color}/>
      <path d='M8.49554 0.554071C8.74185 0.827746 8.71966 1.24927 8.44598 1.49557L2.31052 7.01745L8.46336 12.9652C8.72809 13.2211 8.73524 13.6431 8.47934 13.9078C8.22344 14.1726 7.8014 14.1798 7.53666 13.9238L0.869891 7.47937C0.737091 7.35089 0.663491 7.173 0.666691 6.98817C0.670024 6.80333 0.749891 6.62817 0.887358 6.50452L7.55404 0.504515C7.82772 0.25821 8.24924 0.280398 8.49554 0.554071Z' fill={color}/>
    </svg>
  );
};

const ListSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g>
        <path fillRule='evenodd' clipRule='evenodd' d='M3 3H2V4H3V3ZM5.5 3C5.22386 3 5 3.22386 5 3.5C5 3.77614 5.22386 4 5.5 4H13.5C13.7761 4 14 3.77614 14 3.5C14 3.22386 13.7761 3 13.5 3H5.5ZM2 8H3V9H2V8ZM5.5 8C5.22386 8 5 8.22386 5 8.5C5 8.77614 5.22386 9 5.5 9H13.5C13.7761 9 14 8.77614 14 8.5C14 8.22386 13.7761 8 13.5 8H5.5ZM2 13H3V14H2V13ZM5.5 13C5.22386 13 5 13.2239 5 13.5C5 13.7761 5.22386 14 5.5 14H13.5C13.7761 14 14 13.7761 14 13.5C14 13.2239 13.7761 13 13.5 13H5.5Z' fill={color}/>
      </g>
    </svg>
  );
};
const MenuSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 10 10' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <line y1='1' x2='10' y2='1' stroke={color} strokeWidth='2'/>
      <line y1='5' x2='10' y2='5' stroke={color} strokeWidth='2'/>
      <line y1='9' x2='10' y2='9' stroke={color} strokeWidth='2'/>
    </svg>

  );
};

const CopySVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill='none' viewBox='0 0 18 18' xmlns='http://www.w3.org/2000/svg'>
      <path d='M12 0.75H3C2.175 0.75 1.5 1.425 1.5 2.25V12.75H3V2.25H12V0.75ZM11.25 3.75H6C5.175 3.75 4.5075 4.425 4.5075 5.25L4.5 15.75C4.5 16.575 5.1675 17.25 5.9925 17.25H14.25C15.075 17.25 15.75 16.575 15.75 15.75V8.25L11.25 3.75ZM6 15.75V5.25H10.5V9H14.25V15.75H6Z' fill={color}/>
    </svg>
  );
};

const FilterSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M22 3H2L10 12.46V19L14 21V12.46L22 3Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const ArrowrightSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 12 11' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path fillRule='evenodd' clipRule='evenodd' d='M10.8333 5.5C10.8333 5.22386 10.6094 5 10.3333 5H0.999918C0.723776 5 0.499918 5.22386 0.499918 5.5C0.499918 5.77614 0.723776 6 0.999918 6H10.3333C10.6094 6 10.8333 5.77614 10.8333 5.5Z' fill={color}/>
      <path fillRule='evenodd' clipRule='evenodd' d='M5.29506 0.665523C5.11033 0.870779 5.12697 1.18692 5.33223 1.37165L9.93383 5.51306L5.3192 9.97384C5.12065 10.1658 5.11529 10.4823 5.30721 10.6808C5.49914 10.8794 5.81567 10.8848 6.01422 10.6928L11.0143 5.8595C11.1139 5.76314 11.1691 5.62972 11.1667 5.4911C11.1642 5.35247 11.1043 5.2211 11.0012 5.12836L6.00119 0.628356C5.79593 0.443627 5.47979 0.460268 5.29506 0.665523Z' fill={color}/>
    </svg>
  );
};

const DownSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path fillRule='evenodd' clipRule='evenodd' d='M2.44254 5.49828C2.71963 5.1904 3.19385 5.16544 3.50173 5.44254L8.00001 9.49098L12.4983 5.44254C12.8062 5.16544 13.2804 5.1904 13.5575 5.49828C13.8346 5.80617 13.8096 6.28038 13.5017 6.55748L8.50173 11.0575C8.2165 11.3142 7.78351 11.3142 7.49828 11.0575L2.49828 6.55748C2.1904 6.28038 2.16544 5.80617 2.44254 5.49828Z' fill={color}/>
    </svg>
  );
};

const PlusSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g opacity='1'>
        <line x1='10' y1='1' x2='10' y2='19' stroke={color} strokeWidth='2' strokeLinecap='round'/>
        <line x1='19' y1='10.5' x2='1' y2='10.5' stroke={color} strokeWidth='2' strokeLinecap='round'/>
      </g>
    </svg>
  );
};

const MinusSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 16 2' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <line x1='16' y1='1' x2='-8.74226e-08' y2='0.999999' stroke={color} strokeWidth='2'/>
    </svg>
  );
};

const PluscircleSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M7.99999 15.3334C12.0501 15.3334 15.3333 12.0502 15.3333 8.00008C15.3333 3.94999 12.0501 0.666748 7.99999 0.666748C3.9499 0.666748 0.666656 3.94999 0.666656 8.00008C0.666656 12.0502 3.9499 15.3334 7.99999 15.3334Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M8 3.66675V12.3334' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M12.6667 8L3.99999 8' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
    </svg>

  );
};

const WindowsettingsSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 32 34' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M23.0415 25.6307C24.0997 25.6307 24.9575 24.7729 24.9575 23.7147C24.9575 22.6565 24.0997 21.7987 23.0415 21.7987C21.9833 21.7987 21.1255 22.6565 21.1255 23.7147C21.1255 24.7729 21.9833 25.6307 23.0415 25.6307Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M24.6666 16.3L25.232 18.156C25.3246 18.4642 25.531 18.7257 25.8093 18.8874C26.0876 19.0491 26.417 19.0989 26.7306 19.0267L28.6133 18.5907C28.9698 18.5097 29.3429 18.5452 29.6778 18.692C30.0127 18.8387 30.2917 19.089 30.4739 19.406C30.656 19.723 30.7317 20.0901 30.6899 20.4533C30.648 20.8165 30.4908 21.1567 30.2413 21.424L28.9333 22.844C28.7142 23.081 28.5925 23.3919 28.5925 23.7147C28.5925 24.0374 28.7142 24.3483 28.9333 24.5853L30.2506 26.004C30.4997 26.2715 30.6566 26.6117 30.6983 26.9748C30.7399 27.338 30.6642 27.7049 30.4822 28.0218C30.3002 28.3388 30.0214 28.5891 29.6867 28.7361C29.3521 28.8831 28.9792 28.919 28.6226 28.8387L26.74 28.4027C26.4263 28.3305 26.0969 28.3803 25.8186 28.542C25.5403 28.7037 25.3339 28.9651 25.2413 29.2733L24.6666 31.1333C24.5614 31.4833 24.3462 31.79 24.053 32.0081C23.7597 32.2261 23.404 32.3438 23.0386 32.3438C22.6732 32.3438 22.3175 32.2261 22.0243 32.0081C21.731 31.79 21.5158 31.4833 21.4106 31.1333L20.8453 29.2773C20.7526 28.9688 20.546 28.7072 20.2674 28.5455C19.9888 28.3837 19.6591 28.3341 19.3453 28.4067L17.464 28.8427C17.1074 28.923 16.7345 28.8871 16.3998 28.7401C16.0652 28.5931 15.7864 28.3428 15.6044 28.0258C15.4223 27.7089 15.3466 27.342 15.3883 26.9788C15.43 26.6157 15.5868 26.2755 15.836 26.008L17.1533 24.5893C17.3718 24.352 17.4931 24.0413 17.4931 23.7187C17.4931 23.3961 17.3718 23.0853 17.1533 22.848L15.836 21.428C15.5842 21.1609 15.4252 20.8199 15.3823 20.4553C15.3394 20.0908 15.415 19.7222 15.5979 19.404C15.7808 19.0857 16.0613 18.8348 16.3978 18.6884C16.7344 18.5419 17.1091 18.5077 17.4666 18.5907L19.348 19.0267C19.6618 19.0992 19.9915 19.0496 20.2701 18.8879C20.5487 18.7262 20.7553 18.4645 20.848 18.156L21.4133 16.3C21.5192 15.9511 21.7345 15.6455 22.0274 15.4283C22.3203 15.2112 22.6753 15.0939 23.04 15.0939C23.4046 15.0939 23.7596 15.2112 24.0525 15.4283C24.3454 15.6455 24.5607 15.9511 24.6666 16.3V16.3Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M1.37207 7.00531H28.0387' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M12.0387 23H4.03874C3.33149 23 2.65322 22.7191 2.15312 22.219C1.65302 21.7189 1.37207 21.0406 1.37207 20.3334V4.33335C1.37207 3.62611 1.65302 2.94783 2.15312 2.44774C2.65322 1.94764 3.33149 1.66669 4.03874 1.66669H25.3721C26.0793 1.66669 26.7576 1.94764 27.2577 2.44774C27.7578 2.94783 28.0387 3.62611 28.0387 4.33335V15' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>

  );
};
const AlarmbellcheckSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 34 34' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M13.7998 11.2986C12.6124 10.9659 11.364 10.9128 10.1526 11.1437C8.94116 11.3746 7.79975 11.8831 6.81792 12.6293C5.83609 13.3754 5.04056 14.339 4.49377 15.4444C3.94699 16.5497 3.66382 17.7668 3.6665 19V26.3333C3.6665 26.8638 3.45579 27.3725 3.08072 27.7475C2.70564 28.1226 2.19694 28.3333 1.6665 28.3333H21.6665C21.1361 28.3333 20.6274 28.1226 20.2523 27.7475C19.8772 27.3725 19.6665 26.8638 19.6665 26.3333V19.248' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M13.5532 31C13.4155 31.3902 13.1602 31.7281 12.8224 31.9671C12.4846 32.2061 12.081 32.3344 11.6672 32.3344C11.2535 32.3344 10.8499 32.2061 10.5121 31.9671C10.1743 31.7281 9.91895 31.3902 9.78125 31' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M11.6665 8.33466V11.0013' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M13.7998 11.2986C12.6124 10.9659 11.364 10.9128 10.1526 11.1437C8.94116 11.3746 7.79975 11.8831 6.81792 12.6293C5.83609 13.3754 5.04056 14.339 4.49377 15.4444C3.94699 16.5497 3.66382 17.7668 3.6665 19V26.3333C3.6665 26.8638 3.45579 27.3725 3.08072 27.7475C2.70564 28.1226 2.19694 28.3333 1.6665 28.3333H21.6665C21.1361 28.3333 20.6274 28.1226 20.2523 27.7475C19.8772 27.3725 19.6665 26.8638 19.6665 26.3333V19.248' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M13.5532 31C13.4155 31.3902 13.1602 31.7281 12.8224 31.9671C12.4846 32.2061 12.081 32.3344 11.6672 32.3344C11.2535 32.3344 10.8499 32.2061 10.5121 31.9671C10.1743 31.7281 9.91895 31.3902 9.78125 31' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M11.6665 8.33466V11.0013' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M24.3335 17.6693C28.7518 17.6693 32.3335 14.0876 32.3335 9.66931C32.3335 5.25103 28.7518 1.66931 24.3335 1.66931C19.9152 1.66931 16.3335 5.25103 16.3335 9.66931C16.3335 14.0876 19.9152 17.6693 24.3335 17.6693Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M27.8989 7.34399L24.0256 12.508C23.9394 12.6224 23.8297 12.7171 23.7039 12.7856C23.5781 12.8541 23.439 12.8948 23.2962 12.9051C23.1533 12.9153 23.0098 12.8948 22.8755 12.8449C22.7412 12.7951 22.6192 12.717 22.5176 12.616L20.5176 10.616' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};
const EmailsyncSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 34 34' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M20.9999 25.6667H16.3333V30.3333' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M31.556 27.3107C31.0089 28.9148 29.9299 30.2836 28.4978 31.1901C27.0657 32.0966 25.3668 32.4862 23.6829 32.2942C21.9989 32.1023 20.4313 31.3403 19.2399 30.1348C18.0486 28.9293 17.3053 27.3527 17.1333 25.6667' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M27.6667 23H32.3334V18.3333' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M17.1106 21.356C17.6576 19.7519 18.7367 18.383 20.1688 17.4766C21.6008 16.5701 23.2998 16.1805 24.9837 16.3725C26.6676 16.5644 28.2353 17.3263 29.4266 18.5318C30.6179 19.7373 31.3613 21.3139 31.5333 23' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M13.6667 20.3333H3.66675C3.13632 20.3333 2.62761 20.1226 2.25253 19.7475C1.87746 19.3725 1.66675 18.8638 1.66675 18.3333V3.66666C1.66675 3.13622 1.87746 2.62752 2.25253 2.25244C2.62761 1.87737 3.13632 1.66666 3.66675 1.66666H27.6667C28.1972 1.66666 28.7059 1.87737 29.081 2.25244C29.456 2.62752 29.6667 3.13622 29.6667 3.66666V13.6667' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M29.0895 2.26132L15.6668 13L2.24414 2.26132' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const EmailcheckSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 34 34' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M24.2412 32.4027C28.6595 32.4027 32.2412 28.8209 32.2412 24.4027C32.2412 19.9844 28.6595 16.4027 24.2412 16.4027C19.8229 16.4027 16.2412 19.9844 16.2412 24.4027C16.2412 28.8209 19.8229 32.4027 24.2412 32.4027Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M27.8066 22.0773L23.9333 27.2413C23.8474 27.3562 23.7378 27.4512 23.612 27.5199C23.4861 27.5887 23.347 27.6296 23.204 27.6398C23.0609 27.65 22.9174 27.6294 22.783 27.5793C22.6487 27.5292 22.5267 27.4507 22.4253 27.3493L20.4253 25.3493' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M13.7012 20.3773H3.70117C3.17074 20.3773 2.66203 20.1666 2.28696 19.7915C1.91189 19.4165 1.70117 18.9078 1.70117 18.3773V3.71066C1.70117 3.18023 1.91189 2.67152 2.28696 2.29645C2.66203 1.92138 3.17074 1.71066 3.70117 1.71066H27.7012C28.2316 1.71066 28.7403 1.92138 29.1154 2.29645C29.4905 2.67152 29.7012 3.18023 29.7012 3.71066V13.7107' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M29.124 2.30533L15.7013 13.044L2.27734 2.30533' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>

  );
};

const EmailsettingsSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 19 19' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M13.9375 14.4761C14.5327 14.4761 15.0152 13.9935 15.0152 13.3983C15.0152 12.8031 14.5327 12.3206 13.9375 12.3206C13.3423 12.3206 12.8597 12.8031 12.8597 13.3983C12.8597 13.9935 13.3423 14.4761 13.9375 14.4761Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M14.8533 9.2275L15.1713 10.2715C15.2234 10.4449 15.3395 10.5919 15.496 10.6829C15.6526 10.7738 15.8378 10.8018 16.0143 10.7612L17.0733 10.516C17.2738 10.4708 17.4836 10.491 17.6718 10.5737C17.8601 10.6564 18.0169 10.7972 18.1193 10.9755C18.2217 11.1538 18.2643 11.3602 18.2408 11.5644C18.2174 11.7687 18.1291 11.96 17.989 12.1105L17.248 12.9092C17.1257 13.0421 17.0579 13.2162 17.0579 13.3967C17.0579 13.5773 17.1257 13.7513 17.248 13.8842L17.989 14.683C18.1291 14.8335 18.2174 15.0248 18.2408 15.2291C18.2643 15.4333 18.2217 15.6397 18.1193 15.818C18.0169 15.9963 17.8601 16.1371 17.6718 16.2198C17.4836 16.3025 17.2738 16.3227 17.0733 16.2775L16.0143 16.0315C15.8379 15.9925 15.6533 16.0217 15.4976 16.1133C15.3419 16.2048 15.2267 16.3519 15.175 16.525L14.857 17.569C14.7978 17.7658 14.6768 17.9384 14.5118 18.061C14.3469 18.1837 14.1468 18.2499 13.9413 18.2499C13.7357 18.2499 13.5356 18.1837 13.3707 18.061C13.2057 17.9384 13.0847 17.7658 13.0255 17.569L12.7038 16.525C12.6513 16.3518 12.5352 16.205 12.3787 16.1141C12.2223 16.0232 12.0372 15.995 11.8608 16.0352L10.8018 16.2812C10.6012 16.3265 10.3914 16.3062 10.2032 16.2235C10.015 16.1409 9.85815 16.0001 9.75575 15.8218C9.65336 15.6435 9.61076 15.4371 9.63421 15.2328C9.65765 15.0286 9.74589 14.8372 9.88601 14.6867L10.627 13.888C10.7493 13.7551 10.8172 13.5811 10.8172 13.4005C10.8172 13.2199 10.7493 13.0459 10.627 12.913L9.88601 12.1142C9.74589 11.9638 9.65765 11.7724 9.63421 11.5682C9.61076 11.3639 9.65336 11.1575 9.75575 10.9792C9.85815 10.8009 10.015 10.6601 10.2032 10.5774C10.3914 10.4948 10.6012 10.4745 10.8018 10.5197L11.8608 10.765C12.0372 10.8056 12.2225 10.7776 12.379 10.6866C12.5355 10.5957 12.6516 10.4486 12.7038 10.2752L13.0218 9.23125C13.0805 9.03428 13.2012 8.8615 13.3659 8.73851C13.5306 8.61553 13.7306 8.54888 13.9361 8.54846C14.1417 8.54804 14.3419 8.61387 14.5071 8.73618C14.6723 8.85849 14.7937 9.03077 14.8533 9.2275V9.2275Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M7.75 11.5H2.125C1.82663 11.5 1.54048 11.3815 1.3295 11.1705C1.11853 10.9595 1 10.6734 1 10.375V2.125C1 1.82663 1.11853 1.54048 1.3295 1.3295C1.54048 1.11853 1.82663 1 2.125 1H15.625C15.9234 1 16.2095 1.11853 16.4205 1.3295C16.6315 1.54048 16.75 1.82663 16.75 2.125V7.75' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M16.4253 1.33447L8.87502 7.37497L1.32477 1.33447' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const UpSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 12 12' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M6 0.583313V11.4166' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M10.5834 5.16665L6.00008 0.583313L1.41675 5.16665' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const DownarrowSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill="none" viewBox="0 0 12 12" xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0)">
        <path d="M6 11.4167L6 0.5834" stroke={color} strokeLinecap="round"/>
        <path d="M1.4166 6.83335L5.99992 11.4167L10.5833 6.83335" stroke={color} strokeLinecap="round"/>
      </g>
      <defs>
        <clipPath id="clip0">
          <rect height="12" width="12" fill="white" transform="translate(12 12) rotate(-180)"/>
        </clipPath>
      </defs>
    </svg>
  );
};

const ColumnSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} style={{ marginTop: 2 }} viewBox='0 0 16 15' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g>
        <path d='M0 7.11108H16' stroke={color} strokeWidth='1.5'/>
        <rect x='0.75' y='0.75' width='14.5' height='12.7222' rx='1.25' stroke={color} strokeWidth='1.5'/>
        <path d='M5.33331 0.888916L5.33331 13.3334' stroke={color} strokeWidth='1.5'/>
      </g>
    </svg>

  );
};

const TickSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M12.7698 4.20939C13.0684 4.49648 13.0777 4.97127 12.7906 5.26984L6.54055 11.7699C6.39617 11.92 6.19584 12.0034 5.98755 11.9999C5.77927 11.9965 5.5818 11.9066 5.44245 11.7517L3.19248 9.25171C2.91539 8.94383 2.94035 8.46961 3.24823 8.19252C3.55612 7.91543 4.03034 7.94039 4.30743 8.24828L6.01809 10.1491L11.7093 4.23018C11.9964 3.9316 12.4712 3.92229 12.7698 4.20939Z" fill={color}/>
    </svg>
  );
};
const DownfillSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path fillRule='evenodd' clipRule='evenodd' d='M3.50001 5C3.30255 5 3.12359 5.11621 3.04326 5.29658C2.96293 5.47696 2.99627 5.68771 3.12836 5.83448L7.62836 10.8345C7.72318 10.9398 7.85827 11 8.00001 11C8.14175 11 8.27683 10.9398 8.37166 10.8345L12.8717 5.83448C13.0037 5.68771 13.0371 5.47696 12.9568 5.29658C12.8764 5.11621 12.6975 5 12.5 5H3.50001Z' fill={color}/>
    </svg>
  );
};

const UpfillSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} version='1.1' viewBox='0 0 16 16' xmlns='http://www.w3.org/2000/svg'>
      <g fill='none' fillRule='evenodd' stroke='none' strokeWidth='1'>
        <g fill={color} fillRule='nonzero' transform='translate(5, 8) scale(1, -1) translate(-2, -3)'>
          <path d='M0.50000116,0 C0.30255,0 0.12359,0.11621 0.04326,0.29658 C-0.03707,0.47696 -0.00373,0.68771 0.12836,0.83448 L4.62836,5.8345 C4.72318,5.9398 4.85827,6 5.00000116,6 C5.14175,6 5.27683,5.9398 5.37166,5.8345 L9.8717,0.83448 C10.0037,0.68771 10.0371,0.47696 9.9568,0.29658 C9.8764,0.11621 9.6975,0 9.50000116,0 L0.50000116,0 Z'/>
        </g>
      </g>
    </svg>
  );
};

const UploadSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg
      width={size}
      height={size}
      className={extraClass}
      viewBox='0 0 48 48'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M42 30V38C42 39.0609 41.5786 40.0783 40.8284 40.8284C40.0783 41.5786 39.0609 42 38 42H10C8.93913 42 7.92172 41.5786 7.17157 40.8284C6.42143 40.0783 6 39.0609 6 38V30'
        stroke={color}
        strokeWidth='4'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M34 16L24 6L14 16'
        stroke={color}
        strokeWidth='4'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M24 6V30'
        stroke={color}
        strokeWidth='4'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
};

const HyperlinkSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg
      width={size} height={size} className={extraClass}
      viewBox='0 0 16 16'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M7.33325 3.33333H2.66659C2.31296 3.33333 1.97382 3.4738 1.72378 3.72385C1.47373 3.9739 1.33325 4.31304 1.33325 4.66666V13.3333C1.33325 13.687 1.47373 14.0261 1.72378 14.2761C1.97382 14.5262 2.31296 14.6667 2.66659 14.6667H11.9999C12.3535 14.6667 12.6927 14.5262 12.9427 14.2761C13.1928 14.0261 13.3333 13.687 13.3333 13.3333V9.33333'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        d='M13.3334 2.66667V5.66667M13.3334 2.66667L6.66675 9.33334M13.3334 2.66667H9.84135'
        stroke={color}
        strokeWidth='1.5'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
};

const ThrottlingSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill="none" viewBox="0 0 34 34" xmlns="http://www.w3.org/2000/svg">
      <g>
        <path d="M10.6252 33.2917L11.3335 24.7917H14.8752V19.8333C14.8752 17.9547 14.1289 16.153 12.8005 14.8247C11.4721 13.4963 9.67045 12.75 7.79183 12.75C5.91321 12.75 4.11154 13.4963 2.78316 14.8247C1.45477 16.153 0.708496 17.9547 0.708496 19.8333V24.7917H4.25016L4.9585 33.2917H10.6252Z" stroke={color} strokeLinecap="round" strokeWidth="1.5"/>
        <path d="M7.79183 10.625C10.5302 10.625 12.7502 8.40509 12.7502 5.66668C12.7502 2.92827 10.5302 0.708344 7.79183 0.708344C5.05342 0.708344 2.8335 2.92827 2.8335 5.66668C2.8335 8.40509 5.05342 10.625 7.79183 10.625Z" stroke={color} strokeLinecap="round" strokeWidth="1.5"/>
        <path d="M24.7052 14.6908C23.6717 13.7123 22.3676 13.0674 20.9626 12.84C19.5575 12.6126 18.1166 12.8133 16.8271 13.4158" stroke={color} strokeLinecap="round" strokeWidth="1.5"/>
        <path d="M19.8335 10.625C22.1807 10.625 24.0835 8.72221 24.0835 6.375C24.0835 4.02779 22.1807 2.125 19.8335 2.125C17.4863 2.125 15.5835 4.02779 15.5835 6.375C15.5835 8.72221 17.4863 10.625 19.8335 10.625Z" stroke={color} strokeLinecap="round" strokeWidth="1.5"/>
        <path d="M26.2083 33.2917C30.1204 33.2917 33.2917 30.1204 33.2917 26.2083C33.2917 22.2963 30.1204 19.125 26.2083 19.125C22.2963 19.125 19.125 22.2963 19.125 26.2083C19.125 30.1204 22.2963 33.2917 26.2083 33.2917Z" stroke={color} strokeLinecap="round" strokeWidth="1.5"/>
        <path d="M23.375 26.9167L24.9093 28.4509C24.982 28.5236 25.0697 28.5795 25.1662 28.6148C25.2627 28.6502 25.3658 28.6641 25.4682 28.6556C25.5707 28.6471 25.6701 28.6164 25.7595 28.5657C25.8489 28.5149 25.9261 28.4453 25.9859 28.3617L29.0417 24.0833" stroke={color} strokeLinecap="round" strokeWidth="1.5"/>
      </g>
      <defs>
        <clipPath id="clip0">
          <rect width={34} height={34} fill="white"/>
        </clipPath>
      </defs>
    </svg>
  );
};

const EmailSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M21.5 3.95401H2.5C1.39543 3.95401 0.5 4.84944 0.5 5.95401V17.954C0.5 19.0586 1.39543 19.954 2.5 19.954H21.5C22.6046 19.954 23.5 19.0586 23.5 17.954V5.95401C23.5 4.84944 22.6046 3.95401 21.5 3.95401Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M22.911 4.53601L12 13.454L1.08899 4.53601' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

// const EmailfillSVG = ({ size = 16, color, extraClass }) => {
//   return (
//     <svg width={size} height={size} className={extraClass} fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
//       <path d="M20 5H4C2.89543 5 2 5.89543 2 7L11.1056 11.5528C11.6686 11.8343 12.3314 11.8343 12.8944 11.5528L22 7C22 5.89543 21.1046 5 20 5Z" fill={color}/>
//       <path d="M13.3416 13.3292L22 9V17C22 18.1046 21.1046 19 20 19H4C2.89543 19 2 18.1046 2 17V9L10.6584 13.3292C11.5029 13.7515 12.4971 13.7515 13.3416 13.3292Z" fill={color}/>
//     </svg>
//   );
// };

const SurveysparrowSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height='32' width='141' fill='none' viewBox='0 0 141 32' xmlns='http://www.w3.org/2000/svg'>
      <g clipPath='url(#clip0)'>
        <path d='M0 16.4746L0.967781 14.8663C1.60104 15.4601 2.85026 16.0379 3.90692 16.0379C4.87347 16.0379 5.33144 15.6711 5.33144 15.146C5.33144 13.7647 0.298728 14.9007 0.298728 11.5799C0.298728 10.163 1.53067 8.92157 3.78225 8.92157C5.20676 8.92157 6.35106 9.41104 7.1954 10.0759L6.29798 11.6486C5.78817 11.1248 4.82039 10.6709 3.78225 10.6709C2.97371 10.6709 2.44538 11.0205 2.44538 11.4916C2.44538 12.733 7.49413 11.6842 7.49413 15.0933C7.49413 16.65 6.15849 17.7859 3.79953 17.7859C2.32193 17.7859 0.89742 17.2965 0 16.4746Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M14.6931 17.5761V16.51C14.1117 17.1393 13.0921 17.7858 11.7021 17.7858C9.83691 17.7858 8.95801 16.7713 8.95801 15.1287V9.13123H11.1923V14.2541C11.1923 15.4256 11.807 15.8108 12.7575 15.8108C13.6204 15.8108 14.3067 15.3385 14.6931 14.849V9.13123H16.9286V17.5761H14.6931Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M18.9653 17.5761V9.13122H21.1996V10.2684C21.8156 9.53359 22.8513 8.92145 23.9079 8.92145V11.0891C23.7499 11.0547 23.5561 11.0376 23.2932 11.0376C22.5525 11.0376 21.5687 11.4571 21.1996 11.9993V17.5761H18.9653Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M27.8739 17.5761L24.4607 9.13123H26.8542L29.07 15.0061L31.3043 9.13123H33.6966L30.2847 17.5761H27.8739Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M38.1902 10.7403C36.8003 10.7403 36.2028 11.7021 36.1139 12.5412H40.3011C40.2307 11.7364 39.6678 10.7403 38.1902 10.7403ZM33.792 13.3459C33.792 10.8973 35.6214 8.9223 38.1902 8.9223C40.7405 8.9223 42.4477 10.8102 42.4477 13.5545V14.0795H36.1312C36.2904 15.1112 37.1348 15.9674 38.5766 15.9674C39.2987 15.9674 40.2825 15.6706 40.8294 15.1455L41.8317 16.6152C40.9874 17.3843 39.6505 17.7855 38.3297 17.7855C35.7461 17.7855 33.792 16.0558 33.792 13.3459Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M43.7741 18.8872C43.9667 18.9743 44.2654 19.027 44.4777 19.027C45.0592 19.027 45.4455 18.87 45.6566 18.4149L45.9726 17.6813L42.5249 9.13098H44.9172L47.1354 15.0058L49.3685 9.13098H51.762L47.7675 18.9571C47.1354 20.5482 46.0097 20.9677 44.5481 21.0033C44.3025 21.0033 43.7211 20.9506 43.4581 20.8622L43.7741 18.8872Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M77.4553 16.8704L78.4219 15.2622C79.0564 15.8559 80.3056 16.4337 81.361 16.4337C82.3276 16.4337 82.7855 16.0669 82.7855 15.5419C82.7855 14.1606 77.7541 15.2965 77.7541 11.9758C77.7541 10.5589 78.9848 9.31744 81.2363 9.31744C82.6621 9.31744 83.8064 9.80691 84.6495 10.4718L83.7533 12.0445C83.2423 11.5207 82.2745 11.0668 81.2363 11.0668C80.4278 11.0668 79.8995 11.4164 79.8995 11.8875C79.8995 13.1289 84.9482 12.08 84.9482 15.4891C84.9482 17.0459 83.6126 18.1818 81.2549 18.1818C79.7773 18.1818 78.3527 17.6924 77.4553 16.8704Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M92.7576 13.7413C92.7576 12.2901 91.8787 11.2927 90.5949 11.2927C89.8728 11.2927 89.0643 11.6963 88.6767 12.2557V15.2269C89.047 15.7691 89.8728 16.2058 90.5949 16.2058C91.8787 16.2058 92.7576 15.2097 92.7576 13.7413ZM88.6767 16.8879V21.1888H86.4436V9.52746H88.6767V10.5947C89.3284 9.77281 90.2604 9.31769 91.2986 9.31769C93.4798 9.31769 95.0623 10.9259 95.0623 13.7413C95.0623 16.5554 93.4798 18.1821 91.2986 18.1821C90.2962 18.1821 89.3803 17.7625 88.6767 16.8879Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M101.676 15.9438V14.8949C101.307 14.4054 100.603 14.1429 99.8822 14.1429C99.0021 14.1429 98.2812 14.6152 98.2812 15.42C98.2812 16.2235 99.0021 16.6786 99.8822 16.6786C100.603 16.6786 101.307 16.4332 101.676 15.9438ZM101.676 17.9716V17.0797C101.094 17.7802 100.092 18.1813 98.9836 18.1813C97.6306 18.1813 96.0457 17.2723 96.0457 15.3844C96.0457 13.3909 97.6306 12.6573 98.9836 12.6573C100.128 12.6573 101.113 13.0241 101.676 13.689V12.6218C101.676 11.7655 100.936 11.2061 99.8106 11.2061C98.9144 11.2061 98.0689 11.5557 97.3652 12.2022L96.4863 10.6467C97.5245 9.72054 98.8614 9.31818 100.198 9.31818C102.151 9.31818 103.927 10.0873 103.927 12.5175V17.9716H101.676Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M105.943 17.9721V9.52722H108.178V10.6644C108.794 9.92958 109.829 9.31744 110.886 9.31744V11.4851C110.728 11.4507 110.534 11.4336 110.271 11.4336C109.531 11.4336 108.547 11.8531 108.178 12.3953V17.9721H105.943Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M112.036 17.9721V9.52722H114.27V10.6644C114.886 9.92958 115.922 9.31744 116.978 9.31744V11.4851C116.82 11.4507 116.627 11.4336 116.364 11.4336C115.623 11.4336 114.639 11.8531 114.27 12.3953V17.9721H112.036Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M124.305 13.7413C124.305 12.4299 123.531 11.2927 122.123 11.2927C120.733 11.2927 119.959 12.4299 119.959 13.7413C119.959 15.0698 120.733 16.2058 122.123 16.2058C123.531 16.2058 124.305 15.0698 124.305 13.7413ZM117.637 13.7413C117.637 11.3455 119.327 9.31769 122.123 9.31769C124.939 9.31769 126.627 11.3455 126.627 13.7413C126.627 16.1359 124.939 18.1821 122.123 18.1821C119.327 18.1821 117.637 16.1359 117.637 13.7413Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M135.504 17.972L133.71 12.2199L131.916 17.972H129.522L126.937 9.52719H129.259L130.843 15.2094L132.707 9.52719H134.696L136.56 15.2094L138.143 9.52719H140.482L137.896 17.972H135.504Z' fill='#63686F' fillRule='evenodd'/>
        <path d='M60.7224 0C59.4631 0.148671 57.5016 0.69452 55.8181 2.55345L53.3826 5.27618L59.534 7.26099L52.2341 22.7878L54.0749 22.6088C54.4381 22.5741 63.0061 21.6538 65.2688 14.1942L63.0249 14.1921C62.1666 16.4395 60.4654 18.1769 57.938 19.3521C57.1782 19.7048 56.4417 19.9577 55.8115 20.1367L62.4501 6.01628L57.083 4.28649L57.4053 3.92621C59.3158 1.8166 61.6704 2.01953 61.7623 2.02713C67.8461 2.40043 70.4821 6.91047 70.4189 11.2176C70.3226 17.8275 64.3861 25.1318 51.5807 25.5518L50.9294 25.5735L47.9136 32H50.2416L52.3083 27.5985C66.0452 26.8985 72.4269 18.6913 72.5354 11.2469C72.6263 5.1134 68.4397 0.543679 62.3161 0H60.7224Z' fill='#E59356' fillRule='evenodd'/>
      </g>
      <defs>
        <clipPath id='clip0'>
          <rect height='32' width='141' fill='white'/>
        </clipPath>
      </defs>
    </svg>
  );
};

const DesktopSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill='none' viewBox='0 0 21 21' xmlns='http://www.w3.org/2000/svg'>
      <path d='M18.2822 2H2.82226C1.87749 2 1.10449 2.77685 1.10449 3.72631V14.0842C1.10449 15.0336 1.87749 15.8105 2.82226 15.8105H8.83448V17.5368H7.1167V19.2632H13.9878V17.5368H12.2701V15.8105H18.2822C19.227 15.8105 20 15.0336 20 14.0842V3.72631C20 2.77685 19.227 2 18.2822 2ZM18.2822 14.0842H2.82226V3.72631H18.2822V14.0842Z' fill={color}/>
    </svg>
  );
};

const TabletSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill='none' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
      <path d='M17.8947 2H6.10527C4.94316 2 4 2.96772 4 4.16009V20.5767C4 21.7691 4.94316 22.7368 6.10527 22.7368H17.8947C19.0568 22.7368 20 21.7691 20 20.5767V4.16009C20 2.96772 19.0568 2 17.8947 2ZM12 21.8728C11.301 21.8728 10.7368 21.2938 10.7368 20.5767C10.7368 19.8595 11.301 19.2806 12 19.2806C12.6989 19.2806 13.2632 19.8595 13.2632 20.5767C13.2632 21.2938 12.6989 21.8728 12 21.8728ZM18.3158 18.4166H5.68421V4.59209H18.3158V18.4166Z' fill={color}/>
    </svg>
  );
};

const MobileSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill='none' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
      <path d='M15.7115 1.62107H7.91094C6.56535 1.62107 5.47328 2.65854 5.47328 3.93686V19.6842C5.47328 20.9626 6.56535 22 7.91094 22H15.7115C17.0571 22 18.1492 20.9626 18.1492 19.6842V3.93686C18.1492 2.65854 17.0571 1.62107 15.7115 1.62107ZM11.8112 21.0737C11.0019 21.0737 10.3486 20.453 10.3486 19.6842C10.3486 18.9154 11.0019 18.2947 11.8112 18.2947C12.6205 18.2947 13.2738 18.9154 13.2738 19.6842C13.2738 20.453 12.6205 21.0737 11.8112 21.0737ZM16.199 17.3684H7.42341V4.40002H16.199V17.3684Z' fill={color}/>
    </svg>
  );
};

const HomeSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} fill='none' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
      <path d='M3 9L12 2L21 9V20C21 20.5304 20.7893 21.0391 20.4142 21.4142C20.0391 21.7893 19.5304 22 19 22H5C4.46957 22 3.96086 21.7893 3.58579 21.4142C3.21071 21.0391 3 20.5304 3 20V9Z' fill={color} stroke={color} strokeLinecap='round' strokeWidth='1'/>
      <path d='M9 21V10H15V21' fill='white'/>
      <path d='M9 21V10H15V21' stroke={color} strokeLinecap='round' strokeWidth='1'/>
    </svg>
  );
};
const ContactsSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path d='M17 21V19C17 17.9391 16.5786 16.9217 15.8284 16.1716C15.0783 15.4214 14.0609 15 13 15H5C3.93913 15 2.92172 15.4214 2.17157 16.1716C1.42143 16.9217 1 17.9391 1 19V21' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M9 11C11.2091 11 13 9.20914 13 7C13 4.79086 11.2091 3 9 3C6.79086 3 5 4.79086 5 7C5 9.20914 6.79086 11 9 11Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M23 21V19C22.9993 18.1137 22.7044 17.2528 22.1614 16.5523C21.6184 15.8519 20.8581 15.3516 20 15.13' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M16 3.13C16.8604 3.3503 17.623 3.8507 18.1676 4.55231C18.7122 5.25392 19.0078 6.11683 19.0078 7.005C19.0078 7.89317 18.7122 8.75608 18.1676 9.45769C17.623 10.1593 16.8604 10.6597 16 10.88' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
  </svg>
  );
};
const SettingsSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass}  viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path d='M12 15C13.6569 15 15 13.6569 15 12C15 10.3431 13.6569 9 12 9C10.3431 9 9 10.3431 9 12C9 13.6569 10.3431 15 12 15Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M19.4 15C19.2669 15.3016 19.2272 15.6362 19.286 15.9606C19.3448 16.285 19.4995 16.5843 19.73 16.82L19.79 16.88C19.976 17.0657 20.1235 17.2863 20.2241 17.5291C20.3248 17.7719 20.3766 18.0322 20.3766 18.295C20.3766 18.5578 20.3248 18.8181 20.2241 19.0609C20.1235 19.3037 19.976 19.5243 19.79 19.71C19.6043 19.896 19.3837 20.0435 19.1409 20.1441C18.8981 20.2448 18.6378 20.2966 18.375 20.2966C18.1122 20.2966 17.8519 20.2448 17.6091 20.1441C17.3663 20.0435 17.1457 19.896 16.96 19.71L16.9 19.65C16.6643 19.4195 16.365 19.2648 16.0406 19.206C15.7162 19.1472 15.3816 19.1869 15.08 19.32C14.7842 19.4468 14.532 19.6572 14.3543 19.9255C14.1766 20.1938 14.0813 20.5082 14.08 20.83V21C14.08 21.5304 13.8693 22.0391 13.4942 22.4142C13.1191 22.7893 12.6104 23 12.08 23C11.5496 23 11.0409 22.7893 10.6658 22.4142C10.2907 22.0391 10.08 21.5304 10.08 21V20.91C10.0723 20.579 9.96512 20.258 9.77251 19.9887C9.5799 19.7194 9.31074 19.5143 9 19.4C8.69838 19.2669 8.36381 19.2272 8.03941 19.286C7.71502 19.3448 7.41568 19.4995 7.18 19.73L7.12 19.79C6.93425 19.976 6.71368 20.1235 6.47088 20.2241C6.22808 20.3248 5.96783 20.3766 5.705 20.3766C5.44217 20.3766 5.18192 20.3248 4.93912 20.2241C4.69632 20.1235 4.47575 19.976 4.29 19.79C4.10405 19.6043 3.95653 19.3837 3.85588 19.1409C3.75523 18.8981 3.70343 18.6378 3.70343 18.375C3.70343 18.1122 3.75523 17.8519 3.85588 17.6091C3.95653 17.3663 4.10405 17.1457 4.29 16.96L4.35 16.9C4.58054 16.6643 4.73519 16.365 4.794 16.0406C4.85282 15.7162 4.81312 15.3816 4.68 15.08C4.55324 14.7842 4.34276 14.532 4.07447 14.3543C3.80618 14.1766 3.49179 14.0813 3.17 14.08H3C2.46957 14.08 1.96086 13.8693 1.58579 13.4942C1.21071 13.1191 1 12.6104 1 12.08C1 11.5496 1.21071 11.0409 1.58579 10.6658C1.96086 10.2907 2.46957 10.08 3 10.08H3.09C3.42099 10.0723 3.742 9.96512 4.0113 9.77251C4.28059 9.5799 4.48572 9.31074 4.6 9C4.73312 8.69838 4.77282 8.36381 4.714 8.03941C4.65519 7.71502 4.50054 7.41568 4.27 7.18L4.21 7.12C4.02405 6.93425 3.87653 6.71368 3.77588 6.47088C3.67523 6.22808 3.62343 5.96783 3.62343 5.705C3.62343 5.44217 3.67523 5.18192 3.77588 4.93912C3.87653 4.69632 4.02405 4.47575 4.21 4.29C4.39575 4.10405 4.61632 3.95653 4.85912 3.85588C5.10192 3.75523 5.36217 3.70343 5.625 3.70343C5.88783 3.70343 6.14808 3.75523 6.39088 3.85588C6.63368 3.95653 6.85425 4.10405 7.04 4.29L7.1 4.35C7.33568 4.58054 7.63502 4.73519 7.95941 4.794C8.28381 4.85282 8.61838 4.81312 8.92 4.68H9C9.29577 4.55324 9.54802 4.34276 9.72569 4.07447C9.90337 3.80618 9.99872 3.49179 10 3.17V3C10 2.46957 10.2107 1.96086 10.5858 1.58579C10.9609 1.21071 11.4696 1 12 1C12.5304 1 13.0391 1.21071 13.4142 1.58579C13.7893 1.96086 14 2.46957 14 3V3.09C14.0013 3.41179 14.0966 3.72618 14.2743 3.99447C14.452 4.26276 14.7042 4.47324 15 4.6C15.3016 4.73312 15.6362 4.77282 15.9606 4.714C16.285 4.65519 16.5843 4.50054 16.82 4.27L16.88 4.21C17.0657 4.02405 17.2863 3.87653 17.5291 3.77588C17.7719 3.67523 18.0322 3.62343 18.295 3.62343C18.5578 3.62343 18.8181 3.67523 19.0609 3.77588C19.3037 3.87653 19.5243 4.02405 19.71 4.21C19.896 4.39575 20.0435 4.61632 20.1441 4.85912C20.2448 5.10192 20.2966 5.36217 20.2966 5.625C20.2966 5.88783 20.2448 6.14808 20.1441 6.39088C20.0435 6.63368 19.896 6.85425 19.71 7.04L19.65 7.1C19.4195 7.33568 19.2648 7.63502 19.206 7.95941C19.1472 8.28381 19.1869 8.61838 19.32 8.92V9C19.4468 9.29577 19.6572 9.54802 19.9255 9.72569C20.1938 9.90337 20.5082 9.99872 20.83 10H21C21.5304 10 22.0391 10.2107 22.4142 10.5858C22.7893 10.9609 23 11.4696 23 12C23 12.5304 22.7893 13.0391 22.4142 13.4142C22.0391 13.7893 21.5304 14 21 14H20.91C20.5882 14.0013 20.2738 14.0966 20.0055 14.2743C19.7372 14.452 19.5268 14.7042 19.4 15V15Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
  </svg>
  );
};
const DollarSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass}  viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path d='M12 1V23' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M17 5H9.5C8.57174 5 7.6815 5.36875 7.02513 6.02513C6.36875 6.6815 6 7.57174 6 8.5C6 9.42826 6.36875 10.3185 7.02513 10.9749C7.6815 11.6313 8.57174 12 9.5 12H14.5C15.4283 12 16.3185 12.3687 16.9749 13.0251C17.6313 13.6815 18 14.5717 18 15.5C18 16.4283 17.6313 17.3185 16.9749 17.9749C16.3185 18.6313 15.4283 19 14.5 19H6' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};
const MusicSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg  width={size} height={size} className={extraClass}  viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path d='M9 18V5L21 3V16' stroke={color}strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M6 21C7.65685 21 9 19.6569 9 18C9 16.3431 7.65685 15 6 15C4.34315 15 3 16.3431 3 18C3 19.6569 4.34315 21 6 21Z' stroke={color}strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M18 19C19.6569 19 21 17.6569 21 16C21 14.3431 19.6569 13 18 13C16.3431 13 15 14.3431 15 16C15 17.6569 16.3431 19 18 19Z' stroke={color}strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
  </svg>
  );
};
const SupportSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path d='M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M12 16C14.2091 16 16 14.2091 16 12C16 9.79086 14.2091 8 12 8C9.79086 8 8 9.79086 8 12C8 14.2091 9.79086 16 12 16Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M4.92993 4.93L9.16993 9.17' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M14.8301 14.83L19.0701 19.07' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M14.8301 9.17L19.0701 4.93' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M14.8301 9.17L18.3601 5.64' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M4.92993 19.07L9.16993 14.83' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};
const LogoutSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path d='M9 21H5C4.46957 21 3.96086 20.7893 3.58579 20.4142C3.21071 20.0391 3 19.5304 3 19V5C3 4.46957 3.21071 3.96086 3.58579 3.58579C3.96086 3.21071 4.46957 3 5 3H9' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M16 17L21 12L16 7' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M21 12H9' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
  </svg>
  );
};

const UserSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path d='M17 21V19C17 17.9391 16.5786 16.9217 15.8284 16.1716C15.0783 15.4214 14.0609 15 13 15H5C3.93913 15 2.92172 15.4214 2.17157 16.1716C1.42143 16.9217 1 17.9391 1 19V21' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M9 11C11.2091 11 13 9.20914 13 7C13 4.79086 11.2091 3 9 3C6.79086 3 5 4.79086 5 7C5 9.20914 6.79086 11 9 11Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M23 21V19C22.9993 18.1137 22.7044 17.2528 22.1614 16.5523C21.6184 15.8519 20.8581 15.3516 20 15.13' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <path d='M16 3.13C16.8604 3.3503 17.623 3.8507 18.1676 4.55231C18.7122 5.25392 19.0078 6.11683 19.0078 7.005C19.0078 7.89317 18.7122 8.75608 18.1676 9.45769C17.623 10.1593 16.8604 10.6597 16 10.88' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
  </svg>
  );
};

const UsersettingsSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 23 21' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M16.9375 15.47C17.5327 15.47 18.0152 14.9874 18.0152 14.3922C18.0152 13.797 17.5327 13.3145 16.9375 13.3145C16.3423 13.3145 15.8597 13.797 15.8597 14.3922C15.8597 14.9874 16.3423 15.47 16.9375 15.47Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M18.063 9.77024L18.4311 10.9542C18.4921 11.1506 18.6271 11.3171 18.8088 11.42C18.9905 11.523 19.2054 11.5547 19.4101 11.5088L20.638 11.2307C20.8707 11.1794 21.1141 11.2023 21.3325 11.2961C21.5509 11.3899 21.7329 11.5495 21.8517 11.7517C21.9705 11.9539 22.0199 12.188 21.9927 12.4196C21.9655 12.6513 21.8631 12.8683 21.7005 13.0389L20.8416 13.9448C20.6997 14.0955 20.621 14.2929 20.621 14.4977C20.621 14.7025 20.6997 14.8998 20.8416 15.0505L21.7005 15.9564C21.8631 16.127 21.9655 16.344 21.9927 16.5757C22.0199 16.8073 21.9705 17.0414 21.8517 17.2436C21.7329 17.4458 21.5509 17.6055 21.3325 17.6992C21.1141 17.793 20.8707 17.8159 20.638 17.7647L19.4101 17.4865C19.2044 17.4396 18.9882 17.4712 18.8056 17.5749C18.6229 17.6787 18.4878 17.8466 18.4276 18.0445L18.0595 19.2285C17.9906 19.4516 17.85 19.6471 17.6585 19.786C17.4671 19.925 17.235 20 16.9965 20C16.7581 20 16.526 19.925 16.3345 19.786C16.1431 19.6471 16.0025 19.4516 15.9335 19.2285L15.568 18.0445C15.5076 17.8479 15.3729 17.6811 15.1912 17.578C15.0096 17.4748 14.7946 17.443 14.5899 17.4891L13.362 17.7672C13.1293 17.8185 12.8859 17.7956 12.6675 17.7018C12.4491 17.608 12.2671 17.4483 12.1483 17.2461C12.0295 17.0439 11.9801 16.8099 12.0073 16.5782C12.0345 16.3466 12.1369 16.1296 12.2995 15.9589L13.1584 15.0531C13.3003 14.9024 13.379 14.705 13.379 14.5002C13.379 14.2954 13.3003 14.0981 13.1584 13.9473L12.2995 13.0415C12.1369 12.8709 12.0345 12.6538 12.0073 12.4222C11.9801 12.1905 12.0295 11.9565 12.1483 11.7543C12.2671 11.5521 12.4491 11.3924 12.6675 11.2986C12.8859 11.2049 13.1293 11.1819 13.362 11.2332L14.5899 11.5113C14.7945 11.5574 15.0094 11.5258 15.191 11.4228C15.3726 11.3198 15.5074 11.1532 15.568 10.9568L15.937 9.77279C16.0057 9.5496 16.146 9.35394 16.3373 9.21477C16.5286 9.07559 16.7606 9.00029 16.999 9C17.2375 8.99971 17.4697 9.07446 17.6613 9.21318C17.8529 9.3519 17.9938 9.54722 18.063 9.77024V9.77024Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M6.54167 19.9999L6.9375 15.2499H8.91667V12.4791C8.91667 11.4293 8.49963 10.4225 7.7573 9.68012C7.01497 8.93779 6.00815 8.52075 4.95833 8.52075C3.90852 8.52075 2.9017 8.93779 2.15937 9.68012C1.41704 10.4225 1 11.4293 1 12.4791V15.2499H2.97917L3.375 19.9999H6.54167Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M4.95833 6.54167C6.48862 6.54167 7.72917 5.30112 7.72917 3.77083C7.72917 2.24054 6.48862 1 4.95833 1C3.42804 1 2.1875 2.24054 2.1875 3.77083C2.1875 5.30112 3.42804 6.54167 4.95833 6.54167Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const ProfileSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 32 32' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <mask id='mask0' mask-type='alpha' maskUnits='userSpaceOnUse' x='0' y='0' width='32' height='32'>
        <circle opacity='0.8' cx='16' cy='16' r='16' fill={color}/>
      </mask>
      <g mask='url(#mask0)'>
        <path opacity='0.8' d='M24 30V28C24 26.9391 23.5786 25.9217 22.8284 25.1716C22.0783 24.4214 21.0609 24 20 24H12C10.9391 24 9.92172 24.4214 9.17157 25.1716C8.42143 25.9217 8 26.9391 8 28V30' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
        <path opacity='0.8' d='M16 20C18.2091 20 20 18.2091 20 16C20 13.7909 18.2091 12 16 12C13.7909 12 12 13.7909 12 16C12 18.2091 13.7909 20 16 20Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
        <circle opacity='0.3' cx='16' cy='16' r='16' fill={color}/>
      </g>
    </svg>
  );
};

const ProfilesettingsSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 17 19' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M8.45801 10.1897C9.8042 10.1897 10.8955 9.09839 10.8955 7.7522C10.8955 6.406 9.8042 5.3147 8.45801 5.3147C7.11181 5.3147 6.02051 6.406 6.02051 7.7522C6.02051 9.09839 7.11181 10.1897 8.45801 10.1897Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M12.25 13.9396C11.9699 13.17 11.4598 12.5053 10.7889 12.0356C10.1181 11.5659 9.31894 11.314 8.5 11.314C7.68106 11.314 6.88194 11.5659 6.21109 12.0356C5.54024 12.5053 5.03014 13.17 4.75 13.9396' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M15.25 1H1.75C1.33579 1 1 1.33579 1 1.75V17.5C1 17.9142 1.33579 18.25 1.75 18.25H15.25C15.6642 18.25 16 17.9142 16 17.5V1.75C16 1.33579 15.6642 1 15.25 1Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const ChatSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 25 25' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M9.02717 13.8098H8.07065L4.24456 17.6359V13.8098H2.33152C2.07784 13.8098 1.83454 13.709 1.65516 13.5296C1.47578 13.3502 1.375 13.1069 1.375 12.8533V2.33152C1.375 2.07784 1.47578 1.83454 1.65516 1.65516C1.83454 1.47578 2.07784 1.375 2.33152 1.375H18.5924C18.8461 1.375 19.0894 1.47578 19.2688 1.65516C19.4481 1.83454 19.5489 2.07784 19.5489 2.33152V8.07065' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M10.9402 18.5924C10.9402 18.8461 11.041 19.0894 11.2203 19.2688C11.3997 19.4481 11.643 19.5489 11.8967 19.5489H17.1576L21.4619 23.375V19.5489H22.4184C22.6721 19.5489 22.9154 19.4481 23.0948 19.2688C23.2742 19.0894 23.375 18.8461 23.375 18.5924V10.9402C23.375 10.6865 23.2742 10.4432 23.0948 10.2639C22.9154 10.0845 22.6721 9.9837 22.4184 9.9837H11.8967C11.643 9.9837 11.3997 10.0845 11.2203 10.2639C11.041 10.4432 10.9402 10.6865 10.9402 10.9402V18.5924Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const OfflineSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 26 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M22.179 14.1216C22.9836 13.7497 23.661 13.1491 24.1267 12.3949C24.5925 11.6406 24.8259 10.7659 24.798 9.87991C24.7882 9.21654 24.6447 8.56196 24.3762 7.9553C24.1076 7.34864 23.7195 6.80235 23.2351 6.34909C22.7506 5.89582 22.1797 5.54489 21.5566 5.31727C20.9334 5.08964 20.2707 4.99 19.6082 5.02429C19.0231 3.84676 18.1193 2.85705 16.9996 2.16775C15.8799 1.47844 14.5892 1.11721 13.2744 1.12514C11.5132 1.10151 9.80935 1.75039 8.51004 2.93953C7.21073 4.12867 6.4138 5.76854 6.28169 7.52491C5.74274 7.40641 5.1842 7.40896 4.64635 7.53237C4.1085 7.65578 3.60472 7.89697 3.17134 8.23857C2.73795 8.58017 2.38574 9.01367 2.14011 9.50781C1.89447 10.002 1.76152 10.5444 1.75084 11.0962C1.70866 11.8868 1.9489 12.6666 2.42865 13.2964C2.9084 13.9263 3.59642 14.365 4.36983 14.5344' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M13.2751 23.1247C16.7465 23.1247 19.5607 20.3106 19.5607 16.8391C19.5607 13.3677 16.7465 10.5536 13.2751 10.5536C9.80365 10.5536 6.9895 13.3677 6.9895 16.8391C6.9895 20.3106 9.80365 23.1247 13.2751 23.1247Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M15.4969 14.6171L11.053 19.061' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M15.4969 19.061L11.053 14.6171' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>

  );
};

const NpsSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 25 15' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M19.0034 10.134C19.2658 10.8778 19.3994 11.661 19.3983 12.4498' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M5.50342 12.4492C5.50339 11.3377 5.77003 10.2425 6.28095 9.25542C6.79187 8.26835 7.53216 7.41828 8.43965 6.77656C9.34715 6.13485 10.3954 5.72024 11.4963 5.56753C12.5972 5.41483 13.7187 5.52849 14.7666 5.89898' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M12.4505 14.1862C13.4097 14.1862 14.1873 13.4086 14.1873 12.4494C14.1873 11.4902 13.4097 10.7126 12.4505 10.7126C11.4912 10.7126 10.7136 11.4902 10.7136 12.4494C10.7136 13.4086 11.4912 14.1862 12.4505 14.1862Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M13.6787 11.2207L18.8186 6.08084' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M23.4502 13.6074C23.4502 11.8705 23.0265 8.95252 22.2158 7.38879C21.4051 5.82505 20.2307 4.47886 18.7915 3.46345C17.3522 2.44804 15.6902 1.7931 13.9451 1.55372C12.2001 1.31434 10.4231 1.49752 8.76354 2.08785C7.10401 2.67819 5.61052 3.65841 4.40867 4.94607C3.20683 6.23373 2.33178 7.79118 1.85712 9.48742C1.48152 10.8297 1.36533 12.2278 1.51107 13.6073' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const NpsworkflowSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <defs/>
      <circle fill="none" stroke={color} strokeWidth={1.5} strokeLinecap="round" strokeLinejoin="round" cx="17.75" cy="17.031" r="1.437"/>
      <path fill="none" stroke={color} strokeWidth={1.5} strokeLinecap="round" strokeLinejoin="round" d="M18.971,11.47l.424,1.392a.951.951,0,0,0,1.124.653l1.412-.327a1.283,1.283,0,0,1,1.221,2.126l-.988,1.065a.96.96,0,0,0,0,1.3l.988,1.065a1.283,1.283,0,0,1-1.221,2.126l-1.412-.328A.953.953,0,0,0,19.4,21.2l-.424,1.392a1.275,1.275,0,0,1-2.442,0L16.105,21.2a.953.953,0,0,0-1.124-.653l-1.412.328a1.283,1.283,0,0,1-1.221-2.126l.988-1.065a.96.96,0,0,0,0-1.3l-.988-1.065a1.283,1.283,0,0,1,1.221-2.126l1.412.327a.951.951,0,0,0,1.124-.653l.424-1.392A1.275,1.275,0,0,1,18.971,11.47Z"/>
      <path fill="none" stroke={color} strokeWidth={1.5} strokeLinecap="round" strokeLinejoin="round" d="M9.5,14.5H2A1.5,1.5,0,0,1,.5,13V2A1.5,1.5,0,0,1,2,.5H20A1.5,1.5,0,0,1,21.5,2V9.5"/>
      <polyline fill="none" stroke={color} strokeWidth={1.5} strokeLinecap="round" strokeLinejoin="round" points="21.067 0.946 11 9 0.933 0.946"/>
    </svg>
  );
};

const ChatsingleSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 20 19' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M15 1H5C3.93913 1 2.92172 1.42143 2.17157 2.17157C1.42143 2.92172 1 3.93913 1 5V10C1 11.0609 1.42143 12.0783 2.17157 12.8284C2.92172 13.5786 3.93913 14 5 14H6V18L10.5 14H15C16.0609 14 17.0783 13.5786 17.8284 12.8284C18.5786 12.0783 19 11.0609 19 10V5C19 3.93913 18.5786 2.92172 17.8284 2.17157C17.0783 1.42143 16.0609 1 15 1V1Z' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const QuestionSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} version="1.1" viewBox="0 0 31.357 31.357" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" xmlSpace="preserve">
      <path fill={color} d="M15.255,0c5.424,0,10.764,2.498,10.764,8.473c0,5.51-6.314,7.629-7.67,9.62c-1.018,1.481-0.678,3.562-3.475,3.562&#xA;&#x9;&#x9;c-1.822,0-2.712-1.482-2.712-2.838c0-5.046,7.414-6.188,7.414-10.343c0-2.287-1.522-3.643-4.066-3.643&#xA;&#x9;&#x9;c-5.424,0-3.306,5.592-7.414,5.592c-1.483,0-2.756-0.89-2.756-2.584C5.339,3.683,10.084,0,15.255,0z M15.044,24.406&#xA;&#x9;&#x9;c1.904,0,3.475,1.566,3.475,3.476c0,1.91-1.568,3.476-3.475,3.476c-1.907,0-3.476-1.564-3.476-3.476&#xA;&#x9;&#x9;C11.568,25.973,13.137,24.406,15.044,24.406z"/>
    </svg>
  );
};

const QuestionoutlineSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill="none" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
      <path d="M8.19329 3C9.94017 3 11.66 3.80452 11.66 5.72887C11.66 7.50345 9.62648 8.18591 9.18976 8.82714C8.86189 9.30412 8.9714 9.97434 8.07058 9.97434C7.48377 9.97434 7.19713 9.49704 7.19713 9.06032C7.19713 7.43517 9.58493 7.06737 9.58493 5.72919C9.58493 4.99262 9.09475 4.5559 8.27541 4.5559C6.52853 4.5559 7.21066 6.35689 5.88761 6.35689C5.40999 6.35689 5 6.07026 5 5.52468C4.99968 4.18617 6.52788 3 8.19329 3ZM8.12533 10.8603C8.73854 10.8603 9.24451 11.3647 9.24451 11.9798C9.24451 12.595 8.73951 13.0993 8.12533 13.0993C7.51115 13.0993 7.00583 12.5956 7.00583 11.9798C7.00583 11.365 7.51115 10.8603 8.12533 10.8603Z" fill={color}/>
      <circle cx="8" cy="8" r="7.75" stroke={color} strokeWidth="0.5"/>
    </svg>
  );
};

const SmileSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 19 14' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M1 4C1 3.20435 1.31607 2.44129 1.87868 1.87868C2.44129 1.31607 3.20435 1 4 1C4.79565 1 5.55871 1.31607 6.12132 1.87868C6.68393 2.44129 7 3.20435 7 4' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M17.5 4C17.5 3.20435 17.1839 2.44129 16.6213 1.87868C16.0587 1.31607 15.2956 1 14.5 1C13.7044 1 12.9413 1.31607 12.3787 1.87868C11.8161 2.44129 11.5 3.20435 11.5 4' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M13 9.25C13 10.2446 12.6049 11.1984 11.9017 11.9017C11.1984 12.6049 10.2446 13 9.25 13C8.25544 13 7.30161 12.6049 6.59835 11.9017C5.89509 11.1984 5.5 10.2446 5.5 9.25' stroke={color} strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const GearSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 21 23' version='1.1' xmlns='http://www.w3.org/2000/svg'>
      <g stroke='none' strokeWidth='1' fill='none' fillRule='evenodd' strokeLinecap='round' strokeLinejoin='round'>
          <g id='03.1-Configuration' transform='translate(-938.000000, -19.000000)' stroke={color} strokeWidth='1.3'>
              <g id='folder-settings' transform='translate(939.000000, 19.000000)'>
                  <ellipse id='Oval' cx='9.58333333' cy='11.5509286' rx='2.395' ry='2.36078571'></ellipse>
                  <path d='M11.6183333,2.415 L12.325,4.70185714 C12.5653522,5.48997599 13.3847103,5.95919074 14.1983333,5.77464286 L16.5516667,5.23742857 C17.4665618,5.0341574 18.4090583,5.44224768 18.8761315,6.24389538 C19.3432046,7.04554308 19.2258359,8.05363737 18.5866667,8.73014286 L16.94,10.4797857 C16.3766002,11.0833842 16.3766002,12.0119015 16.94,12.6155 L18.5866667,14.3651429 C19.2258359,15.0416483 19.3432046,16.0497426 18.8761315,16.8513903 C18.4090583,17.653038 17.4665618,18.0611283 16.5516667,17.8578571 L14.1983333,17.319 C13.3853122,17.1417595 12.5714384,17.6135009 12.3333333,18.4 L11.6266667,20.6868571 C11.3565954,21.5722294 10.5295944,22.1783588 9.59166667,22.1783588 C8.65373894,22.1783588 7.82673792,21.5722294 7.55666667,20.6868571 L6.84166667,18.4 C6.59993271,17.6129575 5.78169225,17.1443827 4.96833333,17.3272143 L2.615,17.8660714 C1.70010489,18.0693426 0.75760833,17.6612523 0.290535177,16.8596046 C-0.176537975,16.0579569 -0.0591691954,15.0498626 0.58,14.3733571 L2.22666667,12.6237143 C2.79006645,12.0201158 2.79006645,11.0915985 2.22666667,10.488 L0.58,8.73835714 C-0.0591691954,8.06185165 -0.176537975,7.05375737 0.290535177,6.25210967 C0.75760833,5.45046197 1.70010489,5.04237168 2.615,5.24564286 L4.96833333,5.78285714 C5.7819564,5.96740503 6.60131448,5.49819027 6.84166667,4.71007143 L7.54833333,2.42321429 C7.81656025,1.5372918 8.64230395,0.929487575 9.5802352,0.92759016 C10.5181664,0.925701612 11.3464282,1.53016764 11.6183333,2.415 Z' id='Path'></path>
              </g>
          </g>
      </g>
    </svg>
  );
};

const GearfillSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill='none' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
      <path d='M24 13.616V10.384C22.349 9.797 21.306 9.632 20.781 8.365V8.364C20.254 7.093 20.881 6.23 21.628 4.657L19.343 2.372C17.782 3.114 16.91 3.747 15.636 3.219H15.635C14.366 2.693 14.2 1.643 13.616 0H10.384C9.802 1.635 9.635 2.692 8.365 3.219H8.364C7.093 3.747 6.232 3.121 4.657 2.372L2.372 4.657C3.117 6.225 3.747 7.091 3.219 8.364C2.692 9.635 1.635 9.802 0 10.384V13.616C1.632 14.196 2.692 14.365 3.219 15.635C3.749 16.917 3.105 17.801 2.372 19.342L4.657 21.628C6.219 20.885 7.091 20.253 8.364 20.781H8.365C9.635 21.307 9.801 22.36 10.384 24H13.616C14.198 22.364 14.366 21.31 15.643 20.778H15.644C16.906 20.254 17.764 20.879 19.342 21.629L21.627 19.343C20.883 17.78 20.252 16.91 20.779 15.637C21.306 14.366 22.367 14.197 24 13.616ZM12 16C9.791 16 8 14.209 8 12C8 9.791 9.791 8 12 8C14.209 8 16 9.791 16 12C16 14.209 14.209 16 12 16Z' fill='#63686F'/>
    </svg>
  );
};

const VariablesSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={[extraClass]}  viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <circle cx='10' cy='10' r='9' fill='white'/>
      <path d='M11.5623 7.26086H9.88596C9.55919 7.26098 9.24203 7.3714 8.98584 7.57424C8.72964 7.77708 8.54942 8.06045 8.47435 8.37849C8.39928 8.69652 8.43376 9.03058 8.57221 9.32657C8.71065 9.62256 8.94496 9.86315 9.23719 10.0094L10.7633 10.7732C11.0555 10.9194 11.2898 11.16 11.4282 11.456C11.5667 11.752 11.6012 12.0861 11.5261 12.4041C11.451 12.7221 11.2708 13.0055 11.0146 13.2084C10.7584 13.4112 10.4413 13.5216 10.1145 13.5217H8.43188' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M9.60791 13.5216V14.6955' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M10.3914 6.08704V7.26094' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M10 19C14.9706 19 19 14.9706 19 10C19 5.02944 14.9706 1 10 1C5.02944 1 1 5.02944 1 10C1 14.9706 5.02944 19 10 19Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const FacebookSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M17.768 7.5H13.5V5.6C13.481 5.45861 13.4934 5.31477 13.5363 5.1787C13.5792 5.04264 13.6515 4.91767 13.748 4.81268C13.8446 4.7077 13.9632 4.62526 14.0952 4.57122C14.2272 4.51718 14.3695 4.49286 14.512 4.5C14.93 4.5 17.5 4.51 17.5 4.51V0.5H13.171C9.244 0.5 8.5 3.474 8.5 5.355V7.5H5.5V11.5H8.5V23.5H13.5V11.5H17.351L17.768 7.5Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const FacebookfillSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <path d="M12 0C5.373 0 0 5.373 0 12C0 18.627 5.373 24 12 24C18.627 24 24 18.627 24 12C24 5.373 18.627 0 12 0ZM15 8H13.65C13.112 8 13 8.221 13 8.778V10H15L14.791 12H13V19H10V12H8V10H10V7.692C10 5.923 10.931 5 13.029 5H15V8Z" fill={color}/>
    </svg>
  );
};

const TwitterSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M5 3.73913C5 3.01267 5.26029 2.31596 5.72362 1.80227C6.18694 1.28859 6.81535 1 7.47059 1C8.12583 1 8.75423 1.28859 9.21756 1.80227C9.68088 2.31596 9.94118 3.01267 9.94118 3.73913V5.56522H16.5294C17.1847 5.56522 17.8131 5.8538 18.2764 6.36749C18.7397 6.88118 19 7.57789 19 8.30435C19 9.03081 18.7397 9.72752 18.2764 10.2412C17.8131 10.7549 17.1847 11.0435 16.5294 11.0435H9.94118V13.7826C9.94118 14.5091 10.2015 15.2058 10.6648 15.7195C11.1281 16.2332 11.7565 16.5217 12.4118 16.5217H16.5294C17.1847 16.5217 17.8131 16.8103 18.2764 17.324C18.7397 17.8377 19 18.5344 19 19.2609C19 19.9873 18.7397 20.684 18.2764 21.1977C17.8131 21.7114 17.1847 22 16.5294 22H11.5882C9.84093 22 8.16518 21.2304 6.92965 19.8606C5.69412 18.4908 5 16.6329 5 14.6957V3.73913Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const TwitterfillSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <path d="M12 0C5.373 0 0 5.373 0 12C0 18.627 5.373 24 12 24C18.627 24 24 18.627 24 12C24 5.373 18.627 0 12 0ZM18.066 9.645C18.249 13.685 15.236 18.189 9.902 18.189C8.28 18.189 6.771 17.713 5.5 16.898C7.024 17.078 8.545 16.654 9.752 15.709C8.496 15.686 7.435 14.855 7.068 13.714C7.519 13.8 7.963 13.775 8.366 13.665C6.985 13.387 6.031 12.143 6.062 10.812C6.45 11.027 6.892 11.156 7.363 11.171C6.084 10.316 5.722 8.627 6.474 7.336C7.89 9.074 10.007 10.217 12.394 10.337C11.975 8.541 13.338 6.81 15.193 6.81C16.018 6.81 16.765 7.159 17.289 7.717C17.943 7.589 18.559 7.349 19.113 7.02C18.898 7.691 18.443 8.253 17.85 8.609C18.431 8.539 18.985 8.385 19.499 8.156C19.115 8.734 18.629 9.24 18.066 9.645Z" fill={color}/>
    </svg>
  );
};

const EmailfillSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <g clipPath="url(#clip0)">
        <path d="M12 0.0200195C5.373 0.0200195 0 5.39302 0 12.02C0 18.647 5.373 24.02 12 24.02C18.627 24.02 24 18.647 24 12.02C24 5.39302 18.627 0.0200195 12 0.0200195ZM18.99 7.00002L12 12.666L5.009 7.00002H18.99ZM19 17H5V8.49502L12 14.168L19 8.49602V17Z" fill={color}/>
      </g>
      <defs>
        <clipPath id="clip0">
          <rect height="24" width="24" fill="white"/>
        </clipPath>
      </defs>
    </svg>
  );
};

const QrcodeSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M0.5 18.531V20.5C0.5 20.7652 0.605357 21.0196 0.792893 21.2071C0.98043 21.3946 1.23478 21.5 1.5 21.5H3.469' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M3.469 2.5H1.5C1.23478 2.5 0.98043 2.60536 0.792893 2.79289C0.605357 2.98043 0.5 3.23478 0.5 3.5V5.469' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M23.5 5.469V3.5C23.5 3.23478 23.3946 2.98043 23.2071 2.79289C23.0196 2.60536 22.7652 2.5 22.5 2.5H20.531' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M20.531 21.5H22.5C22.7652 21.5 23.0196 21.3946 23.2071 21.2071C23.3946 21.0196 23.5 20.7652 23.5 20.5V18.531' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M8.49701 5.5H5.49701V8.5H8.49701V5.5Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M18.497 5.5H15.497V8.5H18.497V5.5Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M8.49701 15.5H5.49701V18.5H8.49701V15.5Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M15.497 15.5V18.5H18.497V15.5H17.497' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M10.497 8V5.5H13.497' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M13.497 7.5V11' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M5.49701 13.5H10.497V11' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M10.497 15.5V18.5H13.497' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M12.497 15.5H13.497' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M5.49701 10.5H8.49701' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M18.497 10.5H15.497V11.5' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M12.497 13.5H18.497' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
    </svg>

  );
};

const WeblinkSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g clipPath='url(#clip0)'>
      <path d='M20.121 18.882L22.242 16.761C22.4277 16.5753 22.5751 16.3548 22.6756 16.1122C22.7762 15.8696 22.828 15.6095 22.828 15.3468C22.8281 15.0842 22.7764 14.8241 22.6759 14.5814C22.5754 14.3388 22.4282 14.1182 22.2425 13.9325C22.0568 13.7467 21.8363 13.5994 21.5937 13.4988C21.351 13.3983 21.091 13.3465 20.8283 13.3464C20.5657 13.3464 20.3056 13.3981 20.0629 13.4985C19.8202 13.599 19.5997 13.7463 19.414 13.932L17.293 16.054' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M15.878 17.468L13.758 19.589C13.3827 19.964 13.1718 20.4727 13.1716 21.0033C13.1714 21.5338 13.382 22.0427 13.757 22.418C14.132 22.7933 14.6407 23.0042 15.1713 23.0044C15.7018 23.0046 16.2107 22.794 16.586 22.419L18.707 20.3' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M19.768 16.408L16.232 19.943' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M2 5.17599H22' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M5 2.92599C4.95055 2.92599 4.90222 2.94066 4.86111 2.96813C4.82 2.9956 4.78795 3.03464 4.76903 3.08032C4.75011 3.12601 4.74516 3.17627 4.7548 3.22477C4.76445 3.27326 4.78826 3.31781 4.82322 3.35277C4.85819 3.38773 4.90273 3.41154 4.95123 3.42119C4.99972 3.43084 5.04999 3.42589 5.09567 3.40696C5.14135 3.38804 5.1804 3.356 5.20787 3.31489C5.23534 3.27377 5.25 3.22544 5.25 3.17599C5.25 3.10969 5.22366 3.0461 5.17678 2.99922C5.12989 2.95233 5.0663 2.92599 5 2.92599Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M7 2.92599C6.95055 2.92599 6.90222 2.94066 6.86111 2.96813C6.82 2.9956 6.78795 3.03464 6.76903 3.08032C6.75011 3.12601 6.74516 3.17627 6.7548 3.22477C6.76445 3.27326 6.78826 3.31781 6.82322 3.35277C6.85819 3.38773 6.90273 3.41154 6.95123 3.42119C6.99972 3.43084 7.04999 3.42589 7.09567 3.40696C7.14135 3.38804 7.1804 3.356 7.20787 3.31489C7.23534 3.27377 7.25 3.22544 7.25 3.17599C7.25 3.10969 7.22366 3.0461 7.17678 2.99922C7.12989 2.95233 7.0663 2.92599 7 2.92599Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M9 2.92599C8.95055 2.92599 8.90222 2.94066 8.86111 2.96813C8.82 2.9956 8.78795 3.03464 8.76903 3.08032C8.75011 3.12601 8.74516 3.17627 8.7548 3.22477C8.76445 3.27326 8.78826 3.31781 8.82322 3.35277C8.85819 3.38773 8.90273 3.41154 8.95123 3.42119C8.99972 3.43084 9.04999 3.42589 9.09567 3.40696C9.14135 3.38804 9.1804 3.356 9.20787 3.31489C9.23534 3.27377 9.25 3.22544 9.25 3.17599C9.25 3.10969 9.22366 3.0461 9.17678 2.99922C9.12989 2.95233 9.0663 2.92599 9 2.92599Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M10 17.176H4C3.46957 17.176 2.96086 16.9653 2.58579 16.5902C2.21071 16.2151 2 15.7064 2 15.176V3.17599C2 2.64556 2.21071 2.13685 2.58579 1.76178C2.96086 1.38671 3.46957 1.17599 4 1.17599H20C20.5304 1.17599 21.0391 1.38671 21.4142 1.76178C21.7893 2.13685 22 2.64556 22 3.17599V9.17599' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      </g>
      <defs>
      <clipPath id='clip0'>
      <rect width='24' height='24' fill='white'/>
      </clipPath>
      </defs>
    </svg>

  );
};

const MobilesdkSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M12.2 11.413C12.1369 11.4406 12.0689 11.4548 12 11.4548C11.9312 11.4548 11.8631 11.4406 11.8 11.413L1.55503 6.95803C1.46601 6.91904 1.39028 6.85496 1.33711 6.77362C1.28393 6.69228 1.25562 6.59721 1.25562 6.50003C1.25562 6.40285 1.28393 6.30778 1.33711 6.22644C1.39028 6.1451 1.46601 6.08102 1.55503 6.04203L11.8 1.58703C11.8631 1.5595 11.9312 1.54529 12 1.54529C12.0689 1.54529 12.1369 1.5595 12.2 1.58703L22.445 6.04203C22.534 6.08102 22.6098 6.1451 22.6629 6.22644C22.7161 6.30778 22.7444 6.40285 22.7444 6.50003C22.7444 6.59721 22.7161 6.69228 22.6629 6.77362C22.6098 6.85496 22.534 6.91904 22.445 6.95803L12.2 11.413Z" stroke={color} stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M20.625 10.75L22.445 11.542C22.534 11.581 22.6098 11.6451 22.6629 11.7264C22.7161 11.8077 22.7444 11.9028 22.7444 12C22.7444 12.0972 22.7161 12.1923 22.6629 12.2736C22.6098 12.3549 22.534 12.419 22.445 12.458L12.2 16.913C12.1369 16.9405 12.0689 16.9547 12 16.9547C11.9312 16.9547 11.8631 16.9405 11.8 16.913L1.55503 12.458C1.46601 12.419 1.39028 12.3549 1.33711 12.2736C1.28393 12.1923 1.25562 12.0972 1.25562 12C1.25562 11.9028 1.28393 11.8077 1.33711 11.7264C1.39028 11.6451 1.46601 11.581 1.55503 11.542L3.37503 10.75" stroke={color} stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M20.625 16.25L22.445 17.042C22.534 17.081 22.6098 17.1451 22.6629 17.2264C22.7161 17.3077 22.7444 17.4028 22.7444 17.5C22.7444 17.5972 22.7161 17.6923 22.6629 17.7736C22.6098 17.8549 22.534 17.919 22.445 17.958L12.2 22.413C12.1369 22.4405 12.0689 22.4547 12 22.4547C11.9312 22.4547 11.8631 22.4405 11.8 22.413L1.55503 17.958C1.46601 17.919 1.39028 17.8549 1.33711 17.7736C1.28393 17.6923 1.25562 17.5972 1.25562 17.5C1.25562 17.4028 1.28393 17.3077 1.33711 17.2264C1.39028 17.1451 1.46601 17.081 1.55503 17.042L3.37503 16.25" stroke={color} stroke-linecap="round" stroke-linejoin="round"/>
    </svg>
  );
};

const SmsSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M22 16.5C22 16.7387 21.913 16.9676 21.758 17.1364C21.6031 17.3052 21.393 17.4 21.1739 17.4H12.087L8.78261 21V17.4H3.82609C3.607 17.4 3.39688 17.3052 3.24196 17.1364C3.08703 16.9676 3 16.7387 3 16.5V3.9C3 3.66131 3.08703 3.43239 3.24196 3.2636C3.39688 3.09482 3.607 3 3.82609 3H21.1739C21.393 3 21.6031 3.09482 21.758 3.2636C21.913 3.43239 22 3.66131 22 3.9V16.5Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M7.13043 12H17.8696' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M7.13043 7.5H17.8696' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const EmbedSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M20.7224 4.65217C20.9001 4.82336 20.9999 5.05556 21 5.2977V21.087C21 21.3291 20.9002 21.5613 20.7225 21.7326C20.5449 21.9038 20.3039 22 20.0526 22H3.94737C3.69611 22 3.45514 21.9038 3.27748 21.7326C3.09981 21.5613 3 21.3291 3 21.087V1.91304C3 1.67089 3.09981 1.43865 3.27748 1.26742C3.45514 1.0962 3.69611 1 3.94737 1H16.5407C16.6656 1.00086 16.7891 1.02551 16.904 1.07251C17.019 1.11952 17.1231 1.18797 17.2105 1.27391L20.7224 4.65217Z' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M10.1053 8.76455L6.78947 11.9602L10.1053 15.1559' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M13.8947 8.76455L17.2105 11.9602L13.8947 15.1559' stroke={color} strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const PlaySVG = ({ size = 16, color, extraClass }) => {
  return (
  <svg width={size} height={size} className={extraClass} viewBox='0 0 10 10' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path d='M0 0L10 4.5L0 10V0Z' fill={color} />
  </svg>
  );
};

const PencilSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 16 16' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path fillRule='evenodd' clipRule='evenodd' d='M12.6562 2.11716C12.3318 1.87383 11.8738 1.92613 11.6126 2.23633L4.47069 10.7173L4.13928 12.7058L5.69192 12.0848L12.7945 3.2065C13.0642 2.86945 13.0015 2.37614 12.6562 2.11716ZM10.8477 1.5922C11.4505 0.876289 12.5075 0.755594 13.2562 1.31716C14.0531 1.91486 14.1977 3.05332 13.5754 3.8312L6.39045 12.8124C6.33647 12.8798 6.26593 12.9322 6.18571 12.9643L3.68571 13.9643C3.51802 14.0313 3.32719 14.0028 3.1865 13.8895C3.04581 13.7763 2.97712 13.596 3.00681 13.4178L3.50681 10.4178C3.52155 10.3294 3.5598 10.2465 3.61756 10.178L10.8477 1.5922Z' fill={color}/>
    </svg>
  );
};

const PencilfillSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill='none' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
      <path d='M5 21H0V19H5V21ZM8.424 15.282L12.826 19.681L7 21L8.424 15.282ZM24 8.534L14.311 18.338L9.775 13.802L19.464 4L24 8.534Z' fill={color}/>
    </svg>
  );
};

const BrushSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill='none' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
      <g>
        <path d='M17.839 7.66998C18.934 6.57198 20.713 5.79898 21.524 4.55698C22.411 3.19198 22.026 1.36898 20.661 0.474979C19.298 -0.410021 17.473 -0.024021 16.575 1.33998C15.737 2.62798 15.784 4.66398 15.151 6.12098C14.339 8.00398 11.599 8.24398 8.445 6.60898C6.723 9.96498 4.38 13.566 2 16.493L13.548 24C15.044 21.114 17.955 16.652 19.989 14.117C17.162 11.854 16.314 9.19198 17.839 7.66998ZM19.591 2.12498C20.043 2.41898 20.173 3.03098 19.873 3.48198C19.579 3.94298 18.968 4.06598 18.517 3.77198C18.059 3.47298 17.932 2.86898 18.23 2.41598C18.522 1.95798 19.133 1.82698 19.591 2.12498ZM12.927 21.352L4.677 15.99C5.653 14.725 6.43 13.509 7.489 11.886C7.916 11.23 8.422 11.093 8.723 11.288C9.827 12.005 7.216 14.913 8.394 15.679C9.563 16.438 11.283 12.311 12.555 13.14C13.54 13.78 11.765 15.838 12.764 16.486C13.229 16.789 13.897 16.228 14.48 15.855C15.486 15.21 16.419 15.861 15.436 17.371C14.455 18.872 13.92 19.686 12.927 21.352Z' fill={color}/>
      </g>
    </svg>
  );
};

const GlobalSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 28 26'>
      <g fill='none' fillRule='evenodd' stroke={color} transform='translate(0 1)'>
        <path strokeLinecap='round' strokeWidth='2' strokeLinejoin='round' d='M23.25 11.409c.002 6.233-5.048 11.289-11.281 11.292C5.706 22.601.697 17.469.75 11.206.753 5.224 5.535.34 11.515.212c.162-.007.326-.01.49-.01A11.184 11.184 0 0 1 23.25 11.409zM11.515.211c-6.333 6.359-6.333 14.656 0 22.481M12.493.211c6.348 6.359 6.348 14.653 0 22.478M1.941 6.201H21.98M1.46 15.201h21.138'/>
        <circle cx='20.5' cy='17.5' r='6.5' fill='#FFF'/>
      </g>
    </svg>
  );
};

const MessagesSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 19 18' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M7.75075 14.9072C7.35775 14.836 1.6465 16.936 1.6465 16.936C3.0325 15.6115 3.7585 14.3163 3.5875 13.0615C2.8034 12.4749 2.16298 11.7177 1.7147 10.8472C1.26642 9.97659 1.02202 9.01546 1 8.0365C1.00075 4.15 4.86175 1 9.62575 1C14.3898 1 18.2507 4.15 18.2507 8.0365C18.2507 11.923 14.3898 15.0737 9.62575 15.0737C8.99704 15.0741 8.36956 15.0184 7.75075 14.9072Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M11.089 3.44873C13.6952 3.98123 15.6257 5.90498 15.6257 8.19998C15.6047 8.93161 15.4079 9.64744 15.0521 10.2871C14.6963 10.9267 14.1918 11.4713 13.5812 11.875' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const DevicesSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 26 19' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M15.5 15H2.5C2.23478 15 1.98043 14.8946 1.79289 14.7071C1.60536 14.5196 1.5 14.2652 1.5 14V2C1.5 1.73478 1.60536 1.48043 1.79289 1.29289C1.98043 1.10536 2.23478 1 2.5 1H20.5C20.7652 1 21.0196 1.10536 21.2071 1.29289C21.3946 1.48043 21.5 1.73478 21.5 2V3' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M9.5 15V18' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M13.5 15V18' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M7.5 18H15.5' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M24.5 5H17.5V17H24.5V5Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M20 14.5H22' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const IntegrationsSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 18 18' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <g clipPath='url(#clip0)'>
        <path d='M16.875 0.375732H1.125C0.710786 0.375732 0.375 0.711519 0.375 1.12573V16.8757C0.375 17.2899 0.710786 17.6257 1.125 17.6257H16.875C17.2892 17.6257 17.625 17.2899 17.625 16.8757V1.12573C17.625 0.711519 17.2892 0.375732 16.875 0.375732Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
        <path d='M6.75 7.12573C7.99264 7.12573 9 6.11837 9 4.87573C9 3.63309 7.99264 2.62573 6.75 2.62573C5.50736 2.62573 4.5 3.63309 4.5 4.87573C4.5 6.11837 5.50736 7.12573 6.75 7.12573Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
        <path d='M11.625 15.3757C12.8676 15.3757 13.875 14.3684 13.875 13.1257C13.875 11.8831 12.8676 10.8757 11.625 10.8757C10.3824 10.8757 9.375 11.8831 9.375 13.1257C9.375 14.3684 10.3824 15.3757 11.625 15.3757Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
        <path d='M9.375 13.1257H2.625' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
        <path d='M15.375 13.1257H13.875' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
        <path d='M9 4.87573H15.375' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
        <path d='M2.625 4.87573H4.5' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      </g>
      <defs>
      <clipPath id='clip0'>
        <rect width='18' height='18' fill='white'/>
      </clipPath>
      </defs>
    </svg>
  );
};

const BillingSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 14 22' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M9.19075 10.6349C9.12584 10.5863 9.04689 10.5599 8.96575 10.5599H5.0425C4.96136 10.5599 4.88241 10.5863 4.8175 10.6349C3.19225 11.8754 1.375 14.2769 1.375 16.1827C1.375 18.8759 2.875 20.3077 7 20.3077C11.125 20.3077 12.625 18.8759 12.625 16.1827C12.625 14.2769 10.8152 11.8754 9.19075 10.6349Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M4.37799 8.05774H9.62799' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M9.80428 2.78043C9.84205 2.71189 9.85744 2.63325 9.84828 2.55553C9.83913 2.47781 9.80588 2.4049 9.75321 2.34702C9.70054 2.28914 9.63108 2.24919 9.55457 2.23277C9.47805 2.21634 9.39831 2.22426 9.32653 2.25543L7.75153 2.93043L7.35028 1.92543C7.32251 1.85572 7.27446 1.79594 7.21236 1.75383C7.15025 1.71172 7.07694 1.68921 7.0019 1.68921C6.92687 1.68921 6.85355 1.71172 6.79145 1.75383C6.72934 1.79594 6.6813 1.85572 6.65353 1.92543L6.25003 2.93268L4.67503 2.25768C4.60324 2.22652 4.52351 2.21859 4.44699 2.23502C4.37048 2.25144 4.30102 2.29139 4.24834 2.34927C4.19567 2.40715 4.16243 2.48006 4.15327 2.55778C4.14411 2.6355 4.1595 2.71414 4.19728 2.78268C4.63612 3.54114 4.94369 4.36827 5.10703 5.22918C5.11842 5.32037 5.16291 5.40421 5.23204 5.46477C5.30117 5.52532 5.39013 5.55839 5.48203 5.55768H8.53078C8.62268 5.55839 8.71164 5.52532 8.78077 5.46477C8.8499 5.40421 8.89439 5.32037 8.90578 5.22918C9.06468 4.368 9.36849 3.54001 9.80428 2.78043V2.78043Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M7.37799 13.1827V12.4327' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M8.5 13.1826H6.97825C6.74408 13.1819 6.51699 13.2629 6.33614 13.4116C6.15529 13.5604 6.03201 13.7676 5.98758 13.9975C5.94314 14.2274 5.98032 14.4656 6.09272 14.6711C6.20511 14.8765 6.38567 15.0363 6.60325 15.1229L8.1505 15.7424C8.36729 15.8296 8.54697 15.9895 8.65873 16.1948C8.77049 16.4 8.80736 16.6377 8.76301 16.8671C8.71867 17.0965 8.59588 17.3034 8.4157 17.4522C8.23552 17.601 8.00918 17.6825 7.7755 17.6826H6.25' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M7.37799 18.4327V17.6827' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const SaveSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 21 22'>
      <g stroke='none' strokeWidth='1' fill='none' fillRule='evenodd'>
          <g transform='translate(-1195.000000, -95.000000)' fill={color} fillRule='nonzero'>
              <g transform='translate(1195.000000, 95.000000)'>
                  <g>
                      <path d='M1.8375,0.0983675051 C0.833146388,0.0983675051 0,0.960414074 0,1.99960444 L0,19.9255304 C0,20.964718 0.833146388,21.8267649 1.8375,21.8267649 L19.1625,21.8267649 C20.1668536,21.8267649 21,20.964718 21,19.9255304 L21,5.2588637 C21.000504,5.04256039 20.9178728,4.83492418 20.7703125,4.68170321 L16.5703125,0.336024198 C16.4222279,0.183345676 16.2215522,0.0978484138 16.0125,0.0983675051 L1.8375,0.0983675051 Z M1.8375,1.72799951 L3.9375,1.72799951 L3.9375,8.51812296 C3.93754341,8.96811415 4.29009374,9.33289286 4.725,9.33293778 L15.225,9.33293778 C15.6599063,9.33289286 16.0124566,8.96811415 16.0125,8.51812296 L16.0125,2.06750568 L19.425,5.59836988 L19.425,19.9255304 C19.425,20.0900958 19.3215459,20.1971353 19.1625,20.1971353 L1.8375,20.1971353 C1.67845414,20.1971353 1.575,20.0900958 1.575,19.9255304 L1.575,1.99960444 C1.575,1.83504173 1.67845414,1.72799951 1.8375,1.72799951 Z M5.5125,1.72799951 L14.4375,1.72799951 L14.4375,7.70330815 L5.5125,7.70330815 L5.5125,1.72799951 Z M10.5,11.2341723 C8.6246559,11.2341723 7.0875,12.8246365 7.0875,14.7650365 C7.0875,16.7054094 8.6246559,18.2959007 10.5,18.2959007 C12.3753441,18.2959007 13.9125,16.7054094 13.9125,14.7650365 C13.9125,12.8246365 12.3753441,11.2341723 10.5,11.2341723 Z M10.5,12.863802 C11.5241508,12.863802 12.3375,13.7053699 12.3375,14.7650365 C12.3375,15.8247032 11.5241508,16.6662711 10.5,16.6662711 C9.47584916,16.6662711 8.6625,15.8247032 8.6625,14.7650365 C8.6625,13.7053699 9.47584916,12.863802 10.5,12.863802 Z'></path>
                  </g>
              </g>
          </g>
      </g>
    </svg>
  );
};

const SavedoneSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 21 22'>
      <g stroke='none' strokeWidth='1' fill='none' fillRule='evenodd'>
          <g transform='translate(-1230.000000, -95.000000)'>
              <g transform='translate(1230.000000, 95.000000)'>
                  <g fill={color} fillRule='nonzero'>
                      <g id='Group'>
                          <path d='M1.8375,0.0983675051 C0.833146388,0.0983675051 0,0.960414074 0,1.99960444 L0,19.9255304 C0,20.964718 0.833146388,21.8267649 1.8375,21.8267649 L19.1625,21.8267649 C20.1668536,21.8267649 21,20.964718 21,19.9255304 L21,5.2588637 C21.000504,5.04256039 20.9178728,4.83492418 20.7703125,4.68170321 L16.5703125,0.336024198 C16.4222279,0.183345676 16.2215522,0.0978484138 16.0125,0.0983675051 L1.8375,0.0983675051 Z M1.8375,1.72799951 L3.9375,1.72799951 L3.9375,8.51812296 C3.93754341,8.96811415 4.29009374,9.33289286 4.725,9.33293778 L15.225,9.33293778 C15.6599063,9.33289286 16.0124566,8.96811415 16.0125,8.51812296 L16.0125,2.06750568 L19.425,5.59836988 L19.425,19.9255304 C19.425,20.0900958 19.3215459,20.1971353 19.1625,20.1971353 L1.8375,20.1971353 C1.67845414,20.1971353 1.575,20.0900958 1.575,19.9255304 L1.575,1.99960444 C1.575,1.83504173 1.67845414,1.72799951 1.8375,1.72799951 Z M5.5125,1.72799951 L14.4375,1.72799951 L14.4375,7.70330815 L5.5125,7.70330815 L5.5125,1.72799951 Z'></path>
                      </g>
                  </g>
                  <g transform='translate(7.000000, 12.000000)' stroke={color} strokeLinejoin='round' strokeWidth='1.5'>
                      <polyline id='Path' points='7 0 2.91666667 5 0 2.89473684'></polyline>
                  </g>
              </g>
          </g>
      </g>
    </svg>
  );
};

const PrintSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 24 24' version='1.1'>
      <g id='Print' stroke='none' strokeWidth='1' fill='none' fillRule='evenodd' strokeLinecap='round'
        strokeLinejoin='round'>
        <g id='Print-Survey--Icon---Artboard---Desktop-1440' transform='translate(-1156.000000, -17.000000)'
          stroke={color}>
          <g id='Group-7' transform='translate(50.000000, 0.000000)'>
            <g id='print-text' transform='translate(1107.000000, 18.000000)'>
              <path d='M6.875,17.8740833 L15.125,17.8740833' id='Path' />
              <path d='M6.875,15.1240833 L15.125,15.1240833' id='Path' />
              <path
                d='M5.04166667,15.125 L2.29166667,15.125 C1.28039829,15.1219798 0.461353568,14.302935 0.458333333,13.2916667 L0.458333333,7.79166667 C0.461353568,6.78039829 1.28039829,5.96135357 2.29166667,5.95833333 L19.7083333,5.95833333 C20.7196017,5.96135357 21.5386464,6.78039829 21.5416667,7.79166667 L21.5416667,13.2916667 C21.5386464,14.302935 20.7196017,15.1219798 19.7083333,15.125 L16.9583333,15.125'
                id='Path' />
              <path
                d='M5.04166667,4.125 L5.04166667,0.458333333 L13.8288333,0.458333333 C14.0719286,0.458385251 14.3050483,0.554996406 14.4769167,0.726916667 L16.68975,2.93975 C16.8616703,3.11161834 16.9582814,3.34473809 16.9583333,3.58783333 L16.9583333,4.125'
                id='Path' />
              <path
                d='M16.9583333,20.625 C16.9583333,21.131261 16.5479277,21.5416667 16.0416667,21.5416667 L5.95833333,21.5416667 C5.45207231,21.5416667 5.04166667,21.131261 5.04166667,20.625 L5.04166667,12.375 L16.9583333,12.375 L16.9583333,20.625 Z'
                id='Path' />
              <circle id='Oval' cx='3.20833333' cy='8.70741667' r='1' />
              <polyline id='Path' points='13.2916667 0.457416667 13.2916667 4.12408333 16.9583333 4.12408333' />
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

const PrintfillSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill='none' viewBox='0 0 24 24' xmlns='http://www.w3.org/2000/svg'>
      <path d='M24 5H20V0H4V5H0V18H4V24H13.519C16.466 24 19.548 20.423 19.953 18H24V5ZM6 2H18V5H6V2ZM14.691 18.648C14.691 18.648 16.16 22 12.691 22H6V14H18V16.648C18 20.242 14.691 18.648 14.691 18.648ZM21.5 8C21.224 8 21 7.776 21 7.5C21 7.224 21.224 7 21.5 7C21.776 7 22 7.224 22 7.5C22 7.776 21.776 8 21.5 8ZM16 17H8V16H16V17ZM13 18H8V19H13V18Z' fill='#63686F'/>
    </svg>
  );
};

const WalletSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 19 19' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M1.6 1.71475L11.5705 4.204C12.1807 4.38338 12.7192 4.74967 13.1102 5.25127C13.5012 5.75287 13.725 6.3645 13.75 7V16C13.7664 16.2737 13.7164 16.5473 13.6045 16.7976C13.4925 17.0479 13.3218 17.2674 13.1068 17.4376C12.8918 17.6078 12.639 17.7236 12.3697 17.7752C12.1004 17.8268 11.8227 17.8127 11.56 17.734L3.193 15.766C2.58115 15.5975 2.03881 15.2386 1.64454 14.7413C1.25027 14.244 1.02452 13.6342 1 13V3.25C1.00178 2.65381 1.23941 2.08255 1.66098 1.66098C2.08255 1.23941 2.65381 1.00178 3.25 1H16C16.5963 1.00158 17.1676 1.23915 17.5892 1.66076C18.0109 2.08237 18.2484 2.65375 18.25 3.25V11.5C18.2484 12.0963 18.0109 12.6676 17.5892 13.0892C17.1676 13.5109 16.5963 13.7484 16 13.75H13.75' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M7.75299 3.25H15.253' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M10.378 12.25C11.2064 12.25 11.878 11.5784 11.878 10.75C11.878 9.92157 11.2064 9.25 10.378 9.25C9.54956 9.25 8.87799 9.92157 8.87799 10.75C8.87799 11.5784 9.54956 12.25 10.378 12.25Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M13.753 7H15.253' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const NotificationSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M16.668 14.2996V9.20753C16.6665 7.84962 16.2499 6.52463 15.474 5.4102C14.6981 4.29577 13.6001 3.44518 12.3271 2.97247C12.2375 2.42189 11.9549 1.92115 11.53 1.55978C11.105 1.19842 10.5653 1 10.0075 1C9.44967 1 8.91003 1.19842 8.48506 1.55978C8.06009 1.92115 7.77752 2.42189 7.68787 2.97247C6.41504 3.44502 5.31725 4.29567 4.54188 5.41021C3.76651 6.52474 3.35067 7.84981 3.35019 9.20753V14.2996C3.35019 14.9229 3.10258 15.5207 2.66184 15.9615C2.22109 16.4022 1.62331 16.6498 1 16.6498H19.0181C18.3948 16.6498 17.7971 16.4022 17.3563 15.9615C16.9156 15.5207 16.668 14.9229 16.668 14.2996Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
      <path d='M12.3593 16.6499C12.3593 17.2732 12.1117 17.871 11.6709 18.3117C11.2302 18.7525 10.6324 19.0001 10.0091 19.0001C9.38576 19.0001 8.78798 18.7525 8.34723 18.3117C7.90648 17.871 7.65887 17.2732 7.65887 16.6499' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const WebhooksSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 11 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M1.43346 19L4.00489 10H1.64303C1.54356 10 1.44543 9.97697 1.35638 9.93264C1.26733 9.88831 1.18978 9.82392 1.12983 9.74453C1.06989 9.66514 1.02918 9.57293 1.01092 9.47515C0.992663 9.37736 0.997346 9.27667 1.0246 9.181L3.22832 1.46671C3.26663 1.33225 3.34772 1.21395 3.45932 1.12973C3.57092 1.04551 3.70693 0.999962 3.84675 1H8.65532C8.76906 0.999976 8.88077 1.03013 8.97905 1.08739C9.07733 1.14464 9.15866 1.22695 9.21474 1.3259C9.27082 1.42486 9.29964 1.53692 9.29826 1.65065C9.29688 1.76439 9.26535 1.87572 9.20689 1.97329L5.93346 7.42857H8.53317C8.65105 7.42978 8.76632 7.46338 8.86638 7.52569C8.96645 7.588 9.04745 7.67663 9.10054 7.78187C9.15363 7.88712 9.17676 8.00494 9.1674 8.12245C9.15804 8.23996 9.11654 8.35263 9.04746 8.44814L1.43346 19Z' stroke={color} strokeWidth='1.5' strokeLinecap='round' strokeLinejoin='round'/>
    </svg>
  );
};

const AppleiconSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass}viewBox="0 0 17 20" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <g id="Design" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="Offline-App" transform="translate(-355.000000, -702.000000)" fill="#FFFFFF" fillRule="nonzero">
                <g id="Group-9" transform="translate(260.000000, 623.000000)">
                    <g id="Group-8" transform="translate(81.000000, 69.000000)">
                        <g id="iconmonstr-apple-os-1" transform="translate(14.000000, 10.000000)">
                            <path d="M16.6666667,14.6725 C16.0116667,16.5725 14.0508333,19.9366667 12.0308333,19.9733333 C10.6908333,19.9991667 10.26,19.1791667 8.72833333,19.1791667 C7.1975,19.1791667 6.71833333,19.9483333 5.45166667,19.9983333 C3.30833333,20.0808333 0,15.1425 0,10.8358333 C0,6.88 2.75666667,4.91916667 5.165,4.88333333 C6.45666667,4.86 7.67666667,5.75416667 8.46416667,5.75416667 C9.255,5.75416667 10.7366667,4.67916667 12.2941667,4.83666667 C12.9458333,4.86416667 14.7766667,5.09916667 15.9516667,6.8175 C12.8341667,8.8525 13.32,13.1083333 16.6666667,14.6725 Z M12.315,0 C9.96,0.095 8.03833333,2.56583333 8.30666667,4.60916667 C10.4833333,4.77833333 12.5716667,2.33833333 12.315,0 Z" id="Shape"></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  );
};

const AndroidiconSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass}  viewBox='0 0 17 18' version='1.1' xmlns='http://www.w3.org/2000/svg'>
        <g id='Design' stroke='none' strokeWidth='1' fill='none' fillRule='evenodd'>
            <g id='Offline-App' transform='translate(-534.000000, -705.000000)' fill='#FFFFFF'>
                <g id='Group-9' transform='translate(260.000000, 623.000000)'>
                    <g id='Group-8-Copy' transform='translate(258.000000, 69.000000)'>
                        <g id='iconmonstr-google-play-1-(1)' transform='translate(16.000000, 13.000000)'>
                            <path d='M0.42825,17.748 L8.3265,9.87225 L11.1105,12.648 L1.72125,17.85375 C1.36425,18.05175 0.927,18.04875 0.57225,17.8455 L0.42825,17.748 Z M7.53,9.078 L0,16.58625 L0,1.56975 L7.53,9.078 Z M12.2355,5.97525 L15.91425,8.0145 C16.27575,8.2155 16.5,8.592 16.5,9 C16.5,9.408 16.27575,9.7845 15.91425,9.9855 L12.135,12.08025 L9.12375,9.078 L12.2355,5.97525 Z M0.345,0.32475 C0.4125,0.2595 0.48825,0.2025 0.57225,0.1545 C0.927,-0.04875 1.36425,-0.05175 1.72125,0.14625 L11.211,5.4075 L8.3265,8.28375 L0.345,0.32475 Z' id='Shape'></path>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  );
};

const RatingiconstarSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 44 44'>
      <path
        fill='none'
        stroke='currentColor'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='3'
        className={extraClass}
        d='M21.108 2.09a.955.955 0 011.787 0l4.832 13.7h13.646a.955.955 0 01.62 1.68l-11.402 9.454 4.773 14.337a.955.955 0 01-1.47 1.07L22 33.606l-11.9 8.727a.955.955 0 01-1.464-1.071l4.773-14.337L2.004 17.47a.955.955 0 01.62-1.68h13.649l4.835-13.7z'
      ></path>
    </svg>
  );
};

const RatingiconthumbsupSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 44 41'>
      <path
        fill='none'
        fillRule='evenodd'
        stroke='currentColor'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='3'
        className={extraClass}
        d='M2 14.255h3.5c.966 0 1.75.783 1.75 1.75v21a1.75 1.75 0 01-1.75 1.75H2v-24.5zm5.25 19.25c12.843 6.52 12.217 5.661 23.214 5.661 4.606 0 6.926-3.001 8.28-7.399v-.028l3.338-11.2v-.02a3.5 3.5 0 00-3.332-4.514h-8.575a3.5 3.5 0 01-3.384-4.393l1.543-5.852a2.998 2.998 0 00-5.355-2.481l-8.554 12.119a3.5 3.5 0 01-2.86 1.482H7.25v16.625z'
      ></path>
    </svg>
  );
};

const RatingiconcrownSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 44 39'>
      <g
        fill='none'
        fillRule='evenodd'
        stroke='currentColor'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='3'
        transform='translate(1 2)'
        className={extraClass}
      >
        <circle cx='3.5' cy='7.583' r='2.625'></circle>
        <circle cx='38.5' cy='7.73' r='2.625'></circle>
        <path d='M7 35h28M7 29.75L3.418 10.199S5.25 19.25 12.25 19.25C19.25 19.25 21 7 21 7s1.75 12.25 8.75 12.25 8.832-8.913 8.832-8.913L35 29.75H7z'></path>
        <circle cx='21' cy='3.5' r='3.5'></circle>
      </g>
    </svg>
  );
};

const RatingiconuserSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 39 44'>
      <g
        fill='none'
        fillRule='evenodd'
        stroke='currentColor'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='3'
        transform='translate(2 1)'
        className={extraClass}
      >
        <circle cx='17.304' cy='12.261' r='11.375'></circle>
        <path d='M35 41.136a18.624 18.624 0 00-35 0h35z'></path>
      </g>
    </svg>
  );
};

const RatingiconlightningSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 21 44'>
      <path
        fill='none'
        stroke='currentColor'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='3'
        className={extraClass}
        d='M2.4 41.6L8 22H2.856a1.4 1.4 0 01-1.346-1.784l4.799-16.8A1.4 1.4 0 017.656 2.4h10.472a1.4 1.4 0 011.2 2.12L12.2 16.4h5.662a1.4 1.4 0 011.12 2.22L2.4 41.6z'
      ></path>
    </svg>
  );
};

const Ratingiconsmiley1SVG = ({ size = 16 }) => {
  return (
    <svg width={size} height={size} className="ss-smiley-icon ss-smiley-icon--1" viewBox="0 0 44 44">
      <g
        fill='none'
        fillRule='evenodd'
        stroke='currentColor'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='3'
        transform='translate(1 1)'
        className='ss-smiley-icon__g'
      >
        <circle cx='21' cy='21' r='20.125'></circle>
        <path className='ss-smiley-icon__path' d='M11.375 17.063a.437.437 0 110 .874.437.437 0 010-.875m19.25.001a.437.437 0 100 .874.437.437 0 000-.875M18.375 34.32a11.9 11.9 0 0112.25-5.25'></path>
      </g>
    </svg>
  );
};

const Ratingiconsmiley2SVG = ({ size = 16 }) => {
  return (
    <svg width={size} height={size} className="ss-smiley-icon ss-smiley-icon--2" viewBox="0 0 44 44">
      <g
        fill='none'
        fillRule='evenodd'
        stroke='currentColor'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='3'
        transform='translate(1 1)'
        className='ss-smiley-icon__g'
      >
        <circle cx='21' cy='21' r='20.125'></circle>
        <path className='ss-smiley-icon__path' d='M14.875 13.563a.437.437 0 110 .874.437.437 0 010-.874m12.25 0a.437.437 0 100 .874.437.437 0 000-.874m-17.5 13.562h22.75'></path>
      </g>
    </svg>
  );
};

const Ratingiconsmiley3SVG = ({ size = 16 }) => {
  return (
    <svg width={size} height={size} className="ss-smiley-icon ss-smiley-icon--3" viewBox="0 0 44 44">
      <g
        fill='none'
        fillRule='evenodd'
        stroke='currentColor'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='3'
        transform='translate(1 1)'
        className='ss-smiley-icon__g'
      >
        <circle cx='21' cy='21' r='20.125'></circle>
        <path className='ss-smiley-icon__path' d='M11.375 17.063a.437.437 0 110 .874.437.437 0 010-.875m19.25.001a.437.437 0 100 .874.437.437 0 000-.875M10.5 27.125a11.375 11.375 0 0021 0'></path>
      </g>
    </svg>
  );
};

const Ratingiconsmiley4SVG = ({ size = 16 }) => {
  return (
    <svg width={size} height={size} className="ss-smiley-icon ss-smiley-icon--4" viewBox="0 0 44 44">
      <g
        fill='none'
        fillRule='evenodd'
        stroke='currentColor'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='3'
        transform='translate(2 2)'
        className='ss-smiley-icon__g'
      >
        <circle cx='20.125' cy='20.125' r='20.125'></circle>
        <path className='ss-smiley-icon__path' d='M15.75 15.939a4.128 4.128 0 00-7 0m15.75 0a4.128 4.128 0 017 0M9.625 26.25a11.375 11.375 0 0021 0'></path>
      </g>
    </svg>
  );
};

const Ratingiconsmiley5SVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className="ss-smiley-icon ss-smiley-icon--5" viewBox="0 0 44 44">
      <g
        fill='none'
        fillRule='evenodd'
        stroke='currentColor'
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='3'
        transform='translate(1 1)'
        className='ss-smiley-icon__g'
      >
        <circle cx='21' cy='21' r='20.125'></circle>
        <path className='ss-smiley-icon__path' d='M10.5 27.125a11.375 11.375 0 0021 0m-19-7l-4.688-4.89a2.775 2.775 0 01-.525-3.202 2.775 2.775 0 014.443-.72l.765.765.767-.764a2.774 2.774 0 014.441.719 2.774 2.774 0 01-.525 3.203L12.5 20.125zm17 0l4.688-4.891a2.776 2.776 0 00.525-3.203 2.775 2.775 0 00-4.443-.719l-.765.765-.767-.765a2.774 2.774 0 00-4.441.72 2.776 2.776 0 00.525 3.202l4.678 4.891z'></path>
      </g>
    </svg>
  );
};

const ImageplaceholderSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 36 27'>
      <path fill={color} d='M7.5 8.25a2.25 2.25 0 114.502.002A2.25 2.25 0 017.5 8.25zM21 9l-3.779 6-3.721-2.94-6 8.94h21L21 9zm12-6v21H3V3h30zm3-3H0v27h36V0z'
      ></path>
    </svg>
  );
};

const YesnoicontickSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox="0 0 22 19">
      <g
        fill="none"
        fillRule="evenodd"
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
      >
        <path d='M20.636 1.635L9.182 17.18 1 10.635'></path>
      </g>
    </svg>
  );
};

const YesnoiconcrossSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox="0 0 20 20">
      <g
        fill="none"
        fillRule="evenodd"
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
      >
        <path d='M1.375 1.374l17.25 17.25m0-17.25l-17.25 17.25'></path>
      </g>
    </svg>
  );
};

const YesnoiconthumbsupSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox="0 0 42 42">
      <g
        fill="currentColor"
        fillRule="evenodd"
      >
        <path d='M9.841 42h-6.53a3.287 3.287 0 0 1-3.283-3.284V19.034A3.287 3.287 0 0 1 3.31 15.75h6.531a3.289 3.289 0 0 1 3.284 3.284v19.682A3.288 3.288 0 0 1 9.841 42zm-6.53-23.625a.659.659 0 0 0-.658.659v19.682a.66.66 0 0 0 .657.659h6.531a.66.66 0 0 0 .659-.659V19.034a.66.66 0 0 0-.659-.659h-6.53z' fillRule='nonzero'/>
        <path d='M33.898 42H17.99c-.81 0-1.62-.192-2.345-.554l-4.42-2.21a1.313 1.313 0 0 1 1.174-2.348l4.42 2.21a2.63 2.63 0 0 0 1.171.277h15.908c.479 0 .925-.259 1.164-.676l1.482-2.593a21.526 21.526 0 0 0 2.831-10.662v-4.441a2.63 2.63 0 0 0-2.628-2.628h-9.879a2.618 2.618 0 0 1-2.432-3.592 25.25 25.25 0 0 0 1.814-9.421V3.84c0-.669-.545-1.215-1.215-1.215h-.817c-.54 0-1.02.362-1.168.88l-.514 1.8A29.96 29.96 0 0 1 17.1 15.833a6.735 6.735 0 0 1-5.288 2.542H7.874a1.313 1.313 0 0 1 0-2.625h3.938a4.129 4.129 0 0 0 3.239-1.558 27.35 27.35 0 0 0 4.96-9.609l.514-1.8A3.858 3.858 0 0 1 24.218 0h.817a3.844 3.844 0 0 1 3.84 3.84v1.522c0 3.579-.675 7.077-2.002 10.396l9.874-.008A5.258 5.258 0 0 1 42 21.003v4.441a24.15 24.15 0 0 1-3.178 11.965l-1.481 2.593A3.975 3.975 0 0 1 33.898 42z'/>
        <circle cx='6.563' cy='35.438' r='1.313'/>
      </g>
    </svg>
  );
};

const YesnoiconthumbsdownSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox="0 0 42 42">
      <g
        fill="currentColor"
        fillRule="evenodd"
        transform="matrix(1 0 0 -1 0 42)"
      >
        <path d='M9.841 42h-6.53a3.287 3.287 0 0 1-3.283-3.284V19.034A3.287 3.287 0 0 1 3.31 15.75h6.531a3.289 3.289 0 0 1 3.284 3.284v19.682A3.288 3.288 0 0 1 9.841 42zm-6.53-23.625a.659.659 0 0 0-.658.659v19.682a.66.66 0 0 0 .657.659h6.531a.66.66 0 0 0 .659-.659V19.034a.66.66 0 0 0-.659-.659h-6.53z' fillRule='nonzero'/>
        <path d='M33.898 42H17.99c-.81 0-1.62-.192-2.345-.554l-4.42-2.21a1.313 1.313 0 0 1 1.174-2.348l4.42 2.21a2.63 2.63 0 0 0 1.171.277h15.908c.479 0 .925-.259 1.164-.676l1.482-2.593a21.526 21.526 0 0 0 2.831-10.662v-4.441a2.63 2.63 0 0 0-2.628-2.628h-9.879a2.618 2.618 0 0 1-2.432-3.592 25.25 25.25 0 0 0 1.814-9.421V3.84c0-.669-.545-1.215-1.215-1.215h-.817c-.54 0-1.02.362-1.168.88l-.514 1.8A29.96 29.96 0 0 1 17.1 15.833a6.735 6.735 0 0 1-5.288 2.542H7.874a1.313 1.313 0 0 1 0-2.625h3.938a4.129 4.129 0 0 0 3.239-1.558 27.35 27.35 0 0 0 4.96-9.609l.514-1.8A3.858 3.858 0 0 1 24.218 0h.817a3.844 3.844 0 0 1 3.84 3.84v1.522c0 3.579-.675 7.077-2.002 10.396l9.874-.008A5.258 5.258 0 0 1 42 21.003v4.441a24.15 24.15 0 0 1-3.178 11.965l-1.481 2.593A3.975 3.975 0 0 1 33.898 42z'/>
        <circle cx='6.563' cy='35.438' r='1.313'/>
      </g>
    </svg>
  );
};

const YesnoicontickhandwritingSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 17 22'>
      <g
        fill='none'
        fillRule='evenodd'
        stroke={color}
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='2'
      >
        <path d='M16.203 0C11.69 4.673 8.158 13.618 7.339 17.07c-1.044-1.445-3.228-3.147-4.432-3.79-1.205-.642-2.94-.273-2.746.08.481.884 1.365.482 4.271 3.55 2.746 2.906 2.987 4.351 3.388 4.913.193.273 1.125.482 1.285-2.408.257-4.48 3.003-11.627 4.609-14.822C15.079 1.847 16.364.128 16.203 0z'
        ></path>
      </g>
    </svg>
  );
};

const YesnoiconcrosshandwritingSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 18 22'>
      <g
        fill='none'
        fillRule='evenodd'
        stroke={color}
        strokeLinecap='round'
        strokeLinejoin='round'
        strokeWidth='2'
      >
        <path d='M17.69 20.158c-.215-.501-.412-1.02-.698-1.503-.537-.93-1.199-1.788-1.842-2.647a51.135 51.135 0 00-4.06-4.74 47.711 47.711 0 003.076-4.507c.554-.912 1.109-1.86 1.556-2.826.232-.5.376-1.037.537-1.574.143-.447.214-1.037.143-1.485a.314.314 0 00-.483-.214c-.036.018-.072.053-.108.071-.035-.053-.089-.107-.178-.107-.144 0-.287.018-.43.018a.373.373 0 01-.107-.072c-.25-.16-.483-.34-.751-.483a.561.561 0 00-.823.34c-1.055 3.202-2.486 6.225-4.221 9.104a45.22 45.22 0 00-3.524-2.897 71.78 71.78 0 00-2.808-1.95C2.075 4.078 1.18 3.452.25 2.898c-.071-.036-.178.053-.125.125.59.805 1.36 1.484 2.11 2.164.86.751 1.736 1.467 2.612 2.2a47.596 47.596 0 013.685 3.416c-1.592 2.504-3.399 4.883-5.312 7.155-.269.286-.537.554-.805.84-.483.537-.984 1.074-1.413 1.664-.215.232-.412.465-.626.68-.126.125.053.286.178.178l.054-.053c.018 0 .054 0 .072-.018.876-.626 1.699-1.36 2.522-2.057a77.978 77.978 0 002.593-2.236 41.208 41.208 0 004.31-4.525 43.86 43.86 0 013.542 4.364c.59.84 1.127 1.717 1.7 2.576.303.465.679.876 1.001 1.323.286.412.733.877 1.18 1.127.215.107.448-.018.465-.268.036-.394-.107-.966-.304-1.395z'
        ></path>
      </g>
    </svg>
  );
};

const CheckfillSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 20 20' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <circle cx='10' cy='10' r='10' fill={color}/>
      <path d='M13.4521 6.83333L8.75 11.6529L6.5475 9.565L5 11.1133L8.75 14.75L15 8.38125L13.4521 6.83333Z' fill='white'/>
    </svg>
  );
};

const CheckemptySVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill="none" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
      <circle cx="8" cy="8" r="7" stroke={color} strokeWidth="2"/>
    </svg>
  );
};

const SoundUpSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox="0 0 944.09 736.89" xmlns="http://www.w3.org/2000/svg">
      <path fill={color} d="M485.09,736.89c-10.87-2.34-18.74-9.47-26.81-16.46q-107.47-93.12-215.16-186a14.76,14.76,0,0,0-8.86-3.15c-66-.18-132-.11-197.95-.15-18.65,0-32-10.25-35.61-27.23A40.3,40.3,0,0,1,0,495.48Q0,366.77,0,238c0-21.31,14.39-35.61,35.87-35.63q100.73-.06,201.45.06a13.85,13.85,0,0,0,9.84-3.63Q356.53,104.07,466,9.47C486.1-7.9,515.16-.43,522.38,24.11A45.91,45.91,0,0,1,524,36.92q.1,331.42.06,662.85c0,19.29-9.16,31.24-27.73,36.31a4.49,4.49,0,0,0-1.19.81Z"/>
      <path fill={color} d="M944.09,389.89c-.62,7.26-1.27,14.52-1.84,21.79Q929.31,575.75,830.82,707.59c-3.38,4.54-6.84,9-10.4,13.43-12.59,15.53-33.57,18.21-48.44,6.27-15.06-12.11-17.33-32.9-4.94-48.58a506.67,506.67,0,0,0,54.65-84.63c26.82-52.73,44.18-108.42,50.55-167.27q22.11-204.17-104.09-366.4c-10.69-13.77-12-29.07-3.46-42.08,12.94-19.65,40.37-20.74,55.47-2a561.63,561.63,0,0,1,122.1,309.74c.54,7.27,1.22,14.54,1.83,21.8Z"/>
      <path fill={color} d="M818.36,369.43c-1,96.48-29.32,184-86.19,262.08-7.94,10.9-18.93,15.78-32.19,14.06-13.07-1.7-22.42-9.09-27.28-21.42-4.72-12-2.57-23.2,4.95-33.5a371.25,371.25,0,0,0,46-83.23c22.15-56.3,30.56-114.57,24.68-174.84-6.66-68.17-30.39-129.85-70.47-185.33-15.55-21.53-5.5-49.63,19.79-54.75,14-2.83,25.79,1.83,34.28,13.35A437,437,0,0,1,807,269.36,427.84,427.84,0,0,1,818.36,369.43Z"/>
      <path fill={color} d="M685.18,369c-.41,59.3-16.52,113.94-48.71,163.79-10.58,16.39-31.16,21.11-47.23,11S568.5,512.86,579,496.37c18.1-28.49,30.38-59.13,35.3-92.59,8.33-56.62-2.83-109.21-32.49-158.07-3.23-5.32-6.34-11.14-7.61-17.13-3.36-15.91,5.66-31.53,20.59-37.49a33.87,33.87,0,0,1,41,12.76A300.5,300.5,0,0,1,685.18,369Z"/>
    </svg>
  );
};
const SoundOffSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox="0 0 944.09 736.89" xmlns="http://www.w3.org/2000/svg">
        <path fill={color} d="M262.47,164.33c26.11,0,52.23.36,78.32-.29a36.89,36.89,0,0,0,19.13-5.88c70.66-49,141-98.5,211.47-147.79C593.83-5.32,620-3.9,636,15.24c6,7.23,10.8,17.18,12,26.42,2,15.56,1.07,31.57.41,47.35-.15,3.59-3.62,7.72-6.63,10.41Q471,251.9,300,404.12c-40.07,35.72-80.27,71.31-120.1,107.3-5.79,5.23-10.83,6-17.78,3.47-18.75-6.91-31.39-23.5-31.47-44.89q-.47-129.09.05-258.18c.16-28,20.55-47,49-47.41C207.31,164.07,234.9,164.34,262.47,164.33Z"/>
        <path fill={color} d="M805.44,15.63l62.7,70.26L138.79,737.25,76,667.11Z"/>
        <path fill={color} d="M648.67,345.09v12.74c0,98.59-.16,197.19.12,295.78.06,21.5-7.41,38.31-27.06,47.78-19.29,9.29-36.91,4.71-53.26-8.47-53.81-43.39-107.84-86.49-163-130.66Z"/>
    </svg>
  );
};

const BoldSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} version='1.1' viewBox='0 0 22 28' xmlns='http://www.w3.org/2000/svg'>
      <g fill='none' fillRule='evenodd' stroke='none' strokeLinecap='round' strokeWidth='1'>
        <g stroke={color} strokeWidth='4' transform='translate(-318.000000, -600.000000)'>
          <g transform='translate(320.000000, 602.000000)'>
            <path id='Path' d='M0.5,11.5 L11.8122107,11.5 C14.8497768,11.5 17.3122107,9.03756612 17.3122107,6 C17.3122107,2.96243388 14.8497768,0.5 11.8122107,0.5 L0.5,0.5 L0.5,11.5 Z'/>
            <path id='Path' d='M7,11.5 L11.3529713,11.5 C14.6666798,11.5 17.3529713,14.1862915 17.3529713,17.5 C17.3529713,20.8137085 14.6666798,23.5 11.3529713,23.5 L0.5,23.5 L0.5,6'/>
          </g>
        </g>
      </g>
    </svg>
  );
};

const ItalicSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill='none' viewBox='0 0 12 13' xmlns='http://www.w3.org/2000/svg'>
      <path d='M7.42727 2.43408L5.0979 11.1274' stroke={color} strokeLinecap='square' strokeWidth='1.5'/>
      <path d='M4.72485 1.5H10.7249' stroke={color} strokeLinecap='square' strokeWidth='1.5'/>
      <path d='M1.72485 12H7.72485' stroke={color} strokeLinecap='square' strokeWidth='1.5'/>
    </svg>
  );
};

const UnderlineSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill='none' viewBox='0 0 14 16' xmlns='http://www.w3.org/2000/svg'>
      <path d='M9.94238 2.41016H11.0791V7.37305C11.0791 8.23633 10.9814 8.92188 10.7861 9.42969C10.5908 9.9375 10.2373 10.3516 9.72559 10.6719C9.21777 10.9883 8.5498 11.1465 7.72168 11.1465C6.91699 11.1465 6.25879 11.0078 5.74707 10.7305C5.23535 10.4531 4.87012 10.0527 4.65137 9.5293C4.43262 9.00195 4.32324 8.2832 4.32324 7.37305V2.41016H5.45996V7.36719C5.45996 8.11328 5.52832 8.66406 5.66504 9.01953C5.80566 9.37109 6.04395 9.64258 6.37988 9.83398C6.71973 10.0254 7.13379 10.1211 7.62207 10.1211C8.45801 10.1211 9.05371 9.93164 9.40918 9.55273C9.76465 9.17383 9.94238 8.44531 9.94238 7.36719V2.41016Z' fill={color}/>
      <path d='M1.37988 15H13.3799' stroke={color} strokeLinecap='square'/>
    </svg>
  );
};

const LeftalignSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 14 13' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M1.53491 1.5H12.5349' stroke={color} strokeWidth='1.5' strokeLinecap='square'/>
      <path d='M1.53491 6.5H12.5349' stroke={color} strokeWidth='1.5' strokeLinecap='square'/>
      <path d='M1.53491 11.5H6.53491' stroke={color} strokeWidth='1.5' strokeLinecap='square'/>
    </svg>
  );
};

const CenteralignSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 14 13' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M1.18994 6.5H12.1899' stroke={color} strokeLinecap='square' strokeWidth='1.5'/>
      <path d='M4.68994 11.5H9.68994' stroke={color} strokeLinecap='square' strokeWidth='1.5'/>
      <path d='M4.68994 1.5H9.68994' stroke={color} strokeLinecap='square' strokeWidth='1.5'/>
    </svg>
  );
};

const RightalignSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 14 13' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M0.844971 1.5H11.845' stroke={color} strokeLinecap='square' strokeWidth='1.5'/>
      <path d='M0.844971 6.5H11.845' stroke={color} strokeLinecap='square' strokeWidth='1.5'/>
      <path d='M6.84497 11.5H11.845' stroke={color} strokeLinecap='square' strokeWidth='1.5'/>
    </svg>
  );
};

const LinkSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 14 14' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M6.16727 3.1837C8.59864 0.665495 10.4831 0.325254 11.8205 2.16297C13.158 4.00069 12.4648 5.97775 9.74102 8.09414H5.21802' stroke={color} strokeWidth='1.5'/>
      <path d='M7.26474 10.3084C4.74697 12.7402 2.85182 13.0145 1.57931 11.1312C0.306798 9.24796 1.06854 7.2963 3.86454 5.27626L8.38479 5.43411' stroke={color} strokeWidth='1.5'/>
    </svg>
  );
};

const IdeaSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} viewBox='0 0 18 18' xmlns='http://www.w3.org/2000/svg'>
      <g fill='none' fillRule='evenodd' stroke={color} strokeLinecap='round'>
        <path d='M10.009 16.667h-2M10.676 15.333H7.343M9.009 1.333v2M3.352 3.677l1.415 1.414M14.666 3.677l-1.414 1.414M1.343 9.333h2M16.676 9.333h-2M13.01 9.333a4 4 0 1 0-6.287 3.278c.18.125.286.33.286.548v.508c0 .184.15.333.334.333h3.333c.184 0 .333-.15.333-.333v-.509c0-.218.107-.423.286-.547a3.987 3.987 0 0 0 1.714-3.278z'/>
      </g>
    </svg>
  );
};

const BirdSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill='none' viewBox='0 0 26 32' xmlns='http://www.w3.org/2000/svg'>
      <path d='M13.7224 0C12.4631 0.148671 10.5016 0.69452 8.81813 2.55345L6.38264 5.27618L12.534 7.26099L5.23411 22.7878L7.07486 22.6088C7.43813 22.5741 16.0061 21.6538 18.2688 14.1942L16.0249 14.1921C15.1666 16.4395 13.4654 18.1769 10.938 19.3521C10.1782 19.7048 9.44168 19.9577 8.81149 20.1367L15.4501 6.01628L10.083 4.28649L10.4053 3.92621C12.3158 1.8166 14.6704 2.01953 14.7623 2.02713C20.8461 2.40043 23.4821 6.91047 23.4189 11.2176C23.3226 17.8275 17.3861 25.1318 4.58066 25.5518L3.92942 25.5735L0.913574 32H3.24164L5.30832 27.5985C19.0452 26.8985 25.4269 18.6913 25.5354 11.2469C25.6263 5.1134 21.4397 0.543679 15.3161 0H13.7224Z' fill='#E59356' fillRule='evenodd'/>
    </svg>
  );
};

const LanguageSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <g>
        <path d="M24 23H21.216L20.146 20H15.271L14.194 23H11.497L16.438 10H19.042L24 23ZM19.427 17.931L17.722 13.028L16.01 17.931H19.427ZM10.175 7.127C10.301 6.641 10.376 6.275 10.446 5.915L8.247 5.487C8.211 5.672 8.145 6.02 8.027 6.487C7.285 6.378 6.495 6.365 5.695 6.446C5.714 5.909 5.747 5.383 5.793 4.877H8.249V2.794H6.088C6.194 2.263 6.286 1.945 6.376 1.645L4.229 1C4.071 1.526 3.939 2.042 3.807 2.794H1.356V4.877H3.54C3.482 5.55 3.447 6.248 3.437 6.954C1.024 7.84 0 9.529 0 11.061C0 12.87 1.427 14.46 3.684 14.255C6.486 14 8.357 11.884 9.454 9.281C10.588 9.935 11.062 11.034 10.635 12.052C10.239 12.993 9.074 13.89 6.85 13.844V16.086C9.319 16.124 11.748 15.187 12.7 12.92C13.63 10.706 12.568 8.285 10.175 7.127ZM7.283 8.658C6.934 9.432 6.474 10.201 5.888 10.807C5.798 10.162 5.737 9.455 5.704 8.7C6.237 8.63 6.776 8.617 7.283 8.658ZM3.495 9.382C3.557 10.329 3.664 11.2 3.812 11.978C1.816 12.343 1.736 10.375 3.495 9.382Z" fill={color}/>
      </g>
    </svg>
  );
};

const ExpandSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill='none' viewBox='0 0 23 24' xmlns='http://www.w3.org/2000/svg'>
      <path d='M6.28572 14.2856H4V20H9.71431V17.7142H6.28572V14.2856ZM4 9.71426H6.28572V6.28571H9.71431V4H4V9.71426ZM17.7143 17.7142H14.2857V20H20V14.2856H17.7143V17.7142ZM14.2857 4V6.28571H17.7143V9.71426H20V4H14.2857Z' fill={color}/>
    </svg>
  );
};

const StarSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} viewBox='0 0 45 42' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <path d='M20.6731 0.906809C20.8047 0.635026 21.0102 0.405817 21.2661 0.245439C21.522 0.0850606 21.8178 0 22.1198 0C22.4218 0 22.7177 0.0850606 22.9735 0.245439C23.2294 0.405817 23.4349 0.635026 23.5666 0.906809L29.5111 12.9245L42.8341 14.8535C43.1314 14.8967 43.4107 15.0223 43.6402 15.2161C43.8697 15.4099 44.0404 15.6641 44.1328 15.95C44.2252 16.2359 44.2356 16.5419 44.1629 16.8334C44.0902 17.1249 43.9373 17.3901 43.7214 17.5991L34.0764 26.9227L36.3526 40.1042C36.4034 40.3994 36.3706 40.7029 36.258 40.9805C36.1455 41.2581 35.9576 41.4987 35.7156 41.6752C35.4735 41.8517 35.187 41.9571 34.8883 41.9794C34.5896 42.0018 34.2906 41.9402 34.025 41.8017L22.1166 35.5839L10.1857 41.8178C9.9201 41.9563 9.62109 42.0179 9.32238 41.9955C9.02367 41.9732 8.73714 41.8678 8.49512 41.6913C8.2531 41.5148 8.06521 41.2742 7.95264 40.9966C7.84007 40.719 7.8073 40.4155 7.85803 40.1203L10.1343 26.9387L0.48923 17.6152C0.273418 17.4062 0.120488 17.1409 0.0477844 16.8494C-0.0249193 16.558 -0.0144862 16.2519 0.0778993 15.9661C0.170285 15.6802 0.340927 15.426 0.57047 15.2322C0.800013 15.0384 1.07927 14.9127 1.37657 14.8696L14.7028 12.9406L20.6731 0.906809Z' fill={color}/>
    </svg>
  );
};

const KeyboardSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill="none" viewBox="0 0 31 20" xmlns="http://www.w3.org/2000/svg">
      <path
        fill={color}
        fillRule="evenodd"
        d="M2.794 0h25.142a2.802 2.802 0 012.794 2.794V16.85a2.802 2.802 0 01-2.794 2.794H2.794A2.802 2.802 0 010 16.85V2.794A2.802 2.802 0 012.794 0zm13.968 2.794h-2.794v2.793h2.794V2.794zM13.968 8.38h2.794v2.794h-2.794V8.38zm-2.793-5.587H8.38v2.793h2.794V2.794zM8.38 8.38h2.794v2.794H8.38V8.38zm-5.587 8.38h2.793v-2.793H2.794v2.794zm2.793-5.586H2.794V8.38h2.793v2.794zM2.794 5.587h2.793V2.794H2.794v2.793zm19.555 11.175H8.381v-2.794h13.968v2.794zm-2.793-5.587h2.793V8.38h-2.793v2.794zm2.793-5.588h-2.793V2.794h2.793v2.793zm2.794 11.175h2.793v-2.794h-2.793v2.794zm2.793-5.587h-2.793V8.38h2.793v2.794zm-2.793-5.588h2.793V2.794h-2.793v2.793z"
        clipRule="evenodd"
      ></path>
      </svg>
  );
};
const EnlargeSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} fill="none" viewBox="0 0 13 14" xmlns="http://www.w3.org/2000/svg">
      <path d="M7.39964 13.4847H12.2896V8.2688H11.139V12.2574H7.39964V13.4847Z" fill={color}/>
      <path d="M6.55521 8.23536L7.36877 7.36755L12.1241 12.4399L11.3105 13.3077L6.55521 8.23536Z" fill={color}/>
      <path d="M0.0741959 5.40414H1.22476V1.41551H4.96411V0.188232H0.0741959V5.40414Z" fill={color}/>
      <path d="M0.238192 1.23096L1.05176 0.363159L5.80705 5.43547L4.99349 6.30328L0.238192 1.23096Z" fill={color}/>
      <path d="M6.55647 5.4356L11.3118 0.363281L12.1253 1.23109L7.37004 6.3034L6.55647 5.4356Z" fill={color}/>
      <path d="M11.139 5.40414H12.2896V0.188232H7.39964V1.41551H11.139V5.40414Z" fill={color}/>
      <path d="M0.0741959 13.4847H4.96411V12.2574H1.22476V8.2688H0.0741959V13.4847Z" fill={color}/>
      <path d="M0.239824 12.44L4.99512 7.36768L5.80868 8.23548L1.05339 13.3078L0.239824 12.44Z" fill={color}/>
    </svg>
  );
};

const ReloadSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg width={size} height={size} className={extraClass} version='1.1' viewBox='0 0 322.447 322.447' xmlns='http://www.w3.org/2000/svg' x='0px' y='0px' xmlSpace='preserve'>
      <path d='M321.832,230.327c-2.133-6.565-9.184-10.154-15.75-8.025l-16.254,5.281C299.785,206.991,305,184.347,305,161.224&#xA;&#x9;&#x9;c0-84.089-68.41-152.5-152.5-152.5C68.411,8.724,0,77.135,0,161.224s68.411,152.5,152.5,152.5c6.903,0,12.5-5.597,12.5-12.5&#xA;&#x9;&#x9;c0-6.902-5.597-12.5-12.5-12.5c-70.304,0-127.5-57.195-127.5-127.5c0-70.304,57.196-127.5,127.5-127.5&#xA;&#x9;&#x9;c70.305,0,127.5,57.196,127.5,127.5c0,19.372-4.371,38.337-12.723,55.568l-5.553-17.096c-2.133-6.564-9.186-10.156-15.75-8.025&#xA;&#x9;&#x9;c-6.566,2.134-10.16,9.186-8.027,15.751l14.74,45.368c1.715,5.283,6.615,8.642,11.885,8.642c1.279,0,2.582-0.198,3.865-0.614&#xA;&#x9;&#x9;l45.369-14.738C320.371,243.946,323.965,236.895,321.832,230.327z'/>
    </svg>
  );
};

const KeyleftSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size} height={size} className={extraClass} 
      fill="none"
      viewBox="0 0 5 9"
    >
      <path
        fill={color}
        fillRule="evenodd"
        d="M4.382 0L0 4.382l4.382 4.382V0z"
        clipRule="evenodd"
      ></path>
    </svg>
  );
};

const KeyrightSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size} height={size} className={extraClass}
      fill="none"
      viewBox="0 0 5 9"
    >
      <path
        fill={color}
        fillRule="evenodd"
        d="M0 0l4.382 4.382L0 8.764V0z"
        clipRule="evenodd"
      ></path>
    </svg>
  );
};

const KeydownSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size} height={size} className={extraClass}
      fill="none"
      viewBox="0 0 10 5"
    >
      <path
        fill={color}
        fillRule="evenodd"
        d="M9.573.19L5.191 4.574.809.191h8.764z"
        clipRule="evenodd"
      ></path>
    </svg>
  );
};

const KeyupSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={size} height={size} className={extraClass}
      fill="none"
      viewBox="0 0 10 5"
    >
      <path
        fill={color}
        fillRule="evenodd"
        d="M9.573 4.573L5.191.191.809 4.573h8.764z"
        clipRule="evenodd"
      ></path>
      </svg>
  );
};

const MinimizeSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill="none" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
      <path d="M5.80555 14.4445V14.1945H5.55555H2.25V12.0278H7.97222V17.75H5.80555V14.4445ZM14.4445 14.1945H14.1945V14.4445V17.75H12.0278V12.0278H17.75V14.1945H14.4445ZM5.55555 5.80555H5.80555V5.55555V2.25H7.97222V7.97222H6.88889H2.25V5.80555H5.55555ZM14.1945 5.55555V5.80555H14.4445H17.75V7.97222H12.0278V2.25H14.1945V5.55555Z" fill={color} strokeWidth="0.5"/>
    </svg>
  );
};

const GiftSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} fill={'none'} className={extraClass} viewBox="0 0 34 34" xmlns="http://www.w3.org/2000/svg">
      <path d="M32.3334 15C32.3334 15.3536 32.1929 15.6928 31.9429 15.9428C31.6928 16.1929 31.3537 16.3333 31.0001 16.3333H3.00008C2.64646 16.3333 2.30732 16.1929 2.05727 15.9428C1.80722 15.6928 1.66675 15.3536 1.66675 15V12.3333C1.66675 11.9797 1.80722 11.6406 2.05727 11.3905C2.30732 11.1405 2.64646 11 3.00008 11H31.0001C31.3537 11 31.6928 11.1405 31.9429 11.3905C32.1929 11.6406 32.3334 11.9797 32.3334 12.3333V15Z" stroke={color} strokeLinecap="round" strokeWidth="2"/>
      <path d="M29.6666 31C29.6666 31.3536 29.5261 31.6928 29.2761 31.9428C29.026 32.1929 28.6869 32.3333 28.3333 32.3333H5.66659C5.31296 32.3333 4.97382 32.1929 4.72378 31.9428C4.47373 31.6928 4.33325 31.3536 4.33325 31V16.3333H29.6666V31Z" stroke={color} strokeLinecap="round" strokeWidth="2"/>
      <path d="M12.3333 11V32.3333" stroke={color} strokeLinecap="round" strokeWidth="2"/>
      <path d="M21.6667 32.3333V11" stroke={color} strokeLinecap="round" strokeWidth="2"/>
      <path d="M24.2359 6.12C23.2173 7.14 18.3333 8.33333 18.3333 8.33333C18.3333 8.33333 19.5333 3.44933 20.5466 2.43067C20.7869 2.17997 21.0749 1.97976 21.3936 1.8418C21.7123 1.70384 22.0554 1.63088 22.4027 1.62722C22.7499 1.62356 23.0944 1.68926 23.416 1.82047C23.7376 1.95168 24.0297 2.14576 24.2753 2.39133C24.5208 2.63691 24.7149 2.92903 24.8461 3.25059C24.9773 3.57214 25.043 3.91666 25.0394 4.26393C25.0357 4.61121 24.9627 4.95426 24.8248 5.27298C24.6868 5.59169 24.4866 5.87966 24.2359 6.12V6.12Z" stroke={color} strokeLinecap="round" strokeWidth="2"/>
      <path d="M9.76407 6.12C10.7827 7.14 15.6667 8.33333 15.6667 8.33333C15.6667 8.33333 14.4667 3.44933 13.4534 2.43067C12.9607 1.95835 12.3026 1.69787 11.6201 1.70507C10.9377 1.71227 10.2852 1.98658 9.80259 2.46918C9.31998 2.95179 9.04568 3.60427 9.03848 4.28673C9.03128 4.9692 9.29176 5.62732 9.76407 6.12Z" stroke={color} strokeLinecap="round" strokeWidth="2"/>
      </svg>
  );
};

const PipeSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill="none" viewBox="0 0 72 72" xmlns="http://www.w3.org/2000/svg">
      <path d="M46.0005 24.2033C46.0005 25.1241 45.2547 25.87 44.3338 25.87H27.8679C26.9478 25.87 26.2005 25.1241 26.2005 24.2033V15.6674C26.2005 14.7466 26.9471 14 27.8679 14H44.3338C45.2547 14 46.0005 14.7466 46.0005 15.6674V24.2033Z" fill={color}/>
      <path d="M59.9971 58C59.0778 57.9993 58.3304 57.2526 58.3304 56.3326V39.8674C58.3304 38.9466 59.0778 38.1999 59.9971 38.1999H68.5338C69.4539 38.1999 70.2012 38.9466 70.2012 39.8674V56.3318C70.2012 57.2526 69.4539 57.9993 68.5338 57.9993H59.9971V58Z" fill={color}/>
      <path d="M3.66744 58C2.74662 57.9993 2 57.2526 2 56.3326V39.8674C2 38.9466 2.74662 38.1999 3.66744 38.1999H12.2034C13.1242 38.1999 13.8708 38.9466 13.8708 39.8674V56.3318C13.8708 57.2526 13.1242 57.9993 12.2034 57.9993H3.66744V58Z" fill={color}/>
      <path d="M57.3949 40.7711H45.239C44.2386 40.7711 43.4287 39.9619 43.4287 38.9622V26.8055H28.7716V38.9622C28.7716 39.9619 27.9624 40.7711 26.962 40.7711H14.8061V55.4281H26.9627H45.2398H57.3957V40.7711H57.3949Z" fill={color}/>
    </svg>
  );
};

const TimeSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <path d="M12 23.501C18.3513 23.501 23.5 18.3523 23.5 12.001C23.5 5.6497 18.3513 0.500977 12 0.500977C5.64873 0.500977 0.5 5.6497 0.5 12.001C0.5 18.3523 5.64873 23.501 12 23.501Z" stroke={color} strokeLinecap="round"/>
      <path d="M17.9996 18.0011L13.0566 13.0591" stroke={color} strokeLinecap="round"/>
      <path d="M12 13.501C12.8284 13.501 13.5 12.8294 13.5 12.001C13.5 11.1725 12.8284 10.501 12 10.501C11.1716 10.501 10.5 11.1725 10.5 12.001C10.5 12.8294 11.1716 13.501 12 13.501Z" stroke={color} strokeLinecap="round"/>
      <path d="M12 6.50098V10.501" stroke={color} strokeLinecap="round"/>
    </svg>
  );
};

const CursorSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <path d="M7.60594 19.1941C5.9894 18.7726 4.5126 17.9322 3.32449 16.7578C2.13637 15.5834 1.27896 14.1165 0.83872 12.5049C0.398485 10.8934 0.390997 9.19424 0.817011 7.57888C1.24303 5.96353 2.08748 4.48907 3.26519 3.30423C4.44291 2.11939 5.91225 1.26607 7.52501 0.830319C9.13777 0.394573 10.8369 0.391817 12.4511 0.822328C14.0652 1.25284 15.5373 2.10139 16.7189 3.28241C17.9005 4.46342 18.7497 5.93513 19.1809 7.54909" stroke={color} strokeLinecap="round"/>
      <path d="M6.8365 14.5002C6.18401 14.041 5.63925 13.4453 5.24001 12.7545C4.84077 12.0637 4.59661 11.2943 4.52448 10.4997C4.45234 9.70507 4.55396 8.90428 4.82227 8.15287C5.09059 7.40146 5.51918 6.71742 6.07831 6.14822C6.63744 5.57902 7.31372 5.13829 8.06022 4.8566C8.80672 4.57492 9.60557 4.45902 10.4013 4.51696C11.1971 4.5749 11.9708 4.80529 12.6686 5.19213C13.3664 5.57898 13.9717 6.11302 14.4425 6.75721" stroke={color} strokeLinecap="round"/>
      <path d="M18.3933 14.8641L20.5853 12.6721C20.6468 12.6105 20.6911 12.5338 20.7139 12.4498C20.7367 12.3658 20.7371 12.2773 20.7152 12.193C20.6933 12.1088 20.6498 12.0317 20.5889 11.9695C20.5281 11.9072 20.452 11.8619 20.3683 11.8381L9.46531 8.73208C9.3796 8.70762 9.2889 8.70652 9.20262 8.7289C9.11633 8.75127 9.0376 8.79631 8.97457 8.85934C8.91154 8.92237 8.86651 9.0011 8.84413 9.08738C8.82176 9.17367 8.82286 9.26436 8.84731 9.35008L11.9533 20.2501C11.9772 20.3338 12.0225 20.4098 12.0847 20.4707C12.147 20.5315 12.224 20.5751 12.3083 20.597C12.3925 20.6189 12.481 20.6185 12.5651 20.5957C12.6491 20.5729 12.7257 20.5286 12.7873 20.4671L14.8373 18.4181L19.5693 23.1501C19.6158 23.1966 19.6709 23.2336 19.7317 23.2588C19.7924 23.284 19.8575 23.297 19.9233 23.297C19.9891 23.297 20.0542 23.284 20.1149 23.2588C20.1757 23.2336 20.2309 23.1966 20.2773 23.1501L23.1263 20.3011C23.22 20.2073 23.2727 20.0802 23.2727 19.9476C23.2727 19.815 23.22 19.6878 23.1263 19.5941L18.3933 14.8641Z" stroke={color} strokeLinecap="round"/>
    </svg>
  );
};

const CompletionSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <path d="M14.5 9.5V5.5H3.5V18.499L8.5 18.5" stroke={color} strokeLinecap="round"/>
      <path d="M17.5 8.49981V3.49981C17.5 3.2346 17.3946 2.98024 17.2071 2.79271C17.0196 2.60517 16.7652 2.49981 16.5 2.49981H12.052C11.7926 1.90677 11.3659 1.40222 10.8242 1.0479C10.2825 0.693588 9.64929 0.504883 9.002 0.504883C8.35471 0.504883 7.72146 0.693588 7.17976 1.0479C6.63806 1.40222 6.2114 1.90677 5.952 2.49981H1.5C1.23478 2.49981 0.98043 2.60517 0.792893 2.79271C0.605357 2.98024 0.5 3.2346 0.5 3.49981V20.4998C0.5 20.765 0.605357 21.0194 0.792893 21.2069C0.98043 21.3945 1.23478 21.4998 1.5 21.4998H9.5" stroke={color} strokeLinecap="round"/>
      <path d="M6.5 8.5H11.5" stroke={color} strokeLinecap="round"/>
      <path d="M6.5 11.5H11.5" stroke={color} strokeLinecap="round"/>
      <path d="M6.5 14.5H9.5" stroke={color} strokeLinecap="round"/>
      <path d="M17.5 23.5C20.8137 23.5 23.5 20.8137 23.5 17.5C23.5 14.1863 20.8137 11.5 17.5 11.5C14.1863 11.5 11.5 14.1863 11.5 17.5C11.5 20.8137 14.1863 23.5 17.5 23.5Z" stroke={color} strokeLinecap="round"/>
      <path d="M20.1737 15.7549L17.2687 19.6289C17.2041 19.7148 17.1218 19.7858 17.0274 19.8372C16.933 19.8886 16.8286 19.9191 16.7214 19.9267C16.6142 19.9343 16.5066 19.9187 16.4059 19.8812C16.3052 19.8436 16.2137 19.7848 16.1377 19.7089L14.6377 18.2089" stroke={color} strokeLinecap="round"/>
    </svg>
  );
};

const DisableSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill="none" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
      <path d="M20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10ZM18 10C18 14.4183 14.4183 18 10 18C8.1513 18 6.44905 17.3729 5.09437 16.3199L16.3199 5.09438C17.3729 6.44906 18 8.1513 18 10ZM3.68016 14.9057L14.9057 3.68016C13.551 2.62709 11.8487 2 10 2C5.58172 2 2 5.58172 2 10C2 11.8487 2.62709 13.551 3.68016 14.9057Z" fill={color} fillRule="evenodd"/>
    </svg>
  );
};

const ReportSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <path d="M19.4134 17.0027H15.0027V21.4134L19.4134 17.0027Z" fill={color}/>
      <path d="M13.0027 15.0027H20V2H4V22H13.0027V15.0027ZM6.99733 5.00267H16.9973V7.00267H6.99733V5.00267ZM6.99733 9.00267H16.9973V11.0027H6.99733V9.00267Z" fill={color}/>
    </svg>
  );
};

const InviteSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill="none" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
      <path d="M11 4.5C11 6.433 12.567 8 14.5 8C16.433 8 18 6.433 18 4.5C18 2.567 16.433 1 14.5 1C12.567 1 11 2.567 11 4.5Z" fill={color}/>
      <path d="M3 4H5V7H8V9H5V12H3V9H0V7H3V4Z" fill={color}/>
      <path d="M19 13C19 11.8954 18.1046 11 17 11H12C8.68629 11 6 13.6863 6 17V18H19V13Z" fill={color}/>
    </svg>
  );
};

const BarchartSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill="none" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
      <path d="M3.1875 3V18.1875H10.7812V3H3.1875ZM4.875 4.6875H9.09375V16.5H4.875V4.6875ZM12.4688 7.21875V18.1875H20.0625V7.21875H12.4688ZM1.5 19.875V21.5625H21.75V19.875H1.5Z" fill={color}/>
    </svg>
  );
};

const LockSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill="none" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg">
      <path d="M31.3332 19.3334C31.3332 18.6261 31.0522 17.9478 30.5521 17.4477C30.052 16.9476 29.3737 16.6667 28.6665 16.6667H11.3332C10.6259 16.6667 9.94765 16.9476 9.44755 17.4477C8.94746 17.9478 8.6665 18.6261 8.6665 19.3334V32.6667C8.6665 33.3739 8.94746 34.0522 9.44755 34.5523C9.94765 35.0524 10.6259 35.3334 11.3332 35.3334H28.6665C29.3737 35.3334 30.052 35.0524 30.5521 34.5523C31.0522 34.0522 31.3332 33.3739 31.3332 32.6667V19.3334Z" stroke={color} strokeLinecap="round" strokeWidth="2"/>
      <path d="M12.6665 12C12.6665 10.0551 13.4391 8.18984 14.8144 6.81457C16.1897 5.4393 18.0549 4.66669 19.9998 4.66669C21.9448 4.66669 23.81 5.4393 25.1853 6.81457C26.5605 8.18984 27.3332 10.0551 27.3332 12V16.6667H12.6665V12Z" stroke={color} strokeLinecap="round" strokeWidth="2"/>
      <path d="M20.0002 27.6666C20.9206 27.6666 21.6668 26.9205 21.6668 26C21.6668 25.0795 20.9206 24.3333 20.0002 24.3333C19.0797 24.3333 18.3335 25.0795 18.3335 26C18.3335 26.9205 19.0797 27.6666 20.0002 27.6666Z" stroke={color} strokeLinecap="round" strokeWidth="2"/>
    </svg>
  );
};

const EmailopenSVG = ({ size = 16, color, extraClass }) => {
  return (
    <svg height={size} width={size} className={extraClass} fill="none" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg">
      <path d="M27.6216 12.6757V23.4595C27.6216 24.3103 26.9319 25 26.0811 25H5.54054C4.68972 25 4 24.3103 4 23.4595V12.6757L15.8108 6L27.6216 12.6757Z" fillRule="evenodd" stroke={color} strokeLinecap="round"/>
      <path d="M7.08521 21.9189L12.2203 17.8108H19.4095L24.5447 21.9189" stroke={color} strokeLinecap="round"/>
      <path d="M27.6257 12.6757L21.4636 16.7838" stroke={color} strokeLinecap="round"/>
      <path d="M4.00409 12.6757L10.1663 16.7838" stroke={color} strokeLinecap="round"/>
    </svg>
  );
};

export {
  BrandSVG,
  DeleteSVG,
  EditSVG,
  CloseSVG,
  SearchSVG,
  RightSVG,
  LeftSVG,
  CheckSVG,
  ErrorSVG,
  InfoSVG,
  DownloadSVG,
  DoneSVG,
  PartialdoneSVG,
  LoadingSVG,
  CheckboxSVG,
  BackSVG,
  ListSVG,
  CopySVG,
  FilterSVG,
  DownSVG,
  PlusSVG,
  PluscircleSVG,
  MinusSVG,
  UpSVG,
  HyperlinkSVG,
  UploadSVG,
  WindowsettingsSVG,
  AlarmbellcheckSVG,
  EmailsyncSVG,
  EmailcheckSVG,
  ThrottlingSVG,
  TickSVG,
  MoreSVG,
  MoreoutlineSVG,
  MenuSVG,
  EmailSVG,
  EmailfillSVG,
  SurveysparrowSVG,
  ArrowrightSVG,
  DesktopSVG,
  TabletSVG,
  MobileSVG,
  HomeSVG,
  ContactsSVG,
  SettingsSVG,
  DollarSVG,
  MusicSVG,
  SupportSVG,
  LogoutSVG,
  UserSVG,
  PauseSVG,
  ProfileSVG,
  ColumnSVG,
  DownfillSVG,
  UpfillSVG,
  ChatSVG,
  NpsSVG,
  OfflineSVG,
  NextSVG,
  PrevSVG,
  ChatsingleSVG,
  QuestionSVG,
  QuestionoutlineSVG,
  SmileSVG,
  GearSVG,
  GearfillSVG,
  VariablesSVG,
  FacebookSVG,
  FacebookfillSVG,
  EmbedSVG,
  TwitterSVG,
  TwitterfillSVG,
  SmsSVG,
  WeblinkSVG,
  QrcodeSVG,
  PencilSVG,
  PencilfillSVG,
  BrushSVG,
  PlaySVG,
  PrintSVG,
  PrintfillSVG,
  GlobalSVG,
  SaveSVG,
  SavedoneSVG,
  RatingiconstarSVG,
  RatingiconthumbsupSVG,
  RatingiconcrownSVG,
  RatingiconuserSVG,
  RatingiconlightningSVG,
  Ratingiconsmiley1SVG,
  Ratingiconsmiley2SVG,
  Ratingiconsmiley3SVG,
  Ratingiconsmiley4SVG,
  Ratingiconsmiley5SVG,
  ImageplaceholderSVG,
  YesnoicontickSVG,
  YesnoiconcrossSVG,
  YesnoiconthumbsupSVG,
  YesnoiconthumbsdownSVG,
  YesnoicontickhandwritingSVG,
  YesnoiconcrosshandwritingSVG,
  BillingSVG,
  MessagesSVG,
  IntegrationsSVG,
  DevicesSVG,
  EmailsettingsSVG,
  UsersettingsSVG,
  ProfilesettingsSVG,
  WalletSVG,
  NotificationSVG,
  WebhooksSVG,
  AppleiconSVG,
  AndroidiconSVG,
  StarSVG,
  CheckfillSVG,
  CheckemptySVG,
  BoldSVG,
  ItalicSVG,
  UnderlineSVG,
  LeftalignSVG,
  RightalignSVG,
  CenteralignSVG,
  LinkSVG,
  IdeaSVG,
  SoundUpSVG,
  SoundOffSVG,
  DownarrowSVG,
  BirdSVG,
  LanguageSVG,
  KeyboardSVG,
  KeyleftSVG,
  KeyrightSVG,
  KeyupSVG,
  KeydownSVG,
  ExpandSVG,
  EnlargeSVG,
  ReloadSVG,
  PipeSVG,
  MinimizeSVG,
  ReportSVG,
  InviteSVG,
  DisableSVG,
  GiftSVG,
  NpsworkflowSVG,
  TimeSVG,
  CursorSVG,
  CompletionSVG,
  BarchartSVG,
  LockSVG,
  EmailopenSVG,
  MobilesdkSVG,
};