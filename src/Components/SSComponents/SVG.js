import React from 'react';
import * as icons from '../SVGIcons';

class SVG extends React.Component {

  handleTitleConversion(str) {
    return str.replace(/(^|\s)\S/g,
      function (t) {
        return t.toUpperCase();
      });
  }
  render() {
    const {
      name, size, color, extraClass 
    } = this.props;
    const properName = this.handleTitleConversion(name) + 'SVG';
    const IconComponent =  icons[properName]; 
    if (!IconComponent){
      console.error('Invalid SVG ICON Name...');
      return null;
    } 
    const strokeColor = 
    color === 'white' ? '#FFFFFF' 
      : color === 'black' ? '#25292D' 
        : color === 'grey' ? '#63686F' 
          : color === 'light' ? '#bdbdbd' 
            : color === 'orange' ? '#E59356' 
              : color === 'green' ? '#00B147' 
                : color === 'red' ? '#F86060' 
                  : color === 'purple' ? '#8D5AAD' 
                    : color === 'yellow' ? '#F5C744' 
                      : color === 'swing' ? '#372459' : color; 
    return (
      <IconComponent size={size} color={strokeColor} extraClass={extraClass} />
    );
  }
}

export default SVG;